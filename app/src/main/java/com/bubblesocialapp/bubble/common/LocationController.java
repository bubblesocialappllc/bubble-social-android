package com.bubblesocialapp.bubble.common;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Location;

import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.parseObjects.Post;
import com.bubblesocialapp.bubble.services.FetchAddressIntentService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.parse.ParseGeoPoint;
import com.parse.ParseUser;

import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.ResultReceiver;
import android.widget.Toast;

import java.lang.reflect.Type;
import java.util.List;


/**
 * Created by sadaf on 5/12/15.
 */
public class LocationController implements ConnectionCallbacks,
        OnConnectionFailedListener, LocationListener {

    public static final int FIND_LOCATION = 31459;
    public static final int FIND_ADDRESS = 11220;
    private int mFLAG;
    protected boolean mAddressRequested;
    protected String mAddressOutput;
    private AddressResultReceiver mResultReceiver;

    private final Context mContext;
    private LocationCallBack mLocationCallBack;
    private GoogleApiClient mGoogleApiClient;
    private Location mCurrentLocation;
    private LocationRequest mLocationRequest;
    private List<Address> mAddresses;

    public interface LocationCallBack {
        public void onNewLocation(Location location, float accuracy);
        public void onAddressFound(List<Address> address);
    }

    private static final int UPDATE_INTERVAL_IN_MILLISECONDS = 1000;

    public LocationController(Context context, LocationCallBack callBack, int flag) {
        mResultReceiver = new AddressResultReceiver(new Handler());
        this.mContext = context;
        mLocationCallBack = callBack;
        this.mFLAG = flag;

        updateLocation();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    public void updateLocation() {
        try {

            createLocationRequest();

            mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Location getLocation() {
        return mCurrentLocation;
    }

    @Override
    public void onConnected(Bundle bundle) {
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);

        mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        //TODO: Check if location is old
        if (mCurrentLocation == null) {
            startLocationUpdates();
        } else {
            if (mFLAG == FIND_LOCATION) {
                mLocationCallBack.onNewLocation(mCurrentLocation, mCurrentLocation.getAccuracy());
                stopLocationUpdates();
            } else if (mFLAG == FIND_ADDRESS) {
                mLocationCallBack.onNewLocation(mCurrentLocation, mCurrentLocation.getAccuracy());
                startIntentService();
                startLocationUpdates();
            }
        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
            if (location != null && location.getAccuracy() <=80) {
                // Update current user location
                Util.updateCurrentUserLocation(ParseUser.getCurrentUser(),location);
                // Send back data;
                if (mFLAG == FIND_LOCATION) {
                    mLocationCallBack.onNewLocation(location, location.getAccuracy());
                    stopLocationUpdates();
                } else if (mFLAG == FIND_ADDRESS) {
                    mLocationCallBack.onNewLocation(mCurrentLocation, mCurrentLocation.getAccuracy());
                    startIntentService();
                    stopLocationUpdates();
                }
            }
    }

    public void connectToGoogleApiClient() {
        mGoogleApiClient.connect();
    }

    public void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
    }

    public void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    class AddressResultReceiver extends ResultReceiver {
        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            // Display the address string
            // or an error message sent from the intent service.
            if (resultCode == Constants.SUCCESS_RESULT) {
                mAddressOutput = resultData.getString(Constants.RESULT_DATA_KEY_ALL_LOCATIONS);
                Gson gSon = new Gson();
                Type type = new TypeToken<List<Address>>() {}.getType();

                mAddresses = gSon.fromJson(mAddressOutput, type);
                mLocationCallBack.onAddressFound(mAddresses);

            }
        }
    }

    protected void showToast(String text) {
        Toast.makeText(mContext, text, Toast.LENGTH_SHORT).show();
    }


    protected void startIntentService() {
        Intent intent = new Intent(mContext, FetchAddressIntentService.class);
        intent.putExtra(Constants.RECEIVER, mResultReceiver);
        intent.putExtra(Constants.LOCATION_DATA_EXTRA, mCurrentLocation);
        mContext.startService(intent);
    }
}


