package com.bubblesocialapp.bubble.common;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.provider.UserDictionary;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.parseObjects.User;
import com.parse.FunctionCallback;
import com.parse.Parse;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Sadaf on 12/16/2014.
 */
public class ParseCloudCode {



    private ParseCloudCode(){}

    public static void setPendingBuddy(String username,
                                       String requestingUser){
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("username",username);
        params.put("requestingUser",requestingUser);
        ParseCloud.callFunctionInBackground("setPendingBuddy", params, new FunctionCallback<String>() {
            public void done(String response, ParseException e) {
                if (e != null) {
                    Log.e("Error",e.getMessage());
                }

            }
        });
    }

    public static void acceptBuddy(String username, String currentUser){
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("username",username);
        params.put("currentUser", currentUser);
        ParseCloud.callFunctionInBackground("acceptBuddy", params, new FunctionCallback<String>() {
            public void done(String response, ParseException e) {
                if (e == null) {
                    // user gets push notification
                } else {
//                    Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                      Log.e("Error",e.getMessage());
                }
            }
        });
    }

    public static void denyBuddy(String username, String currentUser){
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("username",username);
        params.put("currentUser", currentUser);
        ParseCloud.callFunctionInBackground("denyBuddy", params, new FunctionCallback<String>() {
            public void done(String response, ParseException e) {
                if (e == null) {
                    // user gets push notification
                } else {
//                    Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                      Log.e("Error",e.getMessage());
                }
            }
        });
    }

    public static void removeBuddy(String user1){
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("userToRemove", user1);
        ParseCloud.callFunctionInBackground("removeBuddy", params, new FunctionCallback<String>() {
            public void done(String response, ParseException e) {
                if (e == null) {
                    // user gets push notification
                } else {
//                    Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                      Log.e("Error",e.getMessage());
                }
            }
        });
    }

    public static void popPoints(String user, Float bounds){
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("user", user);
        params.put("bounds",bounds);
        ParseCloud.callFunctionInBackground("popPoints", params, new FunctionCallback<ArrayList<String>>() {
            public void done(ArrayList<String> Users, ParseException e) {
                if (e == null) {
                    // Get arrayList and set to list adapter
                } else {
//                    Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                      Log.e("Error",e.getMessage());
                }
            }
        });
    }

    public static void addPoints(String username){
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("username", username);
        ParseCloud.callFunctionInBackground("addPoints", params, new FunctionCallback<Float>() {
            public void done(Float totalPoints, ParseException error) {
                if (error == null) {
                    // Get arrayList and set to list adapter
                } else {
//                    Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                    Log.e("Error", error.getMessage());
                }
            }
        });
    }

    public static void postStatus(boolean shouldSavePhoto, String text, byte[] file,
                                  ParseGeoPoint location, String locationName, List<String> mentions,
                                  String pointOfInterest, String pointOfInterestName,
                                  ParseGeoPoint pointOfInterestGeoPoint){

        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("shouldSavePhoto", shouldSavePhoto);
        params.put("text", text);
        params.put("file",file);

        params.put("point",location);
        params.put("locationName",locationName);
        params.put("mentions",mentions);

        params.put("pointOfInterest", pointOfInterest);
        params.put("pointOfInterestName", pointOfInterestName);
        params.put("pointOFInterestGeoPoint", pointOfInterestGeoPoint);

        ParseCloud.callFunctionInBackground("postStatus", params, new FunctionCallback<Object>() {
            public void done(Object response, ParseException error) {
                if (error != null) {
                    Log.e("Error",error.getMessage());
                }
            }

        });
    }

    public static void postComment(String postId, String text, byte[] file,
                                  ParseGeoPoint location, String locationName, String postUsername){

        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("postID", postId);
        params.put("text", text);
        params.put("file",file);
        params.put("point",location);
        params.put("locationName",locationName);
        params.put("username", postUsername);
        ParseCloud.callFunctionInBackground("postComment", params, new FunctionCallback<Object>() {
            public void done(Object response, ParseException error) {
                if (error == null) {
                    // do something
                } else {
//                    Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show();
                    Log.e("Error",error.getMessage());
                }
            }

        });
    }

    public static void pushNotification(String channel, String alert,
                                   String sound, String category, String objectId, String username){

        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("channel", channel);
        params.put("alert",alert);
        params.put("sound",sound);
        params.put("category",category);
        params.put("requestingUser",username);
        params.put("viewToPush",objectId);
        ParseCloud.callFunctionInBackground("pushNotification", params, new FunctionCallback<Object>() {
            public void done(Object response, ParseException error) {
                if (error == null) {
                    // do something
                } else {
//                    Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show();
                    Log.e("Error",error.getMessage());
                }
            }

        });
    }

    public static void setNotification(ParseObject post, String notificationType,
                                        String buddyUsername, String currentUsername){

        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("pObject", post);
        params.put("nType",notificationType);
        params.put("buddy",buddyUsername);
        params.put("currentUsername",currentUsername);
        ParseCloud.callFunctionInBackground("pushNotification", params, new FunctionCallback<Object>() {
            public void done(Object response, ParseException error) {
                if (error == null) {
                    // do something
                } else {
//                    Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show();
                    Log.e("Error",error.getMessage());
                }
            }

        });
    }

    public static void likePost(String objectId) {

        HashMap<String, Object> params = new HashMap<>();
        params.put("objectId", objectId);
        ParseCloud.callFunctionInBackground("likePost", params);

    }

    public static void unlikePost(String currentUser, String objectId) {

        HashMap<String, Object> params = new HashMap<>();
        params.put("user", currentUser);
        params.put("objectId", objectId);
        ParseCloud.callFunctionInBackground("unlikePost", params);

    }

    public static void phoneVerification(String phoneNumber) {

        HashMap<String, Object> params = new HashMap<>();
        params.put("phoneNumber", phoneNumber);
        ParseCloud.callFunctionInBackground("phoneVerification", params, new FunctionCallback<String>() {
            @Override
            public void done(String o, ParseException error) {
                if (error == null) {
                    // do something
                } else {
//                    Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show();
                    Log.e("Error",error.getMessage());
                }
            }
        });

    }

    public static void rank(final Activity activity) {

        //Get the shared preferences
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(activity);
        int maxDistance = sp.getInt(Constants.USER_PICKED_SEARCH_RADIUS, 10);

        //Get ready for the Cloud call
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("bounds", maxDistance);

        try {

            ParseCloud.callFunctionInBackground("rank", params, new FunctionCallback<Integer>() {
                @Override
                public void done(Integer rank, ParseException e) {
                    if (e == null) {
                        TextView rankTextView = (TextView) activity.findViewById(R.id.points);
                        TextView explanationTextView = (TextView) activity.findViewById(R.id.rank_explanation_textview);

                        //Make the views reappear
                        rankTextView.setAlpha(1);
                        explanationTextView.setAlpha(1);

                        String formattedString = activity.getString(R.string.Rank_expression, rank);

                        rankTextView.setText(rank.toString());
                        explanationTextView.setText(formattedString);
                    } else {
                        Log.e("Parse Error", e.toString());
                    }

                }
            });
        } catch (ClassCastException e) {
            Log.e("Rank", e.getMessage());
        }

    }

}


















