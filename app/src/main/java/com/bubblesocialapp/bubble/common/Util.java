package com.bubblesocialapp.bubble.common;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.bubblesocialapp.bubble.R;
import com.parse.ParseGeoPoint;
import com.parse.ParseUser;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.zip.Deflater;

public class Util {
    /**
     * Helper Functions
     */

    public static byte[] UriToByte(Context context, Uri uri) throws IOException {

        InputStream iStream = context.getContentResolver().openInputStream(uri);
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = iStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }

        return byteBuffer.toByteArray();

    }

    public static byte[] BitmapToByte(Context context, Bitmap bitmap) throws IOException {

        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, byteStream);
        byte[] byteArray =  byteStream.toByteArray();
        return byteArray;

    }

    public static Bitmap ByteToBitmap(byte[] bytes) throws NullPointerException {

        try {
            return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        }
        catch (NullPointerException e){
            Log.e("ByteToBitmap", "NullPointer");
            return null;
        }

    }

    public static byte[] scalePhoto(byte[] data) {

        // Resize photo from camera byte array
        Bitmap mealImage = BitmapFactory.decodeByteArray(data, 0, data.length);
        Bitmap mealImageScaled = Bitmap.createScaledBitmap(mealImage, 600, 600
                * mealImage.getHeight() / mealImage.getWidth(), false);

        // Override Android default landscape orientation and save portrait
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        mealImageScaled.compress(Bitmap.CompressFormat.JPEG, 70, bos);

        byte[] scaledData = bos.toByteArray();

        return scaledData;
    }

    public static String returnTime(Date createdDate) {
        Calendar currentDate = Calendar.getInstance();
        //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-ddTHH:mm:ss:SSS");

        float diff = Math.abs(currentDate.getTimeInMillis() - createdDate.getTime());
        if (Math.floor(diff / (1000 * 60 * 60 * 24)) != 0) {
            double value = Math.floor(diff / (1000 * 60 * 60 * 24));
            String time = (int) value + " d";
            return time;
        } else if (Math.floor(diff/(1000*60*60)) != 0) {
            double value = Math.floor(diff/(1000*60*60));
            String time = (int) value + " h";
            return time;
        } else if (Math.floor(diff/(1000*60)) != 0) {
            double value = Math.floor(diff / (1000 * 60));
            String time = (int) value + " m";
            return time;
        } else {
            double value = Math.floor(diff / (1000));
            String time = (int) value + " s";
            return time;
        }
    }

    public static float pxToDp(final Context context, final int px) {
        // Took from http://stackoverflow.com/questions/8309354/formula-px-to-dp-dp-to-px-android
        final float scale = context.getResources().getDisplayMetrics().density;
        return (px)/scale;
    }

    public static float dpToPx(final Context context, final int dp) {
        // Took from http://stackoverflow.com/questions/8309354/formula-px-to-dp-dp-to-px-android
        final float scale = context.getResources().getDisplayMetrics().density;
        return dp*scale;
    }

    public static Point getDisplaySize(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size;
    }

    public static void updateCurrentUserLocation(ParseUser user, Location location) {
        ParseGeoPoint geoPoint = new ParseGeoPoint(location.getLatitude(), location.getLongitude());
        user.put("location",geoPoint);
        user.saveEventually();
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static int getUserChosenRadius(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        int maxDistance = sp.getInt(Constants.USER_PICKED_SEARCH_RADIUS, 10);
        return maxDistance;
    }

    public static Drawable makeUserProfileImage(String profileName) {
        // Add Auto Profile Picture
        String nameToDisplay;
        ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
        // generate random color
        int color1 = generator.getRandomColor();
        if (profileName != null) {
            String[] parseName = profileName.split(" ", 0);
            if (parseName.length != 1) {
                nameToDisplay = parseName[0].substring(0, 1) +
                        parseName[1].substring(0, 1);
            } else {
                nameToDisplay = parseName[0].substring(0, 2);
            }

        } else {
            nameToDisplay = "NA";
        }

        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .textColor(Color.WHITE)
                .useFont(Typeface.DEFAULT)
                .fontSize(30) /* size in px */
                .bold()
                .toUpperCase()
                .endConfig()
                .buildRound(nameToDisplay, color1);

        return drawable;
    }

    public static Object convertViewToDrawable(View view) {
        int spec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        view.measure(spec, spec);
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        Bitmap b = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        c.translate(-view.getScrollX(), -view.getScrollY());
        view.draw(c);
        view.setDrawingCacheEnabled(true);
        Bitmap cacheBmp = view.getDrawingCache();
        Bitmap viewBmp = cacheBmp.copy(Bitmap.Config.ARGB_8888, true);
        view.destroyDrawingCache();
        return new BitmapDrawable(viewBmp);

    }

    public static SpannableStringBuilder makeToken(Context context, String contactName) {
        final SpannableStringBuilder sb = new SpannableStringBuilder();
        TextView tv = Util.createContactTextView(context,contactName);
        BitmapDrawable bd = (BitmapDrawable) Util.convertViewToDrawable(tv);
        bd.setBounds(0, 0, bd.getIntrinsicWidth(), bd.getIntrinsicHeight());
        sb.append(contactName + ",");
        sb.setSpan(new ImageSpan(bd), sb.length() - (contactName.length() + 1), sb.length() - 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return sb;
    }

    public static TextView createContactTextView(Context context, String text){
        //creating textview dynamically
        TextView tv = new TextView(context);
        tv.setText(text);
        tv.setTextColor(context.getResources().getColor(R.color.white));
        tv.setTextSize(20);
        tv.setBackgroundResource(R.drawable.mention_token_oval);
        tv.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        return tv;
    }

    public static int getResolvedUserColor(Context context, String color) {
        HashMap<String, Integer> colors = new HashMap<>();
        colors.put("Turquoise", R.color.Turquoise);
        colors.put("Emerald", R.color.Emerald);
        colors.put("Peter_Riveolor",R.color.Peter_River);
        colors.put("Amethyst",R.color.Amethyst);
        colors.put("Wet_Asphalolor",R.color.Wet_Asphalt);
        colors.put("Green_Sea",R.color.Green_Sea);
        colors.put("Nephritis",R.color.Nephritis);
        colors.put("Belize_Hole",R.color.Belize_Hole);
        colors.put("Wisteria", R.color.Wisteria);
        colors.put("Midnight_Blue", R.color.Midnight_Blue);
        colors.put("Sun_Flower",R.color.Sun_Flower);
        colors.put("Carrot",R.color.Carrot);
        colors.put("Alizarin",R.color.Alizarin);
        colors.put("Concrete",R.color.Concrete);
        colors.put("Orange",R.color.Orange);
        colors.put("Pumpkin",R.color.Pumpkin);
        colors.put("Pomegranate",R.color.Pomegranate);
        colors.put("Asbestos",R.color.Asbestos);
        colors.put("bubblePink", R.color.bubblePink);

        if (colors.get(color) != null) {
            return colors.get(color);
        } else {
            return R.color.bubble_pink;
        }
    }

    public static String getColorStringFromResource(Context context, int color) {
        HashMap<Integer, String> colors = new HashMap<>();
        colors.put(R.color.Turquoise, "Turquoise");
        colors.put(R.color.Emerald,"Emerald");
        colors.put(R.color.Peter_River,"Peter_Riveolor");
        colors.put(R.color.Amethyst,"Amethyst");
        colors.put(R.color.Wet_Asphalt,"Wet_Asphalolor");
        colors.put(R.color.Green_Sea,"Green_Sea");
        colors.put(R.color.Nephritis,"Nephritis");
        colors.put(R.color.Belize_Hole,"Belize_Hole");
        colors.put(R.color.Wisteria,"Wisteria");
        colors.put(R.color.Midnight_Blue,"Midnight_Blue");
        colors.put(R.color.Sun_Flower,"Sun_Flower");
        colors.put(R.color.Carrot,"Carrot");
        colors.put(R.color.Alizarin,"Alizarin");
        colors.put(R.color.Concrete,"Concrete");
        colors.put(R.color.Orange,"Orange");
        colors.put(R.color.Pumpkin,"Pumpkin");
        colors.put(R.color.Pomegranate,"Pomegranate");
        colors.put(R.color.Asbestos, "Asbestos");
        colors.put(R.color.bubblePink, "bubblePink");

        if (colors.get(color) != null) {
            return colors.get(color);
        } else {
            return "bubblePink";
        }
    }

    public static String getStateAbbreviation(Context context, String state) {
        HashMap<String, String> states = new HashMap<String, String>();
        states.put("Alabama","AL");
        states.put("Alaska","AK");
        states.put("Alberta","AB");
        states.put("American Samoa","AS");
        states.put("Arizona","AZ");
        states.put("Arkansas","AR");
        states.put("Armed Forces (AE)","AE");
        states.put("Armed Forces Americas","AA");
        states.put("Armed Forces Pacific","AP");
        states.put("British Columbia","BC");
        states.put("California","CA");
        states.put("Colorado","CO");
        states.put("Connecticut","CT");
        states.put("Delaware","DE");
        states.put("District Of Columbia","DC");
        states.put("Florida","FL");
        states.put("Georgia","GA");
        states.put("Guam","GU");
        states.put("Hawaii","HI");
        states.put("Idaho","ID");
        states.put("Illinois","IL");
        states.put("Indiana","IN");
        states.put("Iowa","IA");
        states.put("Kansas","KS");
        states.put("Kentucky","KY");
        states.put("Louisiana","LA");
        states.put("Maine","ME");
        states.put("Manitoba","MB");
        states.put("Maryland","MD");
        states.put("Massachusetts","MA");
        states.put("Michigan","MI");
        states.put("Minnesota","MN");
        states.put("Mississippi","MS");
        states.put("Missouri","MO");
        states.put("Montana","MT");
        states.put("Nebraska","NE");
        states.put("Nevada","NV");
        states.put("New Brunswick","NB");
        states.put("New Hampshire","NH");
        states.put("New Jersey","NJ");
        states.put("New Mexico","NM");
        states.put("New York","NY");
        states.put("Newfoundland","NF");
        states.put("North Carolina","NC");
        states.put("North Dakota","ND");
        states.put("Northwest Territories","NT");
        states.put("Nova Scotia","NS");
        states.put("Nunavut","NU");
        states.put("Ohio","OH");
        states.put("Oklahoma","OK");
        states.put("Ontario","ON");
        states.put("Oregon","OR");
        states.put("Pennsylvania","PA");
        states.put("Prince Edward Island","PE");
        states.put("Puerto Rico","PR");
        states.put("Quebec","QC");
        states.put("Rhode Island","RI");
        states.put("Saskatchewan","SK");
        states.put("South Carolina","SC");
        states.put("South Dakota","SD");
        states.put("Tennessee","TN");
        states.put("Texas","TX");
        states.put("Utah","UT");
        states.put("Vermont","VT");
        states.put("Virgin Islands","VI");
        states.put("Virginia","VA");
        states.put("Washington","WA");
        states.put("West Virginia","WV");
        states.put("Wisconsin","WI");
        states.put("Wyoming","WY");
        states.put("Yukon Territory","YT");

        return states.get(state);
    }

}
