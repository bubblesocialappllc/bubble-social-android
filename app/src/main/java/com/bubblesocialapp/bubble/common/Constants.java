package com.bubblesocialapp.bubble.common;

/**
 * Created by sadaf on 7/27/15.
 */
public final class Constants {

    public static final String FLAG = "Flag"; // Used to specify that there is a flag

    public static final String CONNECTION_FLAG = "Connection_Flag";

    public static final int SUCCESS_RESULT = 0;

    public static final int FAILURE_RESULT = 1;

    public static final String PACKAGE_NAME =
            "com.google.android.gms.location.sample.locationaddress";

    public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";

    public static final String RESULT_DATA_KEY = PACKAGE_NAME +
            ".RESULT_DATA_KEY";

    public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME +
            ".LOCATION_DATA_EXTRA";

    public static final String RESULT_DATA_KEY_ALL_LOCATIONS = PACKAGE_NAME +
            ".RESULT_DATA_KEY_ALL_LOCATIONS";

    public static final int REQUEST_PHOTO = 100;

    public static final int REQUEST_GALLARY_PHOTO = 200;

    public static final int REQUEST_PHOTO_FOR_POST = 300;

    public static final int REQUEST_PHOTO_FOR_PROFILE = 400;

    public static final int REQUEST_GALLERY_PHOTO_FOR_PROFILE = 500;

    public static final int REQUEST_PHOTO_FOR_COMMENT = 600;

    public static final int REQUEST_PHOTO_FOR_FACEBOOK_SIGNUP = 700;

    public static final int REQUEST_GALLARY_PHOTO_FOR_FACEBOOK_SIGNUP = 800;

    public static final String NO_NETWORK_AVAILABLE_FLAG = "NO_NETWORK_CONNECTION";

    public static final String NETWORK_AVAILABLE_FLAG = "NETWORK_CONNECTION";

    public static final String USER_OBJECT_ID = "User_object_id";

    public static final String GET_BUDDIES_ARRAY = "buddy_string_array";

    public static final int EVENT_FLAG = 3145;

    public static final int USER_FLAG = 1126;

    public static final String BUDDY_REQUEST = "BuddyRequest";

    public static final String COMMENT = "Comment";

    public static final String COMMENT_RESPONSE = "commentResponse";

    public static final String LIKE = "Like";

    public static final String MENTION = "Mention";

    public static final String NOTIFICATION_INTENT = "Notification_intent";

    public static final String USER_PICKED_SEARCH_RADIUS = "user_picked_search_radius";

    public static final String SEARCH_HEADER_ALL_BUBBLERS = "All Bubblers";

    public static final String SEARCH_HEADER_EVENTS_NEAR_YOU = "Events Near You";

    public static final String SEARCH_HEADER_PEOPLE_NEAR_YOU = "People Near You";

    public static final String SEARCH_HEADER_BUDDIES = "Your Buddies";

    public static final int PLACE_PICKER_REQUEST = 1000;

    public static final String SHARE_LOCATION = "Share_location";

    public static final String POPULARITY_POINTS = "Popularity_points";

    public static final String USER_COLOR = "user_color";

    public static final String USER_FIRST_TIME = "First_open";
}











