package com.bubblesocialapp.bubble.receivers;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

import com.bubblesocialapp.bubble.activities.HomeActivity;

/**
 * Created by sadaf on 7/25/15.
 */
public class NotificationsClickReceiver extends BroadcastReceiver {

    private static final String ACTIVITY_FLAG = "flag";
    private static final String FLAG =  "start_notifications_fragment";
    public NotificationsClickReceiver() {
        super();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent i = new Intent(context, HomeActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.putExtra(ACTIVITY_FLAG, FLAG);
        context.startActivity(i);
    }

    @Override
    public IBinder peekService(Context myContext, Intent service) {
        return super.peekService(myContext, service);
    }
}
