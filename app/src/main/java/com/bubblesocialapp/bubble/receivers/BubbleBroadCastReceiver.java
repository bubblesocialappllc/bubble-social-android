package com.bubblesocialapp.bubble.receivers;

import android.app.Activity;
import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;

import com.bubblesocialapp.bubble.activities.HomeActivity;
import com.bubblesocialapp.bubble.common.Constants;
import com.bubblesocialapp.bubble.services.NotificationService;
import com.google.gson.JsonParser;
import com.parse.ParsePushBroadcastReceiver;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by sadaf on 7/23/15.
 */
public class BubbleBroadCastReceiver extends ParsePushBroadcastReceiver {

    public BubbleBroadCastReceiver() {
        super();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getExtras().getString("com.parse.Data") != null) {
            try {
                JSONObject json = new JSONObject(intent.getExtras().getString("com.parse.Data"));
                String category = json.getString("category");
                if (category.equals(Constants.LIKE)) {
                    Intent notificationService = new Intent(context, NotificationService.class);
                    context.startService(notificationService);
                } else {
                    super.onReceive(context, intent);
                }

            } catch (JSONException e) {
                Log.e("JSON", e.getMessage());
            }
        }
    }

    @Override
    protected void onPushReceive(Context context, Intent intent) {
        super.onPushReceive(context, intent);
    }

    @Override
    protected void onPushOpen(Context context, Intent intent) {
        super.onPushOpen(context, intent);
    }

    @Override
    protected Class<? extends Activity> getActivity(Context context, Intent intent) {
        return super.getActivity(context, intent);
    }

    @Override
    protected int getSmallIconId(Context context, Intent intent) {
        return super.getSmallIconId(context, intent);
    }

    @Override
    protected Notification getNotification(Context context, Intent intent) {
        return super.getNotification(context, intent);
    }
}










