package com.bubblesocialapp.bubble;

import android.app.Application;

import com.bubblesocialapp.bubble.parseObjects.Comment;
import com.bubblesocialapp.bubble.parseObjects.ParseLocation;
import com.bubblesocialapp.bubble.parseObjects.Message;
import com.bubblesocialapp.bubble.parseObjects.MessageHead;
import com.bubblesocialapp.bubble.parseObjects.MessageHolder;
import com.bubblesocialapp.bubble.parseObjects.MessageUser;
import com.bubblesocialapp.bubble.parseObjects.Notification;
import com.bubblesocialapp.bubble.parseObjects.Phone;
import com.bubblesocialapp.bubble.parseObjects.Photo;
import com.bubblesocialapp.bubble.parseObjects.Post;
import com.bubblesocialapp.bubble.parseObjects.PostBackup;
import com.bubblesocialapp.bubble.parseObjects.Registered_Businesses;
import com.bubblesocialapp.bubble.parseObjects.Reporting;
import com.bubblesocialapp.bubble.parseObjects.Tag;
import com.bubblesocialapp.bubble.parseObjects.User;
import com.facebook.FacebookSdk;
import com.parse.Parse;
import com.parse.ParseCrashReporting;
import com.parse.ParseFacebookUtils;
import com.parse.ParseInstallation;
import com.parse.ParseObject;

/**
 * Created by Sadaf Mackertich on 11/6/2014.
 */

public class BubbleApplication extends Application {

    private static final int FACEBOOK_LOGIN_REQUEST_CODE = 11235;
    @Override
    public void onCreate() {
        super.onCreate();
//        FacebookSdk.sdkInitialize(getApplicationContext());
        // Enable Local Datastore.
        Parse.enableLocalDatastore(this);
        ParseCrashReporting.enable(this);

        ParseObject.registerSubclass(Comment.class);
        ParseObject.registerSubclass(ParseLocation.class);
        ParseObject.registerSubclass(MessageHolder.class);
        ParseObject.registerSubclass(MessageUser.class);
        ParseObject.registerSubclass(Message.class);
        ParseObject.registerSubclass(MessageHead.class);
        ParseObject.registerSubclass(Notification.class);
        ParseObject.registerSubclass(Phone.class);
        ParseObject.registerSubclass(Photo.class);
        ParseObject.registerSubclass(Post.class);
        ParseObject.registerSubclass(PostBackup.class);
        ParseObject.registerSubclass(Registered_Businesses.class);
        ParseObject.registerSubclass(Reporting.class);
        ParseObject.registerSubclass(Tag.class);

        // Bubble Dev
//        Parse.initialize(this, "Vg0ww6ltEoqf6z59zgr5mspghxs1l169KF2xeGXM", "cdj51HNemKMH7RuL9G8ybkwj6V2V04W9pbu7azDk");

        // Bubble Social Master
        Parse.initialize(this, "nGqIeadsF1JJxaa17JwryHfzmAnDJeL2qKaGiBHu", "jXeLGJUg2TxhDkE62VEv6swOx3vkTOEiVX5dmGnC");


        ParseFacebookUtils.initialize(getApplicationContext(), FACEBOOK_LOGIN_REQUEST_CODE);

        ParseInstallation.getCurrentInstallation().saveInBackground();

    }

}
