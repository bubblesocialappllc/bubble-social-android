package com.bubblesocialapp.bubble.services;

import android.app.Service;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Binder;
import android.os.IBinder;
import android.view.Gravity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.activities.HomeActivity;
import com.bubblesocialapp.bubble.common.Constants;
import com.bubblesocialapp.bubble.common.Util;
import com.bubblesocialapp.bubble.customLayouts.customNotificationBubble.IconCallback;
import com.bubblesocialapp.bubble.customLayouts.customNotificationBubble.Magnet;
import com.bubblesocialapp.bubble.interfaces.NotificationsServiceCallback;
import com.bubblesocialapp.bubble.parseObjects.Notification;
import com.parse.CountCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

/**
 * Created by sadaf on 7/23/15.
 */
public class NotificationService extends Service implements IconCallback{

    private static final String CUSTOM_INTENT = "com.bubblesocialapp.bubble.NotificationsService.notify";

    private Magnet mMagnet;
    private boolean magnetCreated = false;
    private boolean iconClicked = false;

    // Binder given to clients
    private final IBinder mBinder = new NotificationBinder();

    private NotificationsServiceCallback mCallback;
    private int mNotificationCount = 0;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (!magnetCreated) {
            mNotificationCount += 1;
            createNotificationMagnet();
            toggleMagnetCreated();
        } else {
            mNotificationCount += 1;
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public class NotificationBinder extends Binder {
        public NotificationService getService() {
            return NotificationService.this;
        }
    }

    public void createNotificationMagnet(){
        View iconView = createNotificationsBubble();
        mMagnet = new Magnet.Builder(this)
                .setIconView(iconView)
                .setIconCallback(this)
                .setRemoveIconResId(R.drawable.bubble_pin)
                .setRemoveIconShadow(R.drawable.bottom_shadow)
                .setShouldFlingAway(true)
                .setShouldStickToWall(true)
                .setRemoveIconShouldBeResponsive(true)
                .build();
        mMagnet.show();
    }

    public View createNotificationsBubble(){
        TextView mTextView = new TextView(getBaseContext());
        mTextView.setTextAppearance(getBaseContext(), android.R.style.TextAppearance_Medium);
        mTextView.setTextSize(20);
        mTextView.setBackgroundResource(R.drawable.ic_bubble_pop);
        mTextView.setGravity(Gravity.CENTER);
        mTextView.setTypeface(null, Typeface.BOLD);
        mTextView.isClickable();
        mTextView.setText(Integer.toString(mNotificationCount));
        mTextView.setTextColor(getResources().getColor(R.color.white));
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(50,50);
        params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        mTextView.setLayoutParams(params);

        return mTextView;
    }

    @Override
    public IBinder onBind(Intent intent) {

        if (!magnetCreated) {
            queryParse();
        }

        return mBinder;
    }

    public void queryParse() {
        ParseQuery<Notification> query = new ParseQuery<Notification>("Notification");
        query.whereMatches("sendTo", ParseUser.getCurrentUser().getUsername());
        query.countInBackground(new CountCallback() {
            @Override
            public void done(int i, ParseException e) {
                if (e != null) {
                    Toast.makeText(NotificationService.this, e.getMessage(), Toast.LENGTH_LONG).show();
                } else {
                    if (i != 0) {
                        toggleMagnetCreated();
                        mNotificationCount = i;
                        createNotificationMagnet();
                    }
                }
            }
        });
    }


    @Override
    public void onFlingAway() {

    }

    @Override
    public void onMove(float x, float y) {

    }

    @Override
    public void onIconClick(View icon, float iconXPose, float iconYPose) {
//        sendBroadcast(new Intent(CUSTOM_INTENT), Manifest.permission.VIBRATE);
        String mFlag;
        if (!iconClicked) {
            if (mCallback != null) {//app is already open
                toggleIconClicked();
                mCallback.startNotificationsFragment();
            } else { //app is not open
                Intent homeActivity = new Intent(this, HomeActivity.class);
                homeActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                homeActivity.putExtra(Constants.FLAG, Constants.NOTIFICATION_INTENT);
                if (Util.isNetworkAvailable(this)) {
                    mFlag = Constants.NETWORK_AVAILABLE_FLAG;
                } else {
                    mFlag = Constants.NO_NETWORK_AVAILABLE_FLAG;
                }
                homeActivity.putExtra(Constants.CONNECTION_FLAG, mFlag);
                startActivity(homeActivity);
            }
        }
    }

    public void toggleIconClicked() {
        if (!iconClicked) {
            iconClicked = true;
        } else {
            iconClicked = false;
        }
    }

    public void toggleMagnetCreated() {
        if (!magnetCreated) {
            magnetCreated = true;
        } else {
            magnetCreated = false;
        }
    }

    @Override
    public void onIconDestroyed() {
//        Use only if using startService.
        mMagnet = null;
        stopSelf();

    }

    public void destroyMagnet() {
        if (mMagnet != null) {
            mMagnet.destroy();
        }
    }

    public void setCallBack(NotificationsServiceCallback callback){
        mCallback = callback;
    }
}
