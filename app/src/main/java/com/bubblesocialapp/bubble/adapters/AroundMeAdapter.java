package com.bubblesocialapp.bubble.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.bubblesocialapp.bubble.R;
import com.parse.ParseFile;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by Sadaf on 1/18/2015.
 */

public class AroundMeAdapter extends RecyclerView.Adapter<AroundMeAdapter.ViewHolder> {

    public final static String extraMessage = "User";
    private Context mContext;
    private String nameToDisplay;
    private List<ParseUser> mUsers;
    private OnUserClickListener mCallBack;
    private Target mloadToarget;

    public interface OnUserClickListener{
        public void onUserClicked(String userObjectId, String username);
    }

    public AroundMeAdapter(Context context, List<ParseUser> users) {

        this.mContext = context;
        mUsers = users;
        try {
            this.mCallBack = ((OnUserClickListener) context);
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement AdapterCallback.");
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.around_me_item, parent, false), mCallBack);
    }

    @Override
    public void onBindViewHolder(final ViewHolder mHolder, int position) {

        mHolder.profilePic.setVisibility(View.VISIBLE);
        mHolder.profilePicExists.setVisibility(View.VISIBLE);
        ParseUser user = mUsers.get(position);

        mHolder.authorName.setText(user.getUsername());
        mHolder.userObjectId = user.getObjectId();

        mHolder.itemView.setTag(this);

        ParseFile profilePicFile = user.getParseFile("profilePicture");
        if (profilePicFile != null) {
            mHolder.profilePic.setVisibility(View.GONE);
            Picasso.with(mHolder.profilePicExists.getContext())
                    .load(profilePicFile.getUrl())
                    .error(R.drawable.ic_error)
                    .placeholder(R.drawable.ic_stub)
                    .resize(52, 52).centerCrop()
                    .into(mHolder.profilePicExists);
        } else {
            //Hide Circle ImageView
            mHolder.profilePicExists.setVisibility(View.GONE);
            // Add Auto Profile Picture
            ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
            // generate random color
            int color1 = generator.getRandomColor();

            String profileName = user.getString("realName");
            if (profileName != null){
                String[] parseName = profileName.split(" ", 0);
                if (parseName.length != 1){
                    nameToDisplay = parseName[0].substring(0,1) +
                            parseName[1].substring(0,1);
                } else {
                    nameToDisplay = parseName[0].substring(0,2);
                }

            } else {
                nameToDisplay = "NA";
            }

            TextDrawable drawable = TextDrawable.builder()
                    .beginConfig()
                    .textColor(Color.WHITE)
                    .useFont(Typeface.DEFAULT)
                    .fontSize(30)
                    .bold()
                    .toUpperCase()
                    .endConfig()
                    .buildRound(nameToDisplay, color1);


            mHolder.profilePic.setImageDrawable(drawable);

        }

    }

    @Override
    public int getItemCount() {
        if (mUsers != null) {
            return mUsers.size();
        } else {
            return 0;
        }
    }

    static class ViewHolder extends RecyclerView.ViewHolder{

        private String userObjectId;

        public ImageView profilePic;
        public CircleImageView profilePicExists;
        public TextView authorName;
        public TextView aboutMe;

        public ViewHolder(View view, final OnUserClickListener mCallback){
            super(view);

            authorName = (TextView) view.findViewById(R.id.profile_name);
            profilePic = (ImageView) view.findViewById(R.id.profile_picture_aroundMe);
            profilePicExists = (CircleImageView) view.findViewById(R.id.profile_picture_aroundMe2);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCallback.onUserClicked(userObjectId, authorName.getText().toString());
                }
            });

        }
    }
}
