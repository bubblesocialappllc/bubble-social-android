package com.bubblesocialapp.bubble.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;

import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.bean.ColorBean;
import com.bubblesocialapp.bubble.common.Constants;

import java.util.ArrayList;

/**
 * Created by sadaf on 6/2/15.
 */
public class ColorAdapter extends ArrayAdapter<ColorBean>{

    private Context mContext;
    private ViewHolder mViewHolder;
    private ArrayList<ColorBean> mObjects;


    public ColorAdapter(Context context, int resource, ArrayList<ColorBean> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mObjects = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final int colorItem = getItem(position).getItemColor();

        if (convertView == null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_color, parent, false);
            mViewHolder = new ViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (ViewHolder) convertView.getTag();
        }

        mViewHolder.mTextView.setBackgroundColor(colorItem);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(mContext);
                sp.edit().putInt(Constants.USER_COLOR, colorItem).apply();
                try {
                    Toolbar mToolbar = (Toolbar) ((Activity) v.getContext()).findViewById(R.id.screen_default_toolbar);
                    mToolbar.setBackgroundColor(((ColorDrawable) (v.getBackground())).getColor());
                } catch (NullPointerException e) {
                    // Dont do anything
                }
            }
        });

        return convertView;
    }

    @Override
    public int getCount() {
        return mObjects.size();
    }

    @Override
    public ColorBean getItem(int position) {
        return mObjects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    static class ViewHolder{
        TextView mTextView;

        public ViewHolder(View view){
            mTextView = (TextView) view.findViewById(R.id.color_frame);

        }
    }
}
