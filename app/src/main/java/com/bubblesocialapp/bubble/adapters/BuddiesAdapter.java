package com.bubblesocialapp.bubble.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.common.ParseCloudCode;
import com.bubblesocialapp.bubble.parseObjects.Post;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by sadaf on 8/4/15.
 */
public class BuddiesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private final static int BUDDY_REQUESTS = 100;
    private final static int POST_VIEW = 200;
    private RecyclerView.Adapter mAdapter;
    private List<ParseObject> mObjects;
    private Context mContext;

    public BuddiesAdapter(Context context, RecyclerView.Adapter adapter, List<ParseObject> objects) {
        this.mContext = context;
        this.mAdapter = adapter;
        this.mObjects = objects;

    }

    @Override
    public int getItemViewType(int position) {
        if (position < mObjects.size()) {
            return BUDDY_REQUESTS;
        } else {
            return POST_VIEW;
        }
    }

    @Override
    public int getItemCount() {
        return mAdapter.getItemCount() + mObjects.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case (BUDDY_REQUESTS):
                View view = LayoutInflater.from(mContext).inflate(R.layout.buddy_request, parent, false);
                return new ViewHolderBuddies(view);
            case (POST_VIEW):
                return mAdapter.onCreateViewHolder(parent, viewType);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        switch (viewType) {
            case (BUDDY_REQUESTS):
                populateBuddies(holder, position);
                break;
            case (POST_VIEW):
                mAdapter.onBindViewHolder(holder, position - mObjects.size());
                break;
        }
    }

    private void populateBuddies(RecyclerView.ViewHolder holder, final int position){
        ViewHolderBuddies mHolder = (ViewHolderBuddies) holder;
        ParseUser user = (ParseUser) mObjects.get(position);
        final String username = user.getUsername();
        final String currentUser = ParseUser.getCurrentUser().getUsername();

        String profilePicFile = user.getParseFile("profilePicture").getUrl();

        if (profilePicFile != null) {
            mHolder.profilePicture.setVisibility(View.VISIBLE);
            Picasso.with(mHolder.profilePicture.getContext())
                    .load(profilePicFile)
                    .error(R.drawable.ic_error)
                    .placeholder(R.drawable.ic_stub)
                    .resize(52, 52).centerCrop()
                    .into(mHolder.profilePicture);
        }
        mHolder.username.setText(username);

        mHolder.denyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ParseCloudCode.denyBuddy(username, currentUser);
                mObjects.remove(position);
                notifyDataSetChanged();
            }
        });

        mHolder.acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ParseCloudCode.acceptBuddy(username, currentUser);
                mObjects.remove(position);
                notifyDataSetChanged();
            }
        });
    }

    public static class ViewHolderBuddies extends RecyclerView.ViewHolder{
        TextView username;
        Button acceptButton;
        Button denyButton;
        ImageView profilePicture;

        public ViewHolderBuddies(View v){
            super(v);

            username = (TextView) v.findViewById(R.id.buddy_request_username);
            acceptButton = (Button) v.findViewById(R.id.buddy_request_accept);
            denyButton = (Button) v.findViewById(R.id.buddy_request_deny);
            profilePicture = (ImageView) v.findViewById(R.id.buddy_request_profile_picture);
        }
    }
}
