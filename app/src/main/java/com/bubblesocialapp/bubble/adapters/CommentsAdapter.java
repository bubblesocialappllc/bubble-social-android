package com.bubblesocialapp.bubble.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.common.ParseCloudCode;
import com.bubblesocialapp.bubble.parseObjects.Comment;
import com.bubblesocialapp.bubble.parseObjects.Post;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseImageView;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by Sadaf on 2/10/2015.
 */

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.ViewHolderComments> {

    private List<Comment> mComment;
    private Context mContext;
    private String nameToDisplay;

    public CommentsAdapter(Context context, List<Comment> comments) {

        this.mComment = comments;
        this.mContext = context;
    }

    // For multiple views when we make comments more like iOS versions
    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }


    @Override
    public void onBindViewHolder(ViewHolderComments holder, int position) {
        Comment comment = mComment.get(position);
        // Set Tag to Post
        holder.profileImageActualImage.setVisibility(View.GONE);
        holder.profileImage.setVisibility(View.INVISIBLE);
        holder.postImage.setImageDrawable(null);
        holder.authorName.setText(null);
        holder.dateString.setText(null);
        holder.locationName.setText(null);
        holder.textContent.setText(null);

        // Add Auto Profile Picture
        ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
        // generate random color
        int color1 = generator.getRandomColor();


        String profilePicFile = comment.getProfilePicURL();

        if (profilePicFile != null) {
            holder.profileImageActualImage.setVisibility(View.VISIBLE);
            Picasso.with(holder.profileImageActualImage.getContext())
                    .load(profilePicFile)
                    .error(R.drawable.ic_error)
                    .placeholder(R.drawable.ic_stub)
                    .resize(52, 52).centerCrop()
                    .into(holder.profileImageActualImage);
        } else {
            holder.profileImage.setVisibility(View.VISIBLE);
            String profileName = comment.getCommenter();
            if (profileName != null) {
                String[] parseName = profileName.split(" ", 0);
                if (parseName.length != 1) {
                    nameToDisplay = parseName[0].substring(0, 1) +
                            parseName[1].substring(0, 1);
                } else {
                    nameToDisplay = parseName[0].substring(0, 2);
                }

            } else {
                nameToDisplay = "NA";
            }

            TextDrawable drawable = TextDrawable.builder()
                    .beginConfig()
                    .textColor(Color.WHITE)
                    .useFont(Typeface.DEFAULT)
                    .fontSize(30) /* size in px */
                    .bold()
                    .toUpperCase()
                    .endConfig()
                    .buildRound(nameToDisplay, color1);

            holder.profileImage.setImageDrawable(drawable);
        }

        String imageFile = comment.getPostImageURL();

        if (imageFile != null) {
            holder.postImage.setVisibility(View.VISIBLE);
            Picasso.with(holder.postImage.getContext())
                    .load(imageFile)
                    .error(R.drawable.ic_error)
                    .placeholder(R.drawable.ic_stub)
                    .into(holder.postImage);
        } else {
            holder.postImage.setVisibility(View.GONE);
        }

        holder.authorName.setText(comment.getCommenter());


        holder.locationName.setText(comment.getLocationName());


        holder.textContent.setText(comment.getComment());


        holder.dateString.setText(comment.getPostTime());


    }

    @Override
    public ViewHolderComments onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.comments_item, parent, false);
        return new ViewHolderComments(view);
    }

    @Override
    public int getItemCount() {
        return mComment.size();
    }



    static class ViewHolderComments extends RecyclerView.ViewHolder {
        public ImageView postImage;
        public ImageView profileImage;
        public CircleImageView profileImageActualImage;
        public TextView authorName;
        public TextView locationName;
        public TextView textContent;
        public TextView dateString;

        public ViewHolderComments(View v) {
            super(v);

            profileImageActualImage = (CircleImageView) v.findViewById(R.id.profile_picture_circle_imageView);
            postImage = (ImageView) v.findViewById(R.id.shared_image_imageView);
            profileImage = (ImageView) v.findViewById(R.id.profile_picture_imageView);
            authorName = (TextView) v.findViewById(R.id.username_textView);
            locationName = (TextView) v.findViewById(R.id.post_location_textView);
            textContent = (TextView) v.findViewById(R.id.comment_textView);
            dateString = (TextView) v.findViewById(R.id.post_time_textView);

        }
    }

}
