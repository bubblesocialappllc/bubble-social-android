package com.bubblesocialapp.bubble.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.bean.HeaderItemBean;
import com.bubblesocialapp.bubble.common.Util;
import com.parse.ParseFile;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by sadaf on 4/12/15.
 */
public class SearchAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int VIEW_TYPE_HEADER = 0x01;

    private static final int VIEW_TYPE_CONTENT = 0x00;

    private Context mContext;
    private ViewHolder searchHolder;
    private List<HeaderItemBean> mDataList;
    private String nameToDisplay;
    private OnSearchResultClickListener mListener;
    private View mView;
    private int mHeaderDisplay;
    private boolean mMarginsFixed;

    public interface OnSearchResultClickListener {
        public void onSearchResultClick(ParseUser parseUser);
    }

    public SearchAdapter(Context context, List<HeaderItemBean> objects){
        mContext = context;
        mDataList = objects;

        try {
            this.mListener = ((OnSearchResultClickListener) context);
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement onSearchResultClickListener.");
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mDataList.get(position).isHeader() ? VIEW_TYPE_HEADER : VIEW_TYPE_CONTENT;
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //TODO: Expand for more viewtypes
        if (viewType == VIEW_TYPE_HEADER) {
            mView = LayoutInflater.from(mContext)
                    .inflate(R.layout.item_header, parent, false);
            return new HeaderViewHolder(mView);
        } else {
            mView = LayoutInflater.from(mContext)
                    .inflate(R.layout.around_me_item, parent, false);
            return new ViewHolder(mView);
        }


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final HeaderItemBean itemBean = mDataList.get(position);
        final View itemView = holder.itemView;


//        final GridSLM.LayoutParams layoutParams = GridSLM.LayoutParams.from(itemView.getLayoutParams());
//        layoutParams.setSlm(LinearSLM.ID);
////        layoutParams.headerDisplay = mHeaderDisplay;
//        layoutParams.setFirstPosition(itemBean.getHeaderPosition());
//        layoutParams.headerEndMarginIsAuto = true;
//        layoutParams.headerStartMarginIsAuto = true;
//        itemView.setLayoutParams(layoutParams);

        if (getItemViewType(position) == VIEW_TYPE_HEADER) {

            HeaderViewHolder mHolder = (HeaderViewHolder) holder;
            mHolder.bindItem(itemBean.getHeader());

        } else if (getItemViewType(position) == VIEW_TYPE_CONTENT) {
            searchHolder = (ViewHolder) holder;

            searchHolder.mPosition = position;

            searchHolder.mParseUser =  itemBean.getParseUser();

            ParseFile profilePicFile = searchHolder.mParseUser.getParseFile("profilePicture");
            if (profilePicFile != null) {
                searchHolder.profilePicExists.setVisibility(View.VISIBLE);
                Picasso.with(searchHolder.profilePicExists.getContext())
                        .load(profilePicFile.getUrl())
                        .error(R.drawable.ic_error)
                        .placeholder(R.drawable.ic_stub)
                        .resize(52, 52).centerCrop()
                        .into(searchHolder.profilePicExists);
            } else {
                // Make a profile Picture
                searchHolder.profilePic.setVisibility(View.VISIBLE);
                String profileName = searchHolder.mParseUser.getString("realName");
                Drawable drawable = Util.makeUserProfileImage(profileName);
                searchHolder.profilePic.setImageDrawable(drawable);
            }

            searchHolder.authorName.setText(searchHolder.mParseUser.getString("username"));

            searchHolder.objectId = searchHolder.mParseUser.getObjectId();
        }

//        setClickListeners(searchHolder);
    }

    private void notifyHeaderChanges() {
        for (int i = 0; i < mDataList.size(); i++) {
            HeaderItemBean item = mDataList.get(i);
            if (item.isHeader()) {
                notifyItemChanged(i);
            }
        }
    }

    public void setMarginsFixed(boolean marginsFixed) {
        mMarginsFixed = marginsFixed;
        notifyHeaderChanges();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ParseUser mParseUser;
        public ImageView profilePic;
        public CircleImageView profilePicExists;
        public TextView authorName;
        public String objectId;
        public int mPosition;

        public ViewHolder(View view){
            super(view);
            authorName = (TextView) view.findViewById(R.id.profile_name);
            profilePic = (ImageView) view.findViewById(R.id.profile_picture_aroundMe);
            profilePicExists = (CircleImageView) view.findViewById(R.id.profile_picture_aroundMe2);
        }
    }

    public static class HeaderViewHolder extends RecyclerView.ViewHolder {
        private TextView mTextView;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            mTextView = (TextView) itemView.findViewById(R.id.search_header);
        }

        public void bindItem(String text) {
            mTextView.setText(text);
        }

        @Override
        public String toString() {
            return mTextView.getText().toString();
        }
    }

    //    public void setClickListeners(final ViewHolder viewholder){
//        viewholder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mListener.onSearchResultClick(mDataList.get(viewholder.mPosition));
//            }
//        });
//    }
}
