package com.bubblesocialapp.bubble.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.parseObjects.Photo;
import com.bubblesocialapp.bubble.parseObjects.Post;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by sadaf on 8/1/15.
 */
public class PhotoPagerAdapter extends PagerAdapter {

    private Context mContext;
    private String[] mPhotos;
    private String mUrl;
    private ImageView mImageView;
    private LayoutInflater inflater;

    public PhotoPagerAdapter(Context context, String[] photos) {
        super();
        this.mContext = context;
        this.mPhotos = photos;
    }

    public PhotoPagerAdapter(Context context, String url) {
        super();
        this.mContext = context;
        this.mUrl = url;
    }

    @Override
    public int getCount() {
        if (mPhotos != null)
            return mPhotos.length;
        else
            return 1;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        String imageUrl = null;
        inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.photo_item, container,
                false);

        mImageView = (ImageView) viewLayout.findViewById(R.id.fullscreen_image_vew);

        if (mPhotos != null) {
            imageUrl = mPhotos[position];
        } else if (mUrl != null) {
            imageUrl = mUrl;
        }

        if (imageUrl != null) {
            Picasso.with(mImageView.getContext())
                    .load(imageUrl)
                    .error(R.drawable.ic_error)
                    .placeholder(R.drawable.ic_stub)
                    .into(mImageView);
            }

            container.addView(viewLayout);

            return viewLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((FrameLayout) object);
    }
}
