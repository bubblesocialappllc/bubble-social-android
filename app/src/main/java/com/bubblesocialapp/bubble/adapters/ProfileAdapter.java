package com.bubblesocialapp.bubble.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import com.bubblesocialapp.bubble.R;
import com.parse.ParseUser;

/**
 * Created by sadaf on 7/27/15.
 */
public class ProfileAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private static final int INSERT_HEADER = 11235;

    private RecyclerView.Adapter mAdapter;

    private ParseUser mUser;

    private Context mContext;

    public ProfileAdapter(Context context, RecyclerView.Adapter adapter, ParseUser user) {
        super();
        this.mAdapter = adapter;
        this.mUser = user;
        this.mContext = context;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return INSERT_HEADER;
         else
            return mAdapter.getItemViewType(position - 1);

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case INSERT_HEADER:
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_user_info_cardview, parent, false);
                return new ViewHolderProfile(view);
            default:
                return mAdapter.onCreateViewHolder(parent, viewType);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        switch (viewType) {
            case (INSERT_HEADER):
                populateProfile(holder, position);
                break;
            default:
                mAdapter.onBindViewHolder(holder, position - 1);
                break;
        }

    }

    private void populateProfile(RecyclerView.ViewHolder holder, int position) {
        final ViewHolderProfile mHolder = (ViewHolderProfile) holder;
        mHolder.userName.setText(mUser.getString("realName"));
        if (mUser.get("posts") != null) {
            mHolder.postCount.setText(mUser.get("posts").toString() + " Pops");
        } else {
            mHolder.postCount.setText("0 Pops");
        }

        if ( mUser.getString("aboutMe") != null && !mUser.getString("aboutMe").isEmpty() ) {
            mHolder.aboutMeTextView.setText(mUser.getString("aboutMe"));
        }

        mHolder.aboutMeTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CharSequence text = mHolder.aboutMeTextView.getText();
                mHolder.aboutMeTextView.setVisibility(View.GONE);
                mHolder.aboutMeTextView.setVisibility(View.VISIBLE);
                mHolder.aboutMeTextView.setText(text);
                InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(mHolder.aboutMeEditView, InputMethodManager.SHOW_IMPLICIT);
            }
        });

        mHolder.aboutMeEditView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    //Change Back to TexView
                    CharSequence text = mHolder.aboutMeEditView.getText();
                    mHolder.aboutMeEditView.setVisibility(View.GONE);
                    mHolder.aboutMeTextView.setVisibility(View.VISIBLE);
                    mHolder.aboutMeTextView.setText(text);

                    // Hide Keyboard
                    InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);


                    ParseUser currentUser = ParseUser.getCurrentUser();
                    currentUser.put("aboutMe", mHolder.aboutMeTextView.getText().toString());
                    currentUser.saveInBackground();

                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mAdapter.getItemCount() + 1;
    }


    private static class ViewHolderProfile extends RecyclerView.ViewHolder {

        public TextView userName;
        public TextView postCount;
        public TextView aboutMeTextView;
        public EditText aboutMeEditView;

        public ViewHolderProfile(View itemView) {
            super(itemView);

            userName = (TextView) itemView.findViewById(R.id.fullUserName);
            postCount = (TextView) itemView.findViewById(R.id.popCountTextView);
            aboutMeTextView = (TextView) itemView.findViewById(R.id.aboutme_textbox_Display);
            aboutMeEditView = (EditText) itemView.findViewById(R.id.aboutme_textbox_Edit);
        }
    }
}
