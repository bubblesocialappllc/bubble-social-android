package com.bubblesocialapp.bubble.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.common.Util;
import com.bubblesocialapp.bubble.parseObjects.Message;
import com.parse.ParseObject;
import com.parse.ParseUser;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by sadaf on 4/16/15.
 */
public class MessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private int OUTGOING_MESSAGE = 0;
    private int INCOMING_MESSAGE = 1;
    private Context mContext;
    private List<Message> mMessages;
    private ViewHolder mViewHolder;
    private View mView;


    public MessageAdapter(Context context,List<Message> objects) {
        mContext = context;
        mMessages = objects;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == OUTGOING_MESSAGE) {
            mView = LayoutInflater.from(mContext).inflate(R.layout.message_left, parent, false);
            return new ViewHolder(mView);
        } else if (viewType == INCOMING_MESSAGE) {
            mView = LayoutInflater.from(mContext).inflate(R.layout.message_right, parent, false);
            return new ViewHolder(mView);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        Message message = mMessages.get(position);

        mViewHolder = (ViewHolder) holder;
        if (message.getCreatedAt() != null) {
            String time = Util.returnTime(message.getCreatedAt());
            mViewHolder.mDate.setText(time);
        } else {
            mViewHolder.mDate.setText("Now");
        }

        mViewHolder.mSender.setText(message.getSender().getUsername());

        mViewHolder.mMessage.setText(message.getMessageText());
    }



//    public void newMessage(Message message) {
//        mObjects.add(message);
//
//        notifyDataSetChanged();
//    }



    @Override
    public int getItemViewType(int position) {
        String userId = mMessages.get(position).getSender().getObjectId();
        String currentUserId = ParseUser.getCurrentUser().getObjectId();
        if (userId.equals(currentUserId)) {
            return OUTGOING_MESSAGE;
        } else {
            return INCOMING_MESSAGE;
        }
    }

    @Override
    public int getItemCount() {
        if (mMessages == null){
            return 0;
        } else {
            return mMessages.size();
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView mMessage;
        public TextView mDate;
        public TextView mSender;

        public ViewHolder(View view){
            super(view);

            mMessage = (TextView) view.findViewById(R.id.txtMessage);
            mDate = (TextView) view.findViewById(R.id.txtDate);
            mSender = (TextView) view.findViewById(R.id.txtSender);
        }
    }

}
