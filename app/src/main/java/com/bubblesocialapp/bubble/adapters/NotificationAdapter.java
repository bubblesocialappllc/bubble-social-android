package com.bubblesocialapp.bubble.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.parseObjects.Notification;

import java.util.List;

/**
 * Created by Sadaf on 2/15/2015.
 */
public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolderNotifications> {

    private Context mContext;
    private List<Notification> mNotifications;
    private int BUDDYREQUEST = 1;
    private int COMMENT = 2;
    private int COMMENTRESPONSE = 3;
    private int LIKE = 4;
    private int MENTION = 5;
    private NotificationClickListener mListener;

    public interface NotificationClickListener {
        void onNotificationClick(String notificationType, Notification notification);
    }

    public NotificationAdapter(Context context, List<Notification> notificationList) {
        super();

        this.mContext = context;
        this.mNotifications = notificationList;

        try {
            this.mListener = ((NotificationClickListener) context);
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement NotificationClickListener.");
        }

    }

    @Override
    public int getItemViewType(int position) {
        String itemtype = mNotifications.get(position).getString("type");
        switch(itemtype){
            case ("BuddyRequest"):
                return BUDDYREQUEST;
            case ("Comment"):
                return COMMENT;
            case ("commentResponse"):
                return COMMENTRESPONSE;
            case ("Like"):
                return LIKE;
            case ("Mention"):
                return MENTION;
        }

        return 0;
    }

    @Override
    public ViewHolderNotifications onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.notifications_item, parent, false);

        return new ViewHolderNotifications(mContext, view, mListener);
    }

    @Override
    public void onBindViewHolder(ViewHolderNotifications holder, int position) {

        ViewHolderNotifications mHolder = (ViewHolderNotifications) holder;

        final Notification notificationItem = mNotifications.get(position);

        mHolder.notificationItem = notificationItem;


        final int itemType = getItemViewType(position);
        StringBuilder notificationText = new StringBuilder();
        notificationText.append(notificationItem.getSentFrom());
        if (itemType == 1) {
            notificationText.append(" wants to be your buddy!");
        } else if (itemType == 2) {
            notificationText.append(" commented on your post!");
        } else if (itemType == 3) {
            notificationText.append(" responded to your comment!");
        } else if (itemType == 4) {
            notificationText.append(" likes your post!");
        } else if (itemType == 5) {
            notificationText.append(" mentioned you in a comment!");
        }

        mHolder.notificationText.setText(notificationText);

    }

    @Override
    public int getItemCount() {
        return mNotifications.size();
    }

    public static class ViewHolderNotifications extends RecyclerView.ViewHolder{
        private ImageView notificationIcon;
        private TextView  notificationText;
        private Notification notificationItem;

        public ViewHolderNotifications(final Context context, View itemView, final NotificationClickListener listener) {
            super(itemView);
            notificationIcon = (ImageView) itemView.findViewById(R.id.notification_icon);
            notificationText = (TextView) itemView.findViewById(R.id.notification_text);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onNotificationClick(notificationItem.getNotificationType(), notificationItem);

                }
            });

        }
    }

}
