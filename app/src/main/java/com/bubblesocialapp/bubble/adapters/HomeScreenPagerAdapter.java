package com.bubblesocialapp.bubble.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.fragments.AroundMeAndPopularFeedHolderFragment;
import com.bubblesocialapp.bubble.fragments.FeedFragment;
import com.bubblesocialapp.bubble.fragments.MessageHeadFragment;
import com.bubblesocialapp.bubble.fragments.ProfileFragment;
import com.parse.ParseUser;

/**
 * Created by Sadaf on 3/1/2015.
 */
public class HomeScreenPagerAdapter extends FragmentStatePagerAdapter{

    private static final String[] CONTENT = new String[]{ "Feed" , "Poppin'", "Profile" };

    private static final String SHOW_POSTS_HAPPENING_NOW = "Show_current_posts";

    private static final String SHOW_POPULAR_POSTS = "Popular_posts";

    private static final int[] ICONS = {R.drawable.bubble_icon_white,
                                        R.drawable.popping_icon,
                                        R.drawable.profile_icon};

    private String feedDataFromSplashScreen;

    private boolean mIsNetworkAvailable;

    private SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();

    public HomeScreenPagerAdapter(FragmentManager fragmentManager, boolean isNetworkAvailable) {
        super(fragmentManager);
        this.mIsNetworkAvailable = isNetworkAvailable;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case (0):
                if (feedDataFromSplashScreen != null) {
                    return FeedFragment.newInstance(FeedFragment.SHOW_POSTS_HAPPENING_NOW, mIsNetworkAvailable);
                } else {
                    return FeedFragment.newInstance(FeedFragment.SHOW_POSTS_HAPPENING_NOW, mIsNetworkAvailable);
                }
            case (1):
                return new AroundMeAndPopularFeedHolderFragment();
            case (2):
                return ProfileFragment.newInstance(ParseUser.getCurrentUser().getObjectId(),
                        ParseUser.getCurrentUser().getUsername());
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case (0):
                return CONTENT[position % CONTENT.length];
            case (1):
                return CONTENT[position % CONTENT.length];
            case (2):
                return CONTENT[position % CONTENT.length];
            default:
                return CONTENT[position % CONTENT.length];
        }
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        super.setPrimaryItem(container, position, object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }

    public int getPageImageDrawable(int position) {
        return ICONS[position];
    }

}