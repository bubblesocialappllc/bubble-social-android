package com.bubblesocialapp.bubble.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ScaleDrawable;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bubblesocialapp.bubble.common.Constants;
import com.bubblesocialapp.bubble.common.ParseCloudCode;
import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.bubblesocialapp.bubble.fragments.FeedFragment;
import com.bubblesocialapp.bubble.parseObjects.Post;
import com.bubblesocialapp.bubble.R;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Sadaf Mackertich on 11/6/2014.
 */
public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.ViewHolder> {

    private Context mContext;
    private AdapterCallBack mListener;
    private List<Post> mPost;
    private Picasso imageLoader;
    private String nameToDisplay;
    private Boolean liked;
    private ParseUser currentUser;
    private Point mSize;
    private Target mloadToarget;
    private ViewHolder mViewHolder;
    private String Flag;

    public interface AdapterCallBack {
        public void startCommentsFragment(String objectIdOfPost, String postAuthorUsername);
        public void onPhotoClick(String url);
    }

    public FeedAdapter(Context context, List<Post> post, Point size, String flag) {

        this.mContext = context;
        this.mPost = post;
        this.mSize = size;
        this.Flag = flag;

        try {
            this.mListener = ((AdapterCallBack) context);
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement AdapterCallback.");
        }
    }

    @Override
    public int getItemCount() {
        if (mPost != null){
            return mPost.size();
        } else {
            return 0;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        currentUser = ParseUser.getCurrentUser();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_feed_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view, mContext, mListener);
        view.setTag(viewHolder);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Post post = mPost.get(position);

        mViewHolder = (ViewHolder) holder;

        mViewHolder.profileImageActualImage.setVisibility(View.INVISIBLE);
        mViewHolder.profileImage.setVisibility(View.INVISIBLE);
        mViewHolder.postImage.setImageDrawable(null);
        mViewHolder.authorName.setText(null);
        mViewHolder.comments.setText(null);
        mViewHolder.dateString.setText(null);
        mViewHolder.locationName.setText(null);
        mViewHolder.numberOfLikes.setText(null);
        mViewHolder.textContent.setText(null);
        mViewHolder.objectIdString.setText(null);

        String profilePicFile = post.getProfilePicURL();

        if (profilePicFile != null) {

            mViewHolder.profileImageActualImage.setVisibility(View.VISIBLE);
            Picasso.with(mViewHolder.profileImageActualImage.getContext())
                    .load(profilePicFile)
                    .error(R.drawable.ic_error)
                    .placeholder(R.drawable.ic_stub)
                    .resize(100, 100).centerCrop()
                    .into(mViewHolder.profileImageActualImage);
        } else {
            setRandomProfilePicture(post);
        }

        String imageFile = post.getPostImageURL();

        if (imageFile != null) {
            mViewHolder.postImage.setVisibility(View.VISIBLE);
            Picasso.with(mViewHolder.postImage.getContext())
                    .load(imageFile)
                    .error(R.drawable.ic_error)
                    .resize(mSize.x,0)// Leave one variable zero for autoresize
                    .placeholder(R.drawable.ic_stub)
                    .into(mViewHolder.postImage);

            mViewHolder.postImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onPhotoClick(post.getPostImageURL());
                }
            });

        } else {
            mViewHolder.postImage.setVisibility(View.GONE);
        }

        mViewHolder.authorName.setText(post.getAuthorUsername());
        mViewHolder.locationName.setText(post.getLocationName());
        mViewHolder.textContent.setText(post.getText());
        if (!Flag.equals(FeedFragment.SHOW_POPULAR_POSTS)) {
            mViewHolder.dateString.setText(post.getPostTime());
        } else {
            mViewHolder.dateString.setText(post.getPoints() + "BP");
        }
        mViewHolder.objectIdString.setText(post.getObjectId());

        String objectIdText = mViewHolder.objectIdString.getText().toString();

        setUpLikes(post, objectIdText);
        setUpComments(post,objectIdText);

        setUpMentions(post);
    }

    private void setUpMentions(Post post) {
        if (post.getMentioned() != null) {
            if (post.getMentioned().size() != 0) {
                // Make bold when mentioned
//                for (String mention:post.getMentioned()) {
////                    setUpSpans(post,mention);
//                }
            }
        }
    }

//    private void setUpSpans(Post post, String mention) {
//        int startSpan = post.getText().indexOf(mention);
//        int endSpan = startSpan + mention.length();
//        Spannable spanRange = new SpannableString(post.getText());
//        TextAppearanceSpan tas = new TextAppearanceSpan(mContext, android.R.style.TextAppearance_Large);
//        spanRange.setSpan(tas, startSpan, endSpan, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//        mViewHolder.textContent.setText(spanRange);
//    }

    private void setUpComments(Post post, final String objectIdText){

        Drawable drawable = mContext.getResources().getDrawable(R.drawable.comment_icon_white);
        drawable.setBounds(0,0, (int)(drawable.getIntrinsicWidth()*0.08),
                (int)(drawable.getIntrinsicHeight()*0.08));
        ScaleDrawable scaleDrawable = new ScaleDrawable(drawable,0,
                mViewHolder.comments.getWidth(),mViewHolder.comments.getHeight());
        mViewHolder.comments.setCompoundDrawables(scaleDrawable.getDrawable(), null, null, null);

        mViewHolder.commenters = post.getCommenters();
        mViewHolder.mNumberOfComments = post.getNumberOfComments();
        if (mViewHolder.commenters != null) {
            if (mViewHolder.commenters.contains(currentUser.getUsername())) {
                mViewHolder.comments.setBackgroundResource(R.drawable.buttonshape_pink);
            } else {
                mViewHolder.comments.setBackgroundResource(R.drawable.buttonshape_grey);
            }
        }
        String formattedString = mContext.getString(R.string.format_comments_likes, mViewHolder.mNumberOfComments);

        mViewHolder.comments.setText(formattedString);
    }

    private void setUpLikes(Post post, final String objectIdText){

        Drawable drawable = mContext.getResources().getDrawable(R.drawable.like_icon_white);
        drawable.setBounds(0,0, (int)(drawable.getIntrinsicWidth()*0.05),
                (int)(drawable.getIntrinsicHeight()*0.05));
        ScaleDrawable scaleDrawable = new ScaleDrawable(drawable,0,
                mViewHolder.numberOfLikes.getWidth(),mViewHolder.numberOfLikes.getHeight());
        mViewHolder.numberOfLikes.setCompoundDrawables(scaleDrawable.getDrawable(), null, null, null);

        // Check if already liked
        mViewHolder.likers = post.getLikedUsers();
        if (mViewHolder.likers != null) {
            if (mViewHolder.likers.contains(currentUser.getUsername())) {
                mViewHolder.liked = true;
                mViewHolder.numberOfLikes.setBackgroundResource(R.drawable.buttonshape_pink);
            } else {
                mViewHolder.liked = false;
                mViewHolder.numberOfLikes.setBackgroundResource(R.drawable.buttonshape_grey);
            }
        }

        mViewHolder.numberLiked = post.getLikes();
        String formattedString = mContext.getString(R.string.format_comments_likes, mViewHolder.numberLiked);
        if (mViewHolder.numberLiked != 0) {
            mViewHolder.numberOfLikes.setText(formattedString);
        } else {
            mViewHolder.numberOfLikes.setText(formattedString);
        }

    }

    private void setRandomProfilePicture(Post post){
        if (mViewHolder.profileImage.getDrawable() == null) {
            // Add Auto Profile Picture
            ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
            // generate random color
            int color1 = generator.getRandomColor();

            mViewHolder.profileImage.setVisibility(View.VISIBLE);
            String profileName = post.getAuthorUsername();
            if (profileName != null) {
                String[] parseName = profileName.split(" ", 0);
                if (parseName.length != 1) {
                    nameToDisplay = parseName[0].substring(0, 1) +
                            parseName[1].substring(0, 1);
                } else {
                    nameToDisplay = parseName[0].substring(0, 2);
                }

            } else {
                nameToDisplay = "NA";
            }

            TextDrawable drawable = TextDrawable.builder()
                    .beginConfig()
                    .textColor(Color.WHITE)
                    .useFont(Typeface.DEFAULT)
                    .fontSize(30) /* size in px */
                    .bold()
                    .toUpperCase()
                    .endConfig()
                    .buildRound(nameToDisplay, color1);

            mViewHolder.profileImage.setImageDrawable(drawable);
        }
    }

    public Post getItem(int position) {
        return mPost.get(position);
    }

    static class ViewHolder extends RecyclerView.ViewHolder{
        public ImageView postImage;
        public ImageView profileImage;
        public CircleImageView profileImageActualImage;
        public TextView authorName;
        public TextView locationName;
        public TextView textContent;
        public TextView dateString;
        public TextView objectIdString;
        public Button numberOfLikes;
        public Button comments;
        public Boolean liked = false;
        public List<String> likers = null;
        public int numberLiked = 0;
        public List<String> commenters = null;
        public boolean hasLikedBefore = false;
        public int mNumberOfComments = 0;
        public AdapterCallBack mListener;
        public List<String> mMentioned = null;

        public Context mContext;

        public ViewHolder(View v, Context context, AdapterCallBack adapter) {
            super(v);

            this.mListener = adapter;
            this.mContext = context;

            profileImageActualImage = (CircleImageView) v.findViewById(R.id.profile_picture_circle_imageView);
            postImage = (ImageView) v.findViewById(R.id.shared_image_imageView);
            profileImage = (ImageView) v.findViewById(R.id.profile_picture_imageView);
            authorName = (TextView) v.findViewById(R.id.username_textView);
            locationName = (TextView) v.findViewById(R.id.post_location_textView);
            textContent = (TextView) v.findViewById(R.id.comment_textView);
            dateString = (TextView) v.findViewById(R.id.post_time_textView);
            objectIdString = (TextView) v.findViewById(R.id.storeObjectId);
            numberOfLikes = (Button) v.findViewById(R.id.likeButton);
            comments = (Button) v.findViewById(R.id.commentButton);

            setUpLikeClickListeners();
            setUpCommentClickListeners();
        }

        private void setUpCommentClickListeners(){

            comments.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.startCommentsFragment(objectIdString.getText().toString(), authorName.getText().toString());
                }
            });
        }

        private void setUpLikeClickListeners() {
            numberOfLikes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!liked) {
                        ParseCloudCode.likePost(objectIdString.getText().toString());
                        liked = true;
                        likers.add(ParseUser.getCurrentUser().getUsername());
                        numberLiked += 1;
                        String formattedString = mContext.getString(R.string.format_comments_likes, numberLiked);
                        numberOfLikes.setText(formattedString);
                        numberOfLikes.setBackgroundResource(R.drawable.buttonshape_pink);
                        if (!hasLikedBefore) {
                            ParseCloudCode.pushNotification(authorName.getText().toString(),null,null, Constants.LIKE,
                                    objectIdString.getText().toString(),ParseUser.getCurrentUser().getUsername());
                        }
                    } else {
                        ParseCloudCode.unlikePost(ParseUser.getCurrentUser().toString(), objectIdString.getText().toString());
                        liked = false;
                        likers.remove(ParseUser.getCurrentUser().getUsername());
                        numberLiked -= 1;
                        String formattedString = mContext.getString(R.string.format_comments_likes, numberLiked);
                        numberOfLikes.setText(formattedString);
                        numberOfLikes.setBackgroundResource(R.drawable.buttonshape_grey);
                    }
                }
            });
        }


    }

}

