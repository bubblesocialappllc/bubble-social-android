package com.bubblesocialapp.bubble.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.common.Constants;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by sadaf on 8/1/15.
 */
public class SuggestionsAdapter extends ArrayAdapter<ParseObject> implements Filterable{

    private List<ParseObject> mSearchReturnObjects;
    private Context mContext;
    private ViewHolder mViewHolder;

    public SuggestionsAdapter(Context context, int resource, ArrayList<ParseObject> list) {
        super(context, resource, list);
        this.mContext = context;
        this.mSearchReturnObjects = list;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.suggestions_list, parent, false);
            mViewHolder = new ViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (ViewHolder) convertView.getTag();
        }

        setData(position);

        return convertView;
    }

    public void setData(int position) {
        if (getItemViewType(position) == Constants.EVENT_FLAG) {

            ParseFile mEventImage = (ParseFile) mSearchReturnObjects.get(position).get("eventPhoto");

            String eventUrl = mEventImage.getUrl();

            Picasso.with(mViewHolder.mImageView.getContext())
                    .load(eventUrl)
                    .error(R.drawable.ic_error)
                    .placeholder(R.drawable.ic_stub)
                    .resize(50, 50).centerCrop()
                    .into(mViewHolder.mImageView);

            String mEventName = mSearchReturnObjects.get(position).getString("eventName");

            mViewHolder.mTextView.setText(mEventName);
            mViewHolder.mTextView.setTextColor(mContext.getResources().getColor(R.color.bubble_pink));

        } else if (getItemViewType(position) == Constants.USER_FLAG) {

            ParseFile mUserImage = (ParseFile) mSearchReturnObjects.get(position).get("profilePicture");

            try {
                String eventUrl = mUserImage.getUrl();

                Picasso.with(mViewHolder.mImageView.getContext())
                        .load(eventUrl)
                        .error(R.drawable.ic_error)
                        .placeholder(R.drawable.ic_stub)
                        .resize(50, 50).centerCrop()
                        .into(mViewHolder.mImageView);
            } catch (NullPointerException e){
                e.getMessage();
            }
            String mUserName = mSearchReturnObjects.get(position).getString("username");
            mViewHolder.mTextView.setText(mUserName);

        }
    }

    public int getItemViewType(int position) {
        String className = mSearchReturnObjects.get(position).getClassName();
        switch (className) {
            case ("Event"): {
                return Constants.EVENT_FLAG;
            }case ("_User"): {
                return Constants.USER_FLAG;
            }
        }
        return 0;
    }

    public static class ViewHolder {

        public CircleImageView mImageView;
        public TextView mTextView;

        public ViewHolder(View itemView) {
            mImageView = (CircleImageView) itemView.findViewById(R.id.suggestion_box_profile_picture);
            mTextView = (TextView) itemView.findViewById(R.id.suggestion_box_username);
        }
    }

    @Override
    public Filter getFilter() {
        return newFilter();
    }

    // Since parse does all the filtering this does not do anything. However, this filter is still required
    // to get mentions to work.
    public Filter newFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                FilterResults filterResults = new FilterResults();
                filterResults.values = mSearchReturnObjects;
                return filterResults;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mSearchReturnObjects = (ArrayList<ParseObject>) filterResults.values;
                notifyDataSetChanged();
            }
        };
        return filter;
    }
}
