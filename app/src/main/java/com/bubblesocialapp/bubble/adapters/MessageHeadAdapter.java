package com.bubblesocialapp.bubble.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.common.Util;
import com.bubblesocialapp.bubble.parseObjects.MessageHead;

import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.Date;
import java.util.List;

/**
 * Created by sadaf on 4/16/15.
 */
public class MessageHeadAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<MessageHead> messageHeads;
    private List<ParseUser> mUsers;
    private Context mContext;
    private ViewHolder mViewHolder;
    private View view;
    private OnMessageClickListener mCallBack;

    public interface OnMessageClickListener {
        public void onMessageHeadClick(String objectId);
    }

    public MessageHeadAdapter(Context context, List<MessageHead> objects, List<ParseUser> users) {
        this.mContext = context;
        this.messageHeads = objects;
        this.mUsers = users;

        try {
            this.mCallBack = ((OnMessageClickListener) context);
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement MessageHead Callback.");
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(mContext).inflate(R.layout.fragment_messagehead_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        mViewHolder = (ViewHolder) holder;
        mViewHolder.position = position;
        String profilePicFile = mUsers.get(position).getParseFile("profilePicture").getUrl();
        if (profilePicFile != null) {
            Picasso.with(mViewHolder.profileImage.getContext())
                    .load(profilePicFile)
                    .error(R.drawable.ic_error)
                    .placeholder(R.drawable.ic_stub)
                    .into(mViewHolder.profileImage);
        }

        mViewHolder.lastMessage.setText(messageHeads.get(position).getLastMessage());

        mViewHolder.mUsername.setText(mUsers.get(position).getUsername());

        Date lastMessageDate = messageHeads.get(position).getLastMessageDate();

        if (lastMessageDate != null) {
            String time = Util.returnTime(lastMessageDate);
            mViewHolder.mTime.setText(time);
        } else {
            mViewHolder.mTime.setText("Now");
        }

        view.setTag(mViewHolder);

        setClickListeners(mViewHolder);
    }

    @Override
    public int getItemCount() {
        return messageHeads.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView lastMessage;
        public ImageView profileImage;
        public TextView mUsername;
        public TextView mTime;
        public Context mContext;
        public View mView;
        int position;

        public ViewHolder(View v){
            super(v);
            mView = v;
            mUsername = (TextView) v.findViewById(R.id.username);
            mTime = (TextView) v.findViewById(R.id.time_textview);
            lastMessage = (TextView) v.findViewById(R.id.last_message_box);
            profileImage = (ImageView) v.findViewById(R.id.profile_Image);
        }
    }

    public void setClickListeners(final ViewHolder viewholder){
        viewholder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallBack.onMessageHeadClick(messageHeads.get(viewholder.position).getObjectId());
            }
        });

        viewholder.mView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                setAlertDialog(viewholder.position, view);
                return true;
            }
        });
    }

    public void setAlertDialog(final int position, View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
        builder.setMessage("Delete")
                .setTitle("Delete");

        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                deleteThread(position);
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void deleteThread(int position){
        messageHeads.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, messageHeads.size());
        notifyDataSetChanged();
    }

}
