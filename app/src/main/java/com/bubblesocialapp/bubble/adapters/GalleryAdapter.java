package com.bubblesocialapp.bubble.adapters;

import android.content.Context;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.common.Util;
import com.bubblesocialapp.bubble.parseObjects.Photo;
import com.felipecsl.asymmetricgridview.library.widget.AsymmetricGridView;
import com.felipecsl.asymmetricgridview.library.widget.AsymmetricGridViewAdapter;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by sadaf on 5/17/15.
 */
public class GalleryAdapter extends AsymmetricGridViewAdapter<Photo> {

    private Context mContext;
    private List<Photo> mPhotos;
    private Point size;
    ImageView mImage;
    ProgressBar mProgressBar;

    public GalleryAdapter(Context context, AsymmetricGridView listView, List<Photo> items) {
        super(context, listView, items);
        this.mContext = context;
        this.mPhotos = items;

        try {
            WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            size = new Point();
            display.getSize(size);
            size.x = (int) Util.pxToDp(mContext, size.x);
            size.y = (int) Util.pxToDp(mContext, size.y);
        } catch (NullPointerException e) {
            Log.e("Null Window Manager", e.getMessage());
        }

    }

    @Override
    public View getActualView(int position, View convertView, ViewGroup parent) {


        if (convertView == null) {

            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_grid_image, parent, false);
        }

            mImage = (ImageView) convertView.findViewById(R.id.image);

        Photo photo = getItem(position);
        int columnSpan = photo.getColumnSpan();
        int rowSpan = photo.getColumnSpan();

        if (photo.getPhotoUrl() != null) {
            Picasso.with(mImage.getContext())
                    .load(photo.getPhotoUrl())
                    .placeholder(R.drawable.ic_stub)
                    .error(R.drawable.ic_error)
                    .resize(columnSpan * size.x / 3, 0)
                    .into(mImage);
        } else if (photo.getPhotoUri() != null) {
            mImage.setImageURI(photo.getPhotoUri());
        }

        setClickListener(mImage);

        return convertView;
    }

    public void setClickListener(ImageView view) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }


    @Override
    public Photo getItem(int position) {
        return mPhotos.get(position);
    }

}
