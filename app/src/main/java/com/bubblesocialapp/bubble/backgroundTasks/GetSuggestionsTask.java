package com.bubblesocialapp.bubble.backgroundTasks;

import android.os.AsyncTask;

import com.bubblesocialapp.bubble.adapters.SuggestionsAdapter;
import com.bubblesocialapp.bubble.interfaces.ReturnedResultsInterface;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sadaf on 8/1/15.
 */
public class GetSuggestionsTask extends AsyncTask<String,Void,List<ParseObject>> {

    private ReturnedResultsInterface resultsInterface;

    public GetSuggestionsTask(ReturnedResultsInterface delegate) {
        this.resultsInterface = delegate;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected List<ParseObject> doInBackground(String... strings) {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("_User");
//        query.whereNotEqualTo("username", ParseUser.getCurrentUser().getUsername());
        query.whereStartsWith("username", strings[0]);
        try {
            return query.find();
        } catch (ParseException e) {
            return null;
        }
    }

    @Override
    protected void onPostExecute(List<ParseObject> parseObjects) {
        resultsInterface.returnedResults(parseObjects);
    }
}
