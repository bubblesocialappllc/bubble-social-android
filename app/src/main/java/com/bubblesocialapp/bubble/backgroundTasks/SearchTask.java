package com.bubblesocialapp.bubble.backgroundTasks;

import android.content.Context;
import android.location.Address;
import android.location.Location;
import android.os.AsyncTask;

import com.bubblesocialapp.bubble.bean.HeaderItemBean;
import com.bubblesocialapp.bubble.common.Constants;
import com.bubblesocialapp.bubble.common.LocationController;
import com.bubblesocialapp.bubble.common.Util;
import com.bubblesocialapp.bubble.interfaces.ReturnSearchResultsInterface;
import com.bubblesocialapp.bubble.parseObjects.Event;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sadaf on 8/4/15.
 */
public class SearchTask extends AsyncTask<String, Void, List<HeaderItemBean>> implements LocationController.LocationCallBack{

    private ReturnSearchResultsInterface mInterface;
    private Context mContext;
    private Location mCurrentLocation;
    private List<ParseUser> mBuddies = null;
    private List<ParseUser> mPeopleAroundMe = null;
    private List<ParseUser> mAllBubblers;
    private List<Event> mEvents = null;
    private List<HeaderItemBean> mAllData = new ArrayList<HeaderItemBean>();
    private int mHeaderCount = 0;

    public SearchTask(Context context, ReturnSearchResultsInterface delegate) {
        this.mContext = context;
        this.mInterface = delegate;
    }

    @Override
    protected void onPreExecute() {
        LocationController locationController = new LocationController(mContext,this,LocationController.FIND_LOCATION);
        locationController.connectToGoogleApiClient();
    }

    @Override
    protected void onPostExecute(List<HeaderItemBean> headerItemBeans) {
        mInterface.onSearchResultsFound(headerItemBeans);
    }

    @Override
    protected List<HeaderItemBean> doInBackground(String... strings) {

        // Friends

        if (ParseUser.getCurrentUser().getList("buddies") != null) {
            ParseQuery<ParseUser> queryFriends = ParseUser.getQuery();
            queryFriends.whereContainedIn("objectId", ParseUser.getCurrentUser().getList("buddies"));
            queryFriends.whereStartsWith("username", strings[0]);

            ParseQuery<ParseUser> query2Friends = ParseUser.getQuery();
            query2Friends.whereContainedIn("objectId", ParseUser.getCurrentUser().getList("buddies"));
            query2Friends.whereStartsWith("realName", strings[0]);

            List<ParseQuery<ParseUser>> queriesFriends = new ArrayList<>();
            queriesFriends.add(queryFriends);
            queriesFriends.add(query2Friends);

            ParseQuery<ParseUser> queryBuddies = ParseQuery.or(queriesFriends);
            queryBuddies.setLimit(5);
            try {
              mBuddies = queryBuddies.find();
            } catch (ParseException e) {
                // log message
            }

        }

        // People Near You

        if (mCurrentLocation != null) {
            ParseQuery<ParseUser> queryPeopleNearYou = ParseUser.getQuery();
            queryPeopleNearYou.whereStartsWith("username", strings[0]);

            ParseQuery<ParseUser> query2PeopleNearYou = ParseUser.getQuery();
            query2PeopleNearYou.whereStartsWith("realName", strings[0]);

            List<ParseQuery<ParseUser>> queriesPeopleNearYou = new ArrayList<>();
            queriesPeopleNearYou.add(queryPeopleNearYou);
            queriesPeopleNearYou.add(query2PeopleNearYou);

            ParseQuery<ParseUser> mainQueryPeopleNearYou = ParseQuery.or(queriesPeopleNearYou);
            ParseGeoPoint point = new ParseGeoPoint(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
            mainQueryPeopleNearYou.whereWithinMiles("location", point, Util.getUserChosenRadius(mContext));
            mainQueryPeopleNearYou.setLimit(5);

            try {
                mPeopleAroundMe = mainQueryPeopleNearYou.find();
            } catch (ParseException e) {
                // Log message
            }

            // Events Near You
            ParseQuery<Event> queryEventsNearYou = new ParseQuery<>("Event");
            queryEventsNearYou.whereStartsWith("eventName", strings[0]);

            ParseQuery<Event> query2EventsNearYou = new ParseQuery<>("Event");
            query2EventsNearYou.whereStartsWith("locationName", strings[0]);

            List<ParseQuery<Event>> queriesEventsNearYou = new ArrayList<>();
            queriesEventsNearYou.add(queryEventsNearYou);
            queriesEventsNearYou.add(query2EventsNearYou);

            ParseQuery<Event> mainQueryEventsNearYou = ParseQuery.or(queriesEventsNearYou);
            mainQueryEventsNearYou.whereWithinMiles("location", point, Util.getUserChosenRadius(mContext));
            mainQueryEventsNearYou.setLimit(5);

            try {
                mEvents = mainQueryEventsNearYou.find();
            } catch (ParseException e) {
                // log message
            }

        }

        // All Bubblers
        ParseQuery<ParseUser> queryAllBubblers = ParseUser.getQuery();
        queryAllBubblers.whereStartsWith("username", strings[0]);
        ParseQuery<ParseUser> query2AllBubblers = ParseUser.getQuery();
        query2AllBubblers.whereStartsWith("realName", strings[0]);
        List<ParseQuery<ParseUser>> queriesAllBubblers = new ArrayList<>();
        queriesAllBubblers.add(queryAllBubblers);
        queriesAllBubblers.add(query2AllBubblers);

        ParseQuery<ParseUser> mainQueryAllBubblers = ParseQuery.or(queriesAllBubblers);
        mainQueryAllBubblers.setLimit(10);

        try {
            mAllBubblers = mainQueryAllBubblers.find();
        } catch (ParseException e) {
            // Log message
        }

        if (mBuddies != null) {
            if (mBuddies.size() != 0) {
                HeaderItemBean newItem = new HeaderItemBean(Constants.SEARCH_HEADER_BUDDIES);
                newItem.setHeaderPosition(mHeaderCount);
                newItem.isHeader = true;
                mAllData.add(newItem);
                for (ParseUser obj : mBuddies) {
                    HeaderItemBean addItem = new HeaderItemBean(obj);
                    addItem.isHeader = false;
                    addItem.setHeaderPosition(mHeaderCount);
                    mAllData.add(addItem);
                }
            }
        }

        if (mEvents != null) {
            if (mEvents.size() != 0) {
                mHeaderCount += 1;
                HeaderItemBean newItem = new HeaderItemBean(Constants.SEARCH_HEADER_EVENTS_NEAR_YOU);
                newItem.setHeaderPosition(mHeaderCount);
                newItem.isHeader = true;
                mAllData.add(newItem);
                for (Event obj : mEvents) {
                    HeaderItemBean addItem = new HeaderItemBean(obj);
                    addItem.isHeader = false;
                    addItem.setHeaderPosition(mHeaderCount);
                    mAllData.add(addItem);
                }
            }
        }

        if (mPeopleAroundMe != null) {
            if (mPeopleAroundMe.size() != 0) {
                mHeaderCount += 1;
                HeaderItemBean newItem = new HeaderItemBean(Constants.SEARCH_HEADER_PEOPLE_NEAR_YOU);
                newItem.setHeaderPosition(mHeaderCount);
                newItem.isHeader = true;
                mAllData.add(newItem);
                for (ParseUser obj : mPeopleAroundMe) {
                    HeaderItemBean addItem = new HeaderItemBean(obj);
                    addItem.isHeader = false;
                    addItem.setHeaderPosition(mHeaderCount);
                    mAllData.add(addItem);
                }
            }
        }

        if (mAllBubblers != null) {
            if (mAllBubblers.size() != 0) {
                mHeaderCount += 1;
                HeaderItemBean newItem = new HeaderItemBean(Constants.SEARCH_HEADER_ALL_BUBBLERS);
                newItem.setHeaderPosition(mHeaderCount);
                newItem.isHeader = true;
                mAllData.add(newItem);
                for (ParseUser obj : mAllBubblers) {
                    HeaderItemBean addItem = new HeaderItemBean(obj);
                    addItem.isHeader = false;
                    addItem.setHeaderPosition(mHeaderCount);
                    mAllData.add(addItem);
                }
            }
        }

        return mAllData;

    }

    @Override
    public void onAddressFound(List<Address> address) {

    }

    @Override
    public void onNewLocation(Location location, float accuracy) {
        mCurrentLocation = location;
    }
}
