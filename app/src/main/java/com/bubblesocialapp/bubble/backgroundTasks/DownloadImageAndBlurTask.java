package com.bubblesocialapp.bubble.backgroundTasks;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

import com.bubblesocialapp.bubble.common.Util;
import com.bubblesocialapp.bubble.customLayouts.blurPicture.BlurTransformation;
import com.bubblesocialapp.bubble.fragments.ProfileFragment;
import com.bubblesocialapp.bubble.interfaces.ReturnBitmapResults;
import com.parse.ParseException;
import com.parse.ParseFile;

/**
 * Created by sadaf on 8/3/15.
 */
public class DownloadImageAndBlurTask extends AsyncTask<ParseFile,Void,Bitmap> {

    private ReturnBitmapResults mInterface;

    public DownloadImageAndBlurTask(ReturnBitmapResults delegate) {
        this.mInterface = delegate;
    }

    @Override
    protected Bitmap doInBackground(ParseFile... parseFiles) {

        try {
            byte[] image = parseFiles[0].getData();
            Bitmap bitmap = Util.ByteToBitmap(image);
            Bitmap blurredBitmap = BlurTransformation.blurImage(bitmap, 50);
            return blurredBitmap;

        } catch (ParseException e) {
            Log.e("ParseException", e.getMessage());
        }

        return null;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        mInterface.returnBitmap(bitmap);


    }
}
