package com.bubblesocialapp.bubble.backgroundTasks;

import android.content.Context;
import android.location.Location;
import android.os.AsyncTask;

import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.interfaces.ReturnPlacesInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * Created by Sadaf Mackertich on 8/15/2015.
 */
public class GetPlacesTask extends AsyncTask<String, Void, String> {

    private Location mLocation;
    private String mPlacesSearchURL;
    private Context mContext;
    private ReturnPlacesInterface mInterface;

    public GetPlacesTask(Context context, Location location, ReturnPlacesInterface delegate) {
        this.mContext = context;
        this.mLocation = location;
        this.mInterface = delegate;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String string) {
        super.onPostExecute(string);
        try {
            JSONObject resultObject = new JSONObject(string);
            mInterface.onPlacesReturned(resultObject);
        } catch (JSONException e) {
            e.printStackTrace();
            mInterface.onPlacesReturned(null);
        }
    }

    @Override
    protected String doInBackground(String... params) {
        StringBuilder placesBuilder = new StringBuilder();

        mPlacesSearchURL = "https://maps.googleapis.com/maps/api/place/nearbysearch/" +
                "json?location=" + mLocation.getLatitude() + "," + mLocation.getLongitude() +
                "&radius=16000" +
                "&types=bar%7Cnight_club" +
                "&key=" + mContext.getResources().getString(R.string.google_server_key) +
                "&sensor=true";

        try {
            URL url = new URL(mPlacesSearchURL);
            HttpURLConnection placesFound = (HttpURLConnection) url.openConnection();
            placesFound.connect();

            if (placesFound.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStream placesContent = placesFound.getInputStream();
                InputStreamReader placesInput = new InputStreamReader(placesContent);
                BufferedReader placesReader = new BufferedReader(placesInput);
                String lineIn;
                while ((lineIn = placesReader.readLine()) != null) {
                    placesBuilder.append(lineIn);
                }
                return placesBuilder.toString();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}



















