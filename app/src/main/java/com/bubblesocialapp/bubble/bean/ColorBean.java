package com.bubblesocialapp.bubble.bean;

/**
 * Created by sadaf on 6/8/15.
 */
public class ColorBean {
    private int mColor;

    public ColorBean(int color){
        this.mColor = color;
    }

    public void setItemColor(int color){
        mColor = color;
    }

    public int getItemColor(){
        return mColor;
    }
}
