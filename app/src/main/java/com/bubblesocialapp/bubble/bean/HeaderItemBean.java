package com.bubblesocialapp.bubble.bean;

import com.bubblesocialapp.bubble.parseObjects.Event;
import com.bubblesocialapp.bubble.parseObjects.Post;
import com.parse.ParseUser;

/**
 * Created by sadaf on 8/4/15.
 */
public class HeaderItemBean {

    private String mHeader;
    private ParseUser mParseUser;
    public boolean isHeader;
    private int mPosition;
    private Event mEvent;

    public HeaderItemBean(String header) {
        this.mHeader = header;
    }

    public HeaderItemBean(ParseUser parseUser) {
        this.mParseUser = parseUser;
    }

    public HeaderItemBean(Event event) {
        this.mEvent = event;
    }

    public ParseUser getParseUser() {
        return mParseUser;
    }

    public Event getEvent() {
        return mEvent;
    }

    public String getHeader() {
        return mHeader;
    }

    public boolean isHeader(){
        return isHeader;
    }

    public void setHeaderPosition(int position){
        this.mPosition = position;
    }

    public int getHeaderPosition() {
        return mPosition;
    }

}
