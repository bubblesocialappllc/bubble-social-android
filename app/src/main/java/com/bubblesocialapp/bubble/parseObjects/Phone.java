package com.bubblesocialapp.bubble.parseObjects;

import com.parse.ParseClassName;
import com.parse.ParseObject;

/**
 * Created by sadaf on 4/8/15.
 */
@ParseClassName("Phone")
public class Phone extends ParseObject{

    public Phone(){}

    public String getPhoneNumber(){
        return getString("phoneNumber");
    }

    public void setPhoneNumber(String number){
        put("phoneNumber", number);
    }

    public Number getVerificationCode(){
        return getNumber("verificationCode");
    }

}
