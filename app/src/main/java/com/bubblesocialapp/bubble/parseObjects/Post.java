package com.bubblesocialapp.bubble.parseObjects;

import android.net.Uri;

import com.bubblesocialapp.bubble.common.Util;
import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Sadaf Mackertich on 11/6/2014.
 */
@ParseClassName("Post")
public class Post extends ParseObject{

    private Uri postImageUri;

    public Post(){

    }

    public ParseObject getAuthor() {
        return getParseObject("author");
    }

    public void setAuthor(ParseObject author){
        put("author", author);
    }

    public List<String> getCommenters() {
        return getList("commenters");
    }

    public void setCommenters(String commenter) {
        addUnique("commenters",commenter);
    }

    public int getNumberOfComments(){
        return getInt("comments");
    }

    public void setNumberOfComments(int number){
        put("comments", number);
    }

    public void setHasLikedUser(String value){
        addUnique("hasLikedBefore", value);
    }

    public List<String> getHasLikedUsers(){
        return getList("hasLikedBefore");
    }

    public String getHashTag(){
        return getString("hashtag");
    }

    public void setHashTag(String hashTag){
        put("hashtag", hashTag);
    }

    public void setLikedUser(String value){
        addUnique("likers", value);
    }

    public List<String> getLikedUsers(){
        return getList("likers");
    }

    public int getLikes() {
        return getInt("likes");
    }

    public void setLike(int likes){
        put("likes", likes);
    }

    public String getPostTime(){
        if (getCreatedAt() != null) {
            return Util.returnTime(getCreatedAt());
        } else {
            return "Now";
        }
    }

    public void setPostTime(){
        put("createdAt", "Now");
    }

    public String getLocationName(){
        return getString("locationName");
    }

    public void setLocationName(String name){
        put("locationName", name);
    }

    public ParseGeoPoint getLocation(){
        return getParseGeoPoint("location");
    }

    public void setLocation(ParseGeoPoint point){
        put("location", point);
    }

    public List<String> getMentioned(){
        return getList("mentioned");
    }

    public void setMentioned(List<String> mentioned){
        addAllUnique("mentioned", mentioned);
    }

    public int getPoints(){
        return getInt("points");
    }

    public void setPoints(int points){
        put("points", points);
    }

    public String getProfilePicURL(){
        ParseFile file = getParseFile("profilePic");
        if (file != null) {
            return file.getUrl();
        }
        return null;
    }

    public void setProfilePic(ParseFile file) {
        put("profilePic", file);
    }

    public List<String> getReporters(){
        return getList("reporters");
    }

    public void setReporters(String reporter){
        addUnique("reporters", reporter);
    }

    public String getAuthorId(){
        return getString("AuthorId");
    }

    public void setAuthorId(String author){
        put("authorId", author);
    }

    public String getAuthorUsername(){
        return getString("authorUsername");
    }

    public void setAuthorUsername(String author){
        put("authorUsername", author);
    }

    public String getPostImageURL() {
        ParseFile file = getParseFile("photo");
        if (file != null) {
            return file.getUrl();
        }
        return null;
    }

    public Uri getPostImageUri() {
        return postImageUri;
    }

    public void setPostImage(ParseFile file) {
        put("photo", file);
    }

    public void setPostImage(Uri uri) {
        postImageUri = uri;
    }

    public String getText(){
        return getString("text");
    }

    public void setText(String text){
        put("text", text);
    }

}

