package com.bubblesocialapp.bubble.parseObjects;

import android.app.usage.UsageEvents;
import android.location.Location;

import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;

/**
 * Created by sadaf on 8/4/15.
 */
public class Event extends ParseObject {

    public Event(){}

    public String getEventName() {
        return getString("eventName");
    }

    public void setEventName(String event) {
        put("eventName", event);
    }

    public ParseFile getEventPhoto(){
        return getParseFile("eventPhoto");
    }

    public void setEventPhoto(ParseFile file){
        put("eventPhoto", file);
    }

    public ParseGeoPoint getLocation() {
        return getParseGeoPoint("location");
    }

    public void setLocation(ParseGeoPoint location){
        put("location", location);
    }

    public String getLocationName(){
        return getString("locationName");
    }

    public void setLocationName(String name) {
        put("locationName", name);
    }

    public int getNumberOfPeople(){
        return getInt("numberOfPeople");
    }

    public int getNumberOfPosts(){
        return getInt("numberOfPosts");
    }

}
