package com.bubblesocialapp.bubble.parseObjects;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import java.util.List;

/**
 * Created by sadaf on 4/8/15.
 */
@ParseClassName("MessageUser")
public class MessageUser extends ParseObject {

    public MessageUser(){}

    public String getLastMessage(){
        return getString("lastMessage");
    }

    public void setLastMessage(String text){
        put("lastMessage",text);
    }

    public ParseObject getUser(){
        return getParseObject("user");
    }

    public void setUser(ParseObject user){
        put("user", user);
    }

    public List<String> getTalkingTo(){
        return getList("talkingTo");
    }

    public void setTalkingTo(String objectId){
        addUnique("talkingTo", objectId);
    }

    public List<String> getTalkingToUsernames(){
        return getList("talkingToUsernames");
    }

    public void setTalkingToUsernames(String username){
        addUnique("talkingToUsernames", username);
    }

}

