package com.bubblesocialapp.bubble.parseObjects;

import com.bubblesocialapp.bubble.common.Util;
import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseObject;

import java.util.Calendar;

/**
 * Created by Sadaf Mackertich on 11/9/2014.
 */
@ParseClassName("Comment")
public class Comment extends ParseObject {

    public Comment(){}

    public String getComment(){
        return getString("comment");
    }

    public void setComment(String comment) {
        put("comment", comment);
    }

    public String getCommenter(){
        return getString("commenter");
    }

    public void setCommenter(String authorId){
        put("commenter", authorId);
    }

     public String getLocationName(){
        return getString("locationName");
     }

    public void setLocationName(String location){
        put("locationName", location);
    }

    public String getPostImageURL() {
        ParseFile file = getParseFile("photo");
        if (file != null) {
            return file.getUrl();
        }
        return null;
    }

    public void setPhoto(ParseFile file) {
        put("photo", file);
    }

    public String getProfilePicURL(){
        ParseFile file = getParseFile("profilePic");
        if (file != null) {
            return file.getUrl();
        }
        return null;
    }

    public void setProfilePicture(ParseFile file) {
        put("profilePic", file);
    }

    public void setPostId(ParseObject object) {
        put("post", object);
    }

    public String getPostTime(){
        if (getCreatedAt() !=null) {
            return Util.returnTime(getCreatedAt());
        } else {
            return "Now";
        }
    }

}
