package com.bubblesocialapp.bubble.parseObjects;

import com.parse.ParseClassName;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;

/**
 * Created by sadaf on 4/8/15.
 */
@ParseClassName("Registered_Businesses")
public class Registered_Businesses extends ParseObject{

    public Registered_Businesses(){}

    public ParseGeoPoint getLocation(){
        return getParseGeoPoint("location");
    }

    public void setLocation(ParseGeoPoint geoPoint){
        put("location", geoPoint);
    }

    public String getBusinessName(){
        return getString("nameOfBusiness");
    }

    public void setBusinessName(String name){
        put("nameOfBusiness", name);
    }
}
