package com.bubblesocialapp.bubble.parseObjects;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import java.util.Calendar;
import java.util.Date;

@ParseClassName("Notification")
public class Notification extends ParseObject {

    private Date postDate;
    private Calendar currentDate;
    public Notification(){}

    public ParseObject getPost(){
        return getParseObject("post");
    }

    public void setPost(ParseObject post){
        put("post", post);
    }

    public String getQuery(){
        return getString("query");
    }

    public void setQuery(String query){
        put("query", query);
    }

    public String getSendTo(){
        return getString("sendTo");
    }

    public void setSendTo(String author){
        put("sendTo", author);
    }

    public String getSentFrom(){
        return getString("sentFrom");
    }

    public void setSentFrom(String author){
        put("sentFrom", author);
    }

    public String getNotificationType(){
        return getString("type");
    }

    public void setNotificationType(String type){
        put("type", type);
    }

}
