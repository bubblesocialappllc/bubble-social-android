package com.bubblesocialapp.bubble.parseObjects;

import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;

/**
 * Created by sadaf on 4/8/15.
 */
@ParseClassName("MessageHolder")
public class MessageHolder extends ParseObject {

    public MessageHolder(){}

    public String getMessageText(){
        return getString("messageText");
    }

    public void setMessageText(String text){
        put("messageText", text);
    }

    public ParseFile getPicture(){
        return getParseFile("picture");
    }

    public void setPicture(ParseFile file){
        put("picture", file);
    }

    public ParseGeoPoint getLocation(){
        return getParseGeoPoint("shareLocation");
    }

    public void setLocation(ParseGeoPoint location){
        put("shareLocation", location);
    }

    public ParseObject getSender(){
        return getParseObject("sender");
    }

    public void setSender(ParseObject user){
        put("sender", user);
    }
}




















