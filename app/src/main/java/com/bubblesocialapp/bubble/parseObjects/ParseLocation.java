package com.bubblesocialapp.bubble.parseObjects;

import com.parse.ParseClassName;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;

import java.util.List;

/**
 * Created by Sadaf on 4/6/2015.
 */
@ParseClassName("Location")
public class ParseLocation extends ParseObject {

    public ParseLocation(){}

    public int getCount(){
        return getInt("count");
    }

    public void setCount(int count){
        put("count", count);
    }

    public ParseGeoPoint getLocation(){
        return getParseGeoPoint("geoPoint");
    }

    public void setLocation(ParseGeoPoint GeoPoint){
        put("geoPoint", GeoPoint);
    }

    public String getName(){
        return getString("name");
    }

    public void setName(String name){
        put("name", name);
    }

    public String getPlaceId(){
        return getString("placeId");
    }

    public void setPlaceId(String placeId){
        put("placeId", placeId);
    }

    public List<String> getPosts(){
        return getList("posts");
    }

    public void setPosts(String postId){
        addUnique("post", postId);
    }


}
