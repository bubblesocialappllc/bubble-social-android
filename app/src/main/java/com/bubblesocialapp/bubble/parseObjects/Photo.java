package com.bubblesocialapp.bubble.parseObjects;

import android.net.Uri;
import android.os.Parcel;

import com.felipecsl.asymmetricgridview.library.model.AsymmetricItem;
import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseObject;

import java.util.List;

/**
 * Created by sadaf on 4/8/15.
 */
@ParseClassName("Photo")
public class Photo extends ParseObject implements AsymmetricItem{

    private int columnSpan;
    private int rowSpan;
    private int position;
    private Uri photoUri;

    public Photo(){}

    public List<String> getComments(){
        return getList("comments");
    }

    public void setComments(String comment){
        addUnique("comments", comment);
    }

    public String getPhotoUrl(){
        return getParseFile("photo").getUrl();
    }

    public void setPhotoUri(Uri uri){
        this.photoUri = uri;
    }

    public Uri getPhotoUri(){
        return photoUri;
    }

    public void setPhoto(ParseFile file) {
        put("photo", file);
    }

    public String getPost(){
        return getString("post");
    }

    public void setPost(String post){
        put("post", post);
    }

    public List<String> getTags(){
        return getList("tags");
    }

    public void setTags(String tag){
        addUnique("tags", tag);
    }

    public ParseObject getUser(){
        return getParseObject("user");
    }

    public void setUser(ParseObject user){
        put("user", user);
    }


    @Override
    public int getColumnSpan() {
        return columnSpan;
    }

    @Override
    public int getRowSpan() {
        return rowSpan;
    }

    public void setColumnSpan(int columnSpan) {
        this.columnSpan = columnSpan;
    }

    public void setRowSpan(int rowSpan) {
        this.rowSpan = rowSpan;
    }

    public void setPosition(int position) {
        this.position = position;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(columnSpan);
        parcel.writeInt(rowSpan);
        parcel.writeInt(position);
    }

    private void readFromParcel(Parcel in) {
        columnSpan = in.readInt();
        rowSpan = in.readInt();
        position = in.readInt();
    }
}
