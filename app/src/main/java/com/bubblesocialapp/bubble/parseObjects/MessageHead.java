package com.bubblesocialapp.bubble.parseObjects;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import java.util.Date;
import java.util.List;

/**
 * Created by sadaf on 4/8/15.
 */
@ParseClassName("MessageHead")
public class MessageHead extends ParseObject {

    public MessageHead(){}

    public String getLastMessage(){
        return getString("lastMessage");
    }

    public void setLastMessage(String text){
        put("lastMessage",text);
    }

    public ParseObject getUser(){
        return getParseObject("firstUser");
    }

    public void setUser(ParseObject user){
        put("firstUser", user);
    }

    public ParseObject getSecondUser(){
        return getParseObject("secondUser");
    }

    public void setSecondUser(ParseObject objectId){
        put("secondUser", objectId);
    }

    public String getFirstUserUsername(){
        return getString("firstUserUsername");
    }

    public void setFirstUserUsername(String username){
        put("firstUserUsername", username);
    }

    public String getSecondUserUsername(){
        return getString("secondUserUsername");
    }

    public void setSecondUserUsername(String username){
        put("secondUserUsername", username);
    }

    public Date getLastMessageDate(){
        return getDate("lastMessageDate");
    }

    public void setLastMessageDate(Date date){
        put("lastMessageDate", date);
    }

}

