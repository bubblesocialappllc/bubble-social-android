package com.bubblesocialapp.bubble.parseObjects;

import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseUser;

/**
 * Created by sadaf on 4/8/15.
 */
@ParseClassName("Message")
public class Message extends ParseObject {

    public Message(){}

    public String getMessageText(){
        return getString("text");
    }

    public void setMessageText(String text){
        put("text", text);
    }

    public ParseFile getPicture(){
        return getParseFile("photo");
    }

    public void setPicture(ParseFile file){
        put("photo", file);
    }

    public ParseGeoPoint getLocation(){
        return getParseGeoPoint("location");
    }

    public void setLocation(ParseGeoPoint location){
        put("location", location);
    }

    public ParseObject getParent(){
        return getParseObject("parent");
    }

    public void setParent(ParseObject user){
        put("parent", user);
    }

    public ParseUser getSender(){
        return getParseUser("sender");
    }

    public void setSender(ParseUser user){
        put("sender", user);
    }
}




















