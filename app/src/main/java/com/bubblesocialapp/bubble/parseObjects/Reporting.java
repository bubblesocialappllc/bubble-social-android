package com.bubblesocialapp.bubble.parseObjects;

import com.parse.ParseClassName;
import com.parse.ParseObject;

/**
 * Created by sadaf on 4/8/15.
 */
@ParseClassName("Reporting")
public class Reporting extends ParseObject {

    public Reporting(){
        // Default Constructor
    }

    public ParseObject getPhoto() {
        return getParseObject("photo");
    }

    public void setPhoto(ParseObject photo){
        put("photo", photo);
    }

    public ParseObject getPost() {
        return getParseObject("post");
    }

    public void setPost(ParseObject post){
        put("post", post);
    }

    public String getReason(){
        return getString("reason");
    }

    public void setReason(String reason){
        put("reason", reason);
    }

    public ParseObject getReporter() {
        return getParseObject("reporter");
    }

    public void setReporter(ParseObject reporter){
        put("author", reporter);
    }

}
