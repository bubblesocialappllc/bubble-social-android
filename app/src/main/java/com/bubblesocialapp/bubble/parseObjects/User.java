package com.bubblesocialapp.bubble.parseObjects;

import android.net.Uri;

import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Sadaf on 1/18/2015.
 */

public class User {

    private String username;
    private String AboutMe;
    private Date birthday;
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private String color;
    private boolean beta;
    private boolean staff;
    private String gender;
    private String password;
    private String profilePic;

    public User(){

    }

    public String getUserName() {
        return username;
    }

    public void setUserName(String name) {
        username = name;
    }

    public String getAboutMe() {
        return AboutMe;
    }

    public void setAboutMe(String text) {
        AboutMe = text;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String text) {
        color = text;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String text) {
        email = text;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date date) {
        birthday = date;
    }

    public boolean getBeta() {
        return beta;
    }

    public void setBeta(boolean bool) {
        beta = bool;
    }

    public boolean getStaff() {
        return staff;
    }

    public void setStaff(boolean bool) {
        staff = bool;
    }

    public String getProfilePic(){
        return profilePic;
    }

    public void setProfilePic(String uri) {
        profilePic = uri;
    }

    public String getRealName() {
        return firstName+" "+lastName;
    }

    public void setRealName(String name){

        String[] nameComponents = name.split(" ",2);
        if(nameComponents.length==2){
            firstName = nameComponents[0];
            lastName = nameComponents[1];
        }else{
            firstName = nameComponents[0];
            lastName = "";
        }
    }

    public String getPhoneNumber(){ return phoneNumber;}

    public void setPhoneNumber(String phone){ phoneNumber = phone; }

    public String getPassword() {
        return password;
    }

    public void setPassword(String pass){
        password = pass;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String sex){
        gender = sex;
    }
}
