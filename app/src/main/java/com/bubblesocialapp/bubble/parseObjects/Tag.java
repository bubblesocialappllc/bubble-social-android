package com.bubblesocialapp.bubble.parseObjects;

import com.parse.ParseClassName;
import com.parse.ParseObject;

/**
 * Created by sadaf on 4/8/15.
 */
@ParseClassName("Tag")
public class Tag extends ParseObject{

    public Tag(){}

    public String getPoint(){
        return getString("point");
    }

    public void setPoint(String point){
        put("point", point);
    }

    public String getText(){
        return getString("text");
    }

    public void setText(String text){
        put("text", text);
    }

}
