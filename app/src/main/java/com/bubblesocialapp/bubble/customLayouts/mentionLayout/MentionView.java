package com.bubblesocialapp.bubble.customLayouts.mentionLayout;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;

import com.bubblesocialapp.bubble.R;
import com.parse.ParseObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sadaf on 8/10/15.
 */
public class MentionView extends FrameLayout implements View.OnClickListener{

    protected RecyclerView mRecyclerView;
    protected MentionAdapter mMentionAdapter;
    private ArrayList<ParseObject> mParseObjects = new ArrayList<>();
    private LinearLayoutManager mLinearLayoutManager;

    public MentionView(Context context) {
        super(context);
    }

    public MentionView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MentionView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public MentionView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        addView(LayoutInflater.from(getContext()).inflate(R.layout.mention_recycler_view, this, false));
        mRecyclerView = (RecyclerView) findViewById(R.id.mention_recycler_view);
        loadInfoView();
        mMentionAdapter = new MentionAdapter(getContext(), mParseObjects);
        mRecyclerView.setAdapter(mMentionAdapter);
    }

    @Override
    public void onClick(View view) {

    }

    private void loadInfoView() {
        mLinearLayoutManager = new LinearLayoutManager(getContext());
        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
    }

    public void setMentionAdapter(List<ParseObject> objects) {
        mParseObjects.clear();
        mParseObjects.addAll(objects);
        mMentionAdapter.notifyDataSetChanged();
    }

    public MentionAdapter getMentionAdapter() {
        return mMentionAdapter;
    }

}
