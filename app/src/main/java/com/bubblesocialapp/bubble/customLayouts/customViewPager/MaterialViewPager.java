package com.bubblesocialapp.bubble.customLayouts.customViewPager;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.beardedhen.androidbootstrap.BootstrapEditText;
import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.customLayouts.customSlidingTab.SlidingTabLayout;
import com.bubblesocialapp.bubble.fragments.MapFragment;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by florentchampigny on 28/04/15.
 * <p/>
 * The main class of MaterialViewPager
 * To use in an xml layout with attributes viewpager_*
 * <p/>
 * Display a preview with header, actual logo and fake cells
 */
public class MaterialViewPager extends FrameLayout implements ViewPager.OnPageChangeListener {

    /**
     * the layout containing tabs
     * default : add @layout/material_view_pager_pagertitlestrip_standard
     * with viewpager_pagerTitleStrip you can set your own layout
     */
    private ViewGroup pagerTitleStripContainer;

    /**
     * the layout containing logo
     * default : empty
     * with viewpager_logo you can set your own layout
     */
    private ViewGroup logoContainer;

    /**
     * Contains all references to MatervialViewPager's header views
     */
    protected MaterialViewPagerHeader materialViewPagerHeader;

    //the child toolbar
    protected Toolbar mToolbar;

    protected BootstrapEditText mSearchTextView;

    //the child viewpager
    protected ViewPager mViewPager;

    //a view used to add placeholder color below the header
    protected View headerBackground;

    //a view used to add fading color over the headerBackgroundContainer
    protected View toolbarLayoutBackground;

    //Class containing the configuration of the MaterialViewPager
    protected MaterialViewPagerSettings settings = new MaterialViewPagerSettings();


    protected FrameLayout mMapFrameLayout;

    protected View mProfileView;

    protected CircleImageView profileCircleImage;

    protected Button mSearchButton;

    protected ToggleButton mLocationToggle;

    protected TextView profileImagePopularityPoints;

    //region construct

    public MaterialViewPager(Context context) {
        super(context);
    }

    public MaterialViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        settings.handleAttributes(context, attrs);
    }

    public MaterialViewPager(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        settings.handleAttributes(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public MaterialViewPager(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        settings.handleAttributes(context, attrs);
    }

    //endregion

    @Override
    protected void onDetachedFromWindow() {
        MaterialViewPagerHelper.unregister(getContext());
        super.onDetachedFromWindow();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        //add @layout/material_view_pager_layout as child, containing all the MaterialViewPager views
        addView(LayoutInflater.from(getContext()).inflate(R.layout.material_view_pager_layout, this, false));

        pagerTitleStripContainer = (ViewGroup) findViewById(R.id.pagerTitleStripContainer);
        logoContainer = (ViewGroup) findViewById(R.id.logoContainer);

        mToolbar = (Toolbar) findViewById(R.id.main_activity_toolbar);

        mSearchTextView = (BootstrapEditText) findViewById(R.id.search_textView);

        mSearchButton = (Button) findViewById(R.id.search_button);

        mSearchTextView.setVisibility(View.GONE); //Expand during map collapse

        mSearchTextView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        mViewPager = (ViewPager) findViewById(R.id.viewPager);

        mViewPager.addOnPageChangeListener(this);

        mMapFrameLayout = (FrameLayout) findViewById(R.id.map_frame_layout);

        mLocationToggle = (ToggleButton) findViewById(R.id.toggle_location_button);

        if (isInEditMode()) { //preview titlestrip
            //add fake tabs on edit mode
            settings.pagerTitleStripId = R.layout.tools_material_view_pager_pagertitlestrip;
        }

        if (settings.pagerTitleStripId != -1) {
            pagerTitleStripContainer.addView(LayoutInflater.from(getContext())
                    .inflate(settings.pagerTitleStripId, pagerTitleStripContainer, false));
        }

        if (settings.logoLayoutId != -1) {
            mProfileView = LayoutInflater.from(getContext()).inflate(settings.logoLayoutId, logoContainer, false);
            logoContainer.addView(mProfileView);
            if (settings.logoMarginTop != 0) {
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) logoContainer.getLayoutParams();
                layoutParams.setMargins(0, settings.logoMarginTop, 0, 0);
                logoContainer.setLayoutParams(layoutParams);
                logoContainer.setVisibility(GONE); //Removed from all views except for profile
            }
        }
        profileCircleImage = (CircleImageView) mProfileView.findViewById(R.id.profile_picture_header_image);
        profileImagePopularityPoints = (TextView) mProfileView.findViewById(R.id.popularity_points_profile_view);
        headerBackground = findViewById(R.id.headerBackground);
        toolbarLayoutBackground = findViewById(R.id.toolbar_layout_background);

        initialiseHeights();

        //construct the materialViewPagerHeader with subviews
        if (!isInEditMode()) {
            materialViewPagerHeader = MaterialViewPagerHeader
                    .withToolbar(mToolbar)
                    .withToolbarLayoutBackground(toolbarLayoutBackground)
                    .withPagerSlidingTabStrip(pagerTitleStripContainer)
                    .withHeaderBackground(headerBackground)
                    .withStatusBackground(findViewById(R.id.statusBackground))
                    .withLogo(logoContainer)
                    .withSearchTextView(mSearchTextView)
                    .withSearchButton(mSearchButton);

            //and construct the MaterialViewPagerAnimator
            //attach it to the activity to enable MaterialViewPagerHeaderView.setMaterialHeight();
            MaterialViewPagerHelper.register(getContext(), new MaterialViewPagerAnimator(this));
        } else {

            //if in edit mode, add fake cardsviews
            View sample = LayoutInflater.from(getContext()).inflate(R.layout.tools_list_items, pagerTitleStripContainer, false);

            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) sample.getLayoutParams();
            int marginTop = Math.round(Utils.dpToPx(settings.headerHeight + 10, getContext()));
            params.setMargins(0, marginTop, 0, 0);
            super.setLayoutParams(params);

            addView(sample);
        }
    }

    public void addProfilePicture(String url){
        Picasso.with(profileCircleImage.getContext())
                .load(url)
                .error(R.drawable.ic_error)
                .resize(200,200)
                .placeholder(R.drawable.ic_stub)
                .into(profileCircleImage);

        profileImagePopularityPoints.setText(ParseUser.getCurrentUser().getNumber("points").toString());
    }

    private void initialiseHeights() {
        if (headerBackground != null) {
            headerBackground.setBackgroundColor(this.settings.color);

            ViewGroup.LayoutParams layoutParams = headerBackground.getLayoutParams();
            layoutParams.height = (int) Utils.dpToPx(this.settings.headerHeight + settings.headerAdditionalHeight, getContext());
            headerBackground.setLayoutParams(layoutParams);
        }
        if (pagerTitleStripContainer != null) {
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) pagerTitleStripContainer.getLayoutParams();
            int marginTop = (int) Utils.dpToPx(this.settings.headerHeight - 10, getContext());
            layoutParams.setMargins(0, marginTop, 0, 0);
            pagerTitleStripContainer.setLayoutParams(layoutParams);
        }
        if (toolbarLayoutBackground != null) {
            ViewGroup.LayoutParams layoutParams = toolbarLayoutBackground.getLayoutParams();
            layoutParams.height = (int) Utils.dpToPx(this.settings.headerHeight, getContext());
            toolbarLayoutBackground.setLayoutParams(layoutParams);
        }
    }

    public View getPopularityView() {
        return profileImagePopularityPoints;
    }

    public View getSearchTextView() {
        return mSearchTextView;
    }

    public Button getSearchButton() {
        return mSearchButton;
    }

    /**
     * Retrieve the displayed viewpager, don't forget to use
     * getPagerTitleStrip().setAdapter(materialviewpager.getViewPager())
     * after set an adapter
     *
     * @return the displayed viewpager
     */
    public ViewPager getViewPager() {
        return mViewPager;
    }

    public ToggleButton getLocationToggleButton() {
        return mLocationToggle;
    }

    /**
     * Retrieve the displayed tabs
     *
     * @return the displayed tabs
     */
    public SlidingTabLayout getPagerTitleStrip() {
        return (SlidingTabLayout) pagerTitleStripContainer.findViewById(R.id.materialviewpager_pagerTitleStrip);
    }

    /**
     * Retrieve the displayed toolbar
     *
     * @return the displayed toolbar
     */
    public Toolbar getToolbar() {
        return mToolbar;
    }

    public CircleImageView getProfileImageView() {
        return profileCircleImage;
    }


    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();

        SavedState ss = new SavedState(superState);
        //end
        ss.settings = this.settings;
        ss.yOffset = MaterialViewPagerHelper.getAnimator(getContext()).lastYOffset;

        return ss;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        SavedState ss = (SavedState) state;
        super.onRestoreInstanceState(ss.getSuperState());

        this.settings = ss.settings;
        if (headerBackground != null)
            headerBackground.setBackgroundColor(this.settings.color);

        MaterialViewPagerAnimator animator = MaterialViewPagerHelper.getAnimator(this.getContext());

        //-1*ss.yOffset restore to 0
        animator.restoreScroll(-1 * ss.yOffset, ss.settings);
        MaterialViewPagerHelper.register(getContext(), animator);
    }

    static class SavedState extends BaseSavedState {
        public MaterialViewPagerSettings settings;
        public float yOffset;

        SavedState(Parcelable superState) {
            super(superState);
        }

        private SavedState(Parcel in) {
            super(in);
            this.settings = in.readParcelable(MaterialViewPagerSettings.class.getClassLoader());
            this.yOffset = in.readFloat();
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeParcelable(this.settings, flags);
            out.writeFloat(this.yOffset);
        }

        //required field that makes Parcelables from a Parcel
        public static final Parcelable.Creator<SavedState> CREATOR =
                new Parcelable.Creator<SavedState>() {
                    public SavedState createFromParcel(Parcel in) {
                        return new SavedState(in);
                    }

                    public SavedState[] newArray(int size) {
                        return new SavedState[size];
                    }
                };
    }

    public void setMapFragment(FragmentManager manager) {
        if (mMapFrameLayout != null) {
            MapFragment mMapFragment = new MapFragment();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.add(R.id.map_frame_layout, mMapFragment);
            transaction.commit();
        }
    }

    int lastPosition = 0;

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if (positionOffset >= 0.5) {
            onPageSelected(position + 1);
        } else if (positionOffset <= -0.5) {
            onPageSelected(position - 1);
        } else {
            onPageSelected(position);
        }
        if (position == 2) {
            logoContainer.setVisibility(VISIBLE);
        }
//        else {
//            logoContainer.setVisibility(GONE);
//        }
    }

    @Override
    public void onPageSelected(int position) {
        if (position == 2) {
            logoContainer.setVisibility(VISIBLE);
        } else {
            logoContainer.setVisibility(GONE);
        }

        lastPosition = position;
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        if (settings.displayToolbarWhenSwipe) {
            MaterialViewPagerHelper.getAnimator(getContext()).onViewPagerPageChanged();
        }
    }
}

