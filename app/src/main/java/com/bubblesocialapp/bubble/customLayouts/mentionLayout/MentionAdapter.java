package com.bubblesocialapp.bubble.customLayouts.mentionLayout;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bubblesocialapp.bubble.R;
import com.parse.ParseObject;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by sadaf on 8/10/15.
 */
public class MentionAdapter extends RecyclerView.Adapter<MentionAdapter.MentionsViewHolder> {

    private Context mContext;
    private List<ParseObject> mParseObjects;
    private MentionClickCallBack mCallBack;

    public interface MentionClickCallBack {
        public void onMentionClick(String string);
    }

    public MentionAdapter(Context context, List<ParseObject> parseObjects) {
        this.mContext = context;
        this.mParseObjects = parseObjects;

        try {
            this.mCallBack = ((MentionClickCallBack) context);
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement AdapterCallback.");
        }
    }

    @Override
    public int getItemCount() {
        return mParseObjects.size();
    }

    @Override
    public MentionsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_mention, parent, false);
        return new MentionsViewHolder(view, mCallBack);
    }

    @Override
    public void onBindViewHolder(MentionsViewHolder mViewHolder, int position) {
        ParseObject mItem = mParseObjects.get(position);

        mViewHolder.mNameView.setText(mItem.getString("username"));

        if (mItem.getParseFile("profilePicture") != null) {
            String profilePicFile = mItem.getParseFile("profilePicture").getUrl();

            if (profilePicFile != null) {
                Picasso.with(mViewHolder.mImageView.getContext())
                        .load(profilePicFile)
                        .error(R.drawable.ic_error)
                        .placeholder(R.drawable.ic_stub)
                        .resize(52, 52).centerCrop()
                        .into(mViewHolder.mImageView);
            }
        }

    }

    static class MentionsViewHolder extends RecyclerView.ViewHolder {

        public CircleImageView mImageView;
        public TextView mNameView;

        public MentionsViewHolder(View itemView, final MentionClickCallBack mCallBack) {
            super(itemView);
            mImageView = (CircleImageView) itemView.findViewById(R.id.mention_image_view);
            mNameView = (TextView) itemView.findViewById(R.id.mention_name_view);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCallBack.onMentionClick(mNameView.getText().toString());
                }
            });
        }
    }

}
