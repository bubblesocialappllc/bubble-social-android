package com.bubblesocialapp.bubble.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.common.Constants;
import com.bubblesocialapp.bubble.dialogs.CameraDialog;
import com.bubblesocialapp.bubble.dialogs.CustomProgressDialog;
import com.bubblesocialapp.bubble.dialogs.DatePickerCustomDialog;
import com.bubblesocialapp.bubble.dialogs.LoadingDialog;
import com.bubblesocialapp.bubble.fragments.FacebookSignUpCompleteFragment;
import com.bubblesocialapp.bubble.fragments.LoginFragment;
import com.bubblesocialapp.bubble.fragments.PhoneVerificationFragment;
import com.bubblesocialapp.bubble.fragments.SignUpChooseColorFragment;
import com.bubblesocialapp.bubble.fragments.SignUpFragment;
import com.bubblesocialapp.bubble.fragments.SignUpSecondFragment;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class LoginActivity extends AppCompatActivity
        implements FragmentManager.OnBackStackChangedListener,
                    LoginFragment.onButtonFromLoginFragmentClick,
                    CameraDialog.DialogFragmentClickListener,
                    SignUpFragment.onSignUpStartListener,
                    SignUpSecondFragment.OnSecondSignUpFragmentListener,
                    DatePickerCustomDialog.DatePickerCallBack,
                    SignUpChooseColorFragment.OnSignUpColorPickerListener,
                    PhoneVerificationFragment.OnPhoneVerificationListener,
                    FacebookSignUpCompleteFragment.onFacebookSignUpListener{

    private static final int REQUEST_PHOTO = 100;

    private static final int REQUEST_GALLARY_PHOTO = 200;

    private static final int FACEBOOK_LOGIN_REQUEST_CODE = 11235;

    private Toolbar mToolbar;

    private Uri mSavedPhotoUri;

    private ArrayList<String> mFacebookPermissionRequest;

    private String mFacebookId;

    private Bitmap mProfilePicture;

    private ParseUser mCurrentUser;

    private String mPickedUsername;

    private Boolean mUserFirstTime;

    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        mUserFirstTime = sp.getBoolean(Constants.USER_FIRST_TIME, true);

        setContentView(R.layout.activity_login);

        mToolbar = (Toolbar) findViewById(R.id.screen_default_toolbar);

        getSupportFragmentManager().addOnBackStackChangedListener(this);

        shouldDisplayHomeUp();

            if (savedInstanceState == null) {
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.container, new LoginFragment())
                        .commit();
            }
    }

    @Override
    public void onBackStackChanged() {
        shouldDisplayHomeUp();
    }

    public void shouldDisplayHomeUp(){

        setSupportActionBar(mToolbar);

        //Enable Up button only  if there are entries in the back stack
        boolean canGoBack = getSupportFragmentManager().getBackStackEntryCount()>0;
        getSupportActionBar().setDisplayHomeAsUpEnabled(canGoBack);
    }

    @Override
    public boolean onSupportNavigateUp() {
        getSupportFragmentManager().popBackStack();
        return true;
    }

    @Override
    public void onLoginWithFacebookClick() {
        mFacebookPermissionRequest = new ArrayList<>();
        mFacebookPermissionRequest.add("public_profile");
        mFacebookPermissionRequest.add("email");
        mFacebookPermissionRequest.add("user_about_me");
        mFacebookPermissionRequest.add("user_birthday");
        mFacebookPermissionRequest.add("user_photos");

        ParseFacebookUtils.logInWithReadPermissionsInBackground(this, mFacebookPermissionRequest, new LogInCallback() {
            @Override
            public void done(ParseUser user, ParseException err) {
                if (user == null) {
                    Log.d("MyApp", "Uh oh. The user cancelled the Facebook login.");
                } else if (user.isNew()) {
                    Log.d("MyApp", "User signed up and logged in through Facebook!");

                    mCurrentUser = user;
                    getDataFromFacebook(AccessToken.getCurrentAccessToken());
                } else {
                    Log.d("MyApp", "User logged in through Facebook!");

                    ParseUser.getCurrentUser().fetchInBackground(new GetCallback<ParseObject>() {
                        @Override
                        public void done(ParseObject parseObject, ParseException e) {
                            onLoginClick();
                        }
                    });

                }
            }
        });
    }

    public void getDataFromFacebook(AccessToken accessToken){
        GraphRequest request = GraphRequest.newMeRequest(
                accessToken,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject object,
                            GraphResponse response) {
                        // Application code
                        if (response.getError() == null) {
                            launchFacebookFragment(object);
                        } else {
                            Toast.makeText(getApplicationContext(), response.getError().toString(), Toast.LENGTH_LONG).show();
                        }

                    }
                });
        request.executeAsync();
    }

    public void launchFacebookFragment(JSONObject JSonObject) {
        Fragment facebookSignUpComplete = FacebookSignUpCompleteFragment.newInstance(JSonObject.toString());
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, facebookSignUpComplete);
        transaction.addToBackStack("facebookSignUpFinalStep");
        transaction.commit();
    }


    @Override
    public void onFacebookSignupComplete(String username, JSONObject object) {
        mProgressDialog = CustomProgressDialog.newInstance(this);
        mProgressDialog.show();
        mPickedUsername = username;
        saveDatatoParse(object);

    }

    public void saveDatatoParse(JSONObject object) {

        JSONObject notificationsJSonObject = new JSONObject();
        try {

            notificationsJSonObject.put("buddyRequest", true);
            notificationsJSonObject.put("comment", true);
            notificationsJSonObject.put("commentResponse", true);
            notificationsJSonObject.put("like", true);
            notificationsJSonObject.put("mention", true);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {

            DateFormat format;
            String birthdayString = object.getString("birthday");
            if (birthdayString.length() >=8) {
                format = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
            } else if (birthdayString.length() == 4) {
                format = new SimpleDateFormat("yyyy", Locale.ENGLISH);
            } else if (birthdayString.length() == 5) {
                format = new SimpleDateFormat("MM/dd", Locale.ENGLISH);
            } else {
                format = null;
            }
            if (format != null) {
                Date date = format.parse(birthdayString);
                mCurrentUser.put("birthday", date);
            }

            mCurrentUser.put("realName", object.get("name").toString());
            mCurrentUser.put("username", mPickedUsername);
            mCurrentUser.put("email", object.get("email").toString());
            mCurrentUser.put("locationHidden", false);
            mCurrentUser.put("points", 10);
            mCurrentUser.put("popPoints", 0);
//            mCurrentUser.put("aboutMe", object.get("user_about_me").toString());
            mCurrentUser.put("beta", true);
            mCurrentUser.put("color", "bubblePink");
            mCurrentUser.put("notifications", notificationsJSonObject);
            mCurrentUser.put("posts", 0);
            mCurrentUser.put("staff", false);

            mCurrentUser.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        mProgressDialog.dismiss();
                        onLoginClick();
                        getSupportFragmentManager().popBackStack();
                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                        finish();
                    }
                }
            });
        } catch (JSONException| java.text.ParseException e){
            e.printStackTrace();
        }

    }

    @Override
    public void onSignUpClick() {
        Fragment phoneVerificationFragment = PhoneVerificationFragment.newInstance();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, phoneVerificationFragment);
        transaction.addToBackStack("Login");
        transaction.commit();
    }

    @Override
    public void onSignUpClickFillInfo() {
        Fragment signUpFragment = new SignUpFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, signUpFragment);
        transaction.addToBackStack("Login");
        transaction.commit();
    }

    @Override
    public void onLoginClick() {

        if (mUserFirstTime) {
            Intent jazzHands = new Intent(this, JazzHandsActivity.class);
            startActivity(jazzHands);
        } else {
            Intent feed = new Intent(this, HomeActivity.class);
            feed.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            feed.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            if (isNetworkAvailable()) {
                feed.putExtra(Constants.CONNECTION_FLAG, Constants.NETWORK_AVAILABLE_FLAG);
            } else {
                feed.putExtra(Constants.CONNECTION_FLAG, Constants.NO_NETWORK_AVAILABLE_FLAG);
            }
            startActivity(feed);
        }
    }


    @Override
    public void onDialogGalleryClick(DialogFragment dialog) {
        if (dialog.getTag().equals("profilePicture")) {
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, REQUEST_GALLARY_PHOTO);
        } else if (dialog.getTag().equals("profilePictureFragment")) {
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, Constants.REQUEST_GALLARY_PHOTO_FOR_FACEBOOK_SIGNUP);
        }
    }

    @Override
    public void onDialogCameraClick(DialogFragment dialog) {
        if (dialog.getTag().equals("profilePicture")) {
            Intent intent = setActionImageCaptureIntent();
            startActivityForResult(intent, REQUEST_PHOTO);
        } else if (dialog.getTag().equals("profilePictureFragment")) {
            Intent intent = setActionImageCaptureIntent();
            startActivityForResult(intent, Constants.REQUEST_PHOTO_FOR_FACEBOOK_SIGNUP);
        }
    }

    public Intent setActionImageCaptureIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File bubbleImageFolder = new File(Environment.getExternalStorageDirectory() + File.separator + "bubble");

        if (!bubbleImageFolder.exists()){
            bubbleImageFolder.mkdirs();
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String fileName = "bubble_picture_" + timeStamp + ".jpg";
        File output = new File(bubbleImageFolder, fileName);
        mSavedPhotoUri = Uri.fromFile(output);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mSavedPhotoUri);
        return intent;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_PHOTO) {
            if (resultCode == Activity.RESULT_OK) {
                onImageChosen(mSavedPhotoUri);
            }
        } else if (requestCode == REQUEST_GALLARY_PHOTO) {
            if (resultCode == Activity.RESULT_OK) {
                onImageChosen(data.getData());
            }
        } else if (requestCode == FACEBOOK_LOGIN_REQUEST_CODE) {
            ParseFacebookUtils.onActivityResult(requestCode, resultCode, data);

        } else if (requestCode == Constants.REQUEST_GALLARY_PHOTO_FOR_FACEBOOK_SIGNUP) {

            if (resultCode == Activity.RESULT_OK) {
                onFacebookImageChosen(data.getData());
            }

        } else if (requestCode == Constants.REQUEST_PHOTO_FOR_FACEBOOK_SIGNUP) {
            if (resultCode == Activity.RESULT_OK) {
                onFacebookImageChosen(mSavedPhotoUri);
            }
        }
    }

    public void onFacebookImageChosen(final Uri image) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (image != null) {
                    // Call Fragment method and add picture
                    FacebookSignUpCompleteFragment signUpFragment = (FacebookSignUpCompleteFragment)
                            getSupportFragmentManager().findFragmentById(R.id.container);
                    signUpFragment.setProfilePic(image);
                }
            }
        });
    }

    public void onImageChosen(final Uri image) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (image != null) {
                    // Call Fragment method and add picture
                    SignUpSecondFragment signUpFragment = (SignUpSecondFragment)
                            getSupportFragmentManager().findFragmentById(R.id.container);
                    signUpFragment.setProfilePic(image);
                }
            }
        });
    }

    @Override
    public void onSecondSignUpFragmentComplete(String JsonObject) {
        Fragment signUpColorFragment = SignUpChooseColorFragment.newInstance(JsonObject);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, signUpColorFragment);
        transaction.addToBackStack("signUpSecondColor");
        transaction.commit();
    }

    @Override
    public void onDatePicked(int year, int month, int day) {
        SignUpFragment signUp = (SignUpFragment)
                getSupportFragmentManager().findFragmentById(R.id.container);
        signUp.setPickedBirthdayDate(year, month, day);
    }

    @Override
    public void hideActionBar() {
        getSupportActionBar().hide();
    }

    @Override
    public void onSignUpStart() {
        getSupportActionBar().show();
    }

    @Override
    public void onSignUpComplete(String userJsonObject) {
        Fragment signUpSecondFragment = SignUpSecondFragment.newInstance(userJsonObject);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, signUpSecondFragment);
        transaction.addToBackStack("signUp");
        transaction.commit();
    }

    @Override
    public void onColorPicked(String userObject) {
        onLoginClick();
    }

    @Override
    public void onVerificationComplete() {
        onSignUpClickFillInfo();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}

