/*
 * Copyright (C) 2014 Antonio Leiva Gordillo.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bubblesocialapp.bubble.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bubblesocialapp.bubble.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.ErrorDialogFragment;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationListener;
import com.parse.ParseUser;

public abstract class BaseActivity extends AppCompatActivity
        implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private Toolbar mToolBar;
    private Toolbar mNotificicationsToolbar;

//    public static final int HOME = 0;
//    public static final int MESSAGES = 1;
//    public static final int AROUND_ME = 2;
//    public static final int PROFILE = 3;
//    public static final int BUDDY_SEARCH = 4;
//    public static final int LOGOUT = 5;

    private static final String USER_HAS_GOOGLE_SERVICES = "google_services_installed";
    private Boolean mUserHasGoogleServices;
    private GoogleApiClient mGoogleApiClient;
    private static final String STATE_RESOLVING_ERROR = "resolving_error";
    private static final int REQUEST_RESOLVE_ERROR = 1001;
    private boolean mResolvingError = false;
    private static final int REQUEST_CODE = 100;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(STATE_RESOLVING_ERROR, mResolvingError);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(setLayoutResourceIdentifier());

        mResolvingError = savedInstanceState != null
                && savedInstanceState.getBoolean(STATE_RESOLVING_ERROR, false);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mUserHasGoogleServices = sharedPreferences.getBoolean(USER_HAS_GOOGLE_SERVICES,false);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        loadViewComponents();
//        loadInfoToolbar();
//        loadOnClickListener();


    }

    private void loadViewComponents() {
//        mToolBar = (Toolbar) findViewById(R.id.screen_default_toolbar);
        mNotificicationsToolbar = (Toolbar) findViewById(R.id.notifications_toolbar);

    }

//    private void loadInfoToolbar() {
//        setSupportActionBar(mToolBar);
//        try {
//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        } catch (NullPointerException e) {
//            // set message
//        }
//    }

//    private void loadOnClickListener(){
//        mSearchTextView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startSearchFragment(view);
//            }
//        });
//    }
//
//    public void startSearchFragment(View v){
//        // Create fragment and give it an argument for the selected article
//        SearchFragment newFragment = new SearchFragment();
//        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        transaction.replace(R.id.screen_default_container, newFragment);
//        transaction.addToBackStack("Main");
//        // Commit the transaction
//        transaction.commit();
//    }


    @Override
    public void onConnected(Bundle bundle) {
        // User is connected dont do anything
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (mResolvingError) {
            // Already attempting to resolve an error.
            return;
        } else if (connectionResult.hasResolution()) {
            try {
                mResolvingError = true;
                connectionResult.startResolutionForResult(this, REQUEST_RESOLVE_ERROR);
            } catch (IntentSender.SendIntentException e) {
                // There was an error with the resolution intent. Try again.
                mGoogleApiClient.connect();
            }
        } else {
            // Show dialog using GooglePlayServicesUtil.getErrorDialog()
            showErrorDialog(connectionResult.getErrorCode());
            mResolvingError = true;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_RESOLVE_ERROR) {
            mResolvingError = false;
            if (resultCode == RESULT_OK) {
                // Make sure the app is not already connected or attempting to connect
                if (!mGoogleApiClient.isConnecting() &&
                        !mGoogleApiClient.isConnected()) {
                    mGoogleApiClient.connect();
                }
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!mResolvingError) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    protected void onResume() {
        super.onResume();
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode == ConnectionResult.SUCCESS) {
            //continue
        } else if (resultCode == ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED) {
            // Make toast to inform user of required update
            GooglePlayServicesUtil.getErrorDialog(ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED,this, REQUEST_CODE);
        } else if (resultCode == ConnectionResult.SERVICE_DISABLED) {
            // Enable google play services
        }
    }

    ///////////Temporary Code TODO: MUST UPDATE/////////////////////
        /* Creates a dialog for an error message */
    private void showErrorDialog(int errorCode) {
        // Create a fragment for the error dialog
        ErrorDialogFragment dialogFragment = new ErrorDialogFragment();
        // Pass the error that should be displayed
        Bundle args = new Bundle();
        args.putInt("dialogError", errorCode);
        dialogFragment.setArguments(args);
        dialogFragment.show(getSupportFragmentManager(), "errordialog");
    }

    /* Called from ErrorDialogFragment when the dialog is dismissed. */
    public void onDialogDismissed() {
        mResolvingError = false;
    }

    /* A fragment to display an error dialog */
    public static class ErrorDialogFragment extends DialogFragment {
        public ErrorDialogFragment() { }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Get the error code and retrieve the appropriate dialog
            int errorCode = this.getArguments().getInt("dialogError");
            return GooglePlayServicesUtil.getErrorDialog(errorCode,
                    this.getActivity(), REQUEST_RESOLVE_ERROR);
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            ((BaseActivity)getActivity()).onDialogDismissed();
        }
    }

    protected abstract int setLayoutResourceIdentifier();

    protected abstract int getTitleToolBar();


}

//    @Override
//    public void onNavigationDrawerItemSelected(int position) {
//        switch (position) {
//            case HOME:
//                Intent home = new Intent(getApplicationContext(), FeedActivity.class);
//                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(home);
//                finish();
//                break;
//            case MESSAGES:
//                Intent messages = new Intent(getApplicationContext(), MessageActivity.class);
//                messages.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(messages);
//                finish();
//                break;
//            case AROUND_ME:
//                Intent aroundMe = new Intent(getApplicationContext(), AroundMeActivity.class);
//                aroundMe.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(aroundMe);
//                finish();
//                break;
//            case PROFILE:
//                Intent profile = new Intent(getApplicationContext(), ProfileActivity.class);
//                profile.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(profile);
//                finish();
//                break;
//            case BUDDY_SEARCH:
//                Intent search = new Intent(getApplicationContext(), SearchActivity.class);
//                search.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(search);
//                finish();
//                break;
//            case LOGOUT:
//                ParseUser.getCurrentUser().logOutInBackground();
//                Intent login = new Intent(getApplicationContext(), LoginActivity.class);
//                login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(login);
//                finish();
//                break;
//
//        }
//    }

//        mNavigationDrawerFragment = (NavigationDrawerFragment)
//                getSupportFragmentManager().findFragmentById(R.id.screen_default_navigation_drawer);
//        mDrawerLayout = (DrawerLayout) findViewById(R.id.screen_default_drawer_layout);

//    private void loadInfoDrawerMenu() {
//        mNavigationDrawerFragment.setUp(
//                R.id.screen_default_navigation_drawer,
//                (DrawerLayout) findViewById(R.id.screen_default_drawer_layout));
//    }