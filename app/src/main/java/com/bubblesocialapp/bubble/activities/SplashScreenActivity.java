package com.bubblesocialapp.bubble.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.common.Constants;
import com.bubblesocialapp.bubble.common.Util;
import com.parse.ParseUser;

import java.util.Timer;
import java.util.TimerTask;

public class SplashScreenActivity extends Activity {

    private static final String USER_PICKED_SEARCH_RADIUS = "user_picked_search_radius";

    private static final String FEED_DATA = "feed_data";

    private static final int TIMER_LENGTH = 2000;

    // TODO: Move Google Services check here

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        Timer timer = new Timer();

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (ParseUser.getCurrentUser() != null && Util.isNetworkAvailable(getApplicationContext())) {
                    startHomeActivity(true);
                } else if (ParseUser.getCurrentUser() != null && !Util.isNetworkAvailable(getApplicationContext())) {
                    startHomeActivity(false);
                } else {
                    Intent login = new Intent(getApplicationContext(), LoginActivity.class);
                    login.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(login);
                    finish();
                }
            }
        }, TIMER_LENGTH);

        }

    private void startHomeActivity(boolean networkAvailable) {
        Intent home = new Intent(getApplicationContext(),HomeActivity.class);
        if (networkAvailable) {
            home.putExtra(Constants.CONNECTION_FLAG, Constants.NETWORK_AVAILABLE_FLAG);
        } else {
            home.putExtra(Constants.CONNECTION_FLAG, Constants.NO_NETWORK_AVAILABLE_FLAG);
        }
        home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(home);
        finish();
    }



}
