package com.bubblesocialapp.bubble.activities;


import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;

import android.os.Environment;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.adapters.AroundMeAdapter;
import com.bubblesocialapp.bubble.adapters.MessageHeadAdapter;
import com.bubblesocialapp.bubble.adapters.NotificationAdapter;
import com.bubblesocialapp.bubble.adapters.SearchAdapter;
import com.bubblesocialapp.bubble.common.Constants;
import com.bubblesocialapp.bubble.common.Util;
import com.bubblesocialapp.bubble.customLayouts.customViewPager.MaterialViewPager;
import com.bubblesocialapp.bubble.adapters.FeedAdapter;
import com.bubblesocialapp.bubble.adapters.HomeScreenPagerAdapter;

import com.bubblesocialapp.bubble.customLayouts.mentionLayout.MentionAdapter;
import com.bubblesocialapp.bubble.dialogs.CameraDialog;
import com.bubblesocialapp.bubble.fragments.AroundMeAndPopularFeedHolderFragment;
import com.bubblesocialapp.bubble.fragments.AroundMeFragment;
import com.bubblesocialapp.bubble.fragments.BuddiesFragment;
import com.bubblesocialapp.bubble.fragments.CommentsFragment;
import com.bubblesocialapp.bubble.fragments.DisplayPhotoFragment;
import com.bubblesocialapp.bubble.fragments.EditProfileFragment;
import com.bubblesocialapp.bubble.fragments.FeedFragment;
import com.bubblesocialapp.bubble.fragments.MessageFragment;
import com.bubblesocialapp.bubble.fragments.MessageHeadFragment;
import com.bubblesocialapp.bubble.fragments.NotificationPostFragment;
import com.bubblesocialapp.bubble.fragments.NotificationsFragment;
import com.bubblesocialapp.bubble.fragments.GalleryFragment;
import com.bubblesocialapp.bubble.fragments.PopularityPointsFragment;
import com.bubblesocialapp.bubble.fragments.PostFragment;
import com.bubblesocialapp.bubble.fragments.ProfileFragment;
import com.bubblesocialapp.bubble.fragments.SearchFragment;
import com.bubblesocialapp.bubble.fragments.SeeCurrentBuddiesFragment;
import com.bubblesocialapp.bubble.fragments.SettingsFragment;
import com.bubblesocialapp.bubble.interfaces.NotificationsServiceCallback;
import com.bubblesocialapp.bubble.parseObjects.Notification;
import com.bubblesocialapp.bubble.parseObjects.Post;
import com.bubblesocialapp.bubble.services.NotificationService;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


/**
 * Created by sadaf on 7/7/15.
 */
public class HomeActivity extends BaseActivity
        implements FeedFragment.PostListener,
        FeedAdapter.AdapterCallBack,
        AroundMeAdapter.OnUserClickListener,
        ProfileFragment.OnProfileClickListener,
        MessageHeadFragment.OnMessageClickListener,
        MessageHeadAdapter.OnMessageClickListener,
        SearchAdapter.OnSearchResultClickListener,
        GalleryFragment.OnPhotoClickListener,
        NotificationPostFragment.NotificationPostListener,
        ServiceConnection, NotificationsServiceCallback,
        CommentsFragment.OnCommentsFragmentListener,
        SettingsFragment.OnSettingsClickListener,
        CameraDialog.DialogFragmentClickListener,
        AroundMeFragment.ScrollListenerInterface,
        NotificationAdapter.NotificationClickListener,
        MentionAdapter.MentionClickCallBack,
        PostFragment.PostSendListener{


    private MaterialViewPager mViewPager;
    private static final String FEED_DATA = "feed_data";
    private Toolbar mToolbar;
    private ViewGroup mRootLayout;
    private TextView mNotificationTextView;
    private int _xDelta;
    private int _yDelta;
    private String itemtype;
    private NotificationService mService;
    private int mNotificationNumber;
    private String flag;
    private boolean mBound = false;
    private TextView mSearchTextView;
    private Uri mSavedPhotoUri;
    private HomeScreenPagerAdapter mAdapter;
    private CameraDialog mDialog;
    private String mFlag;

    protected Location mLastLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setUpToolBar();
        Intent intent = getIntent();
        if (intent.getExtras() != null) {

            if (isNetworkAvailable(intent)) {
                getNotifications(intent);
                loadViewComponents(true);
            } else {
                loadViewComponents(false);
            }

            if (intent.getExtras().getString(Constants.FLAG) != null) {
                mFlag = intent.getExtras().getString(Constants.FLAG);
            }

        }

    }

    public boolean isNetworkAvailable(Intent intent) {
        if (intent.getStringExtra(Constants.CONNECTION_FLAG).equals(Constants.NO_NETWORK_AVAILABLE_FLAG)) {
            return false;
        } else {
            return true;
        }
    }

    public void getNotifications(Intent intent) {
        if (intent.getExtras().get("com.parse.Data") != null) {
            JSONObject notificationData = (JSONObject) intent.getExtras().get("com.parse.Data");
            try {
                itemtype = notificationData.getString("category");
            } catch (JSONException e) {
                Log.e("JsonException", e.getMessage());
            }

            if (itemtype != null) {
                goToFragmentForNotification(notificationData, itemtype);
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        // For opening notifications from inapp
        if (!mBound) {
            Intent notificationService = new Intent(this, NotificationService.class);
            this.bindService(notificationService, this, BIND_AUTO_CREATE);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mBound) {
            mService.setCallBack(null);
            mService.destroyMagnet();
            unbindService(this);
            mBound = false;
        }
    }

    private void loadViewComponents(boolean isConnected){

        mAdapter = new HomeScreenPagerAdapter(getSupportFragmentManager(), isConnected);

        mViewPager = (MaterialViewPager) findViewById(R.id.materialViewPager);
        mViewPager.getViewPager().setAdapter(mAdapter);

        mViewPager.getPagerTitleStrip().setCustomTabView(R.layout.custom_tab_view, R.id.tab_icon_textView, R.id.tab_icon_imageView);
        mViewPager.getPagerTitleStrip().setDistributeEvenly(true);
        mViewPager.getPagerTitleStrip().setBackgroundColor(getResources().getColor(R.color.view_pager_background_bubble_pink_transparent));
        mViewPager.getPagerTitleStrip().setSelectedIndicatorColors(getResources().getColor(R.color.white));

        mViewPager.getPagerTitleStrip().setViewPager(mViewPager.getViewPager());
        mViewPager.setMapFragment(getSupportFragmentManager());

        mViewPager.getProfileImageView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog = new CameraDialog();
                mDialog.show(getSupportFragmentManager(), "profilePicture");
            }
        });

        mViewPager.getPopularityView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startPopularityPoints();
            }
        });

        mViewPager.getSearchButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSearchButtonClicked();
            }
        });

        mViewPager.getSearchTextView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSearchButtonClicked();
            }
        });

        mViewPager.getLocationToggleButton().setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                final SharedPreferences sp = PreferenceManager
                    .getDefaultSharedPreferences(HomeActivity.this);
                if (b) {
                    ParseUser user = ParseUser.getCurrentUser();
                    user.put("locationHidden", b);
                    user.saveEventually();
                    showToast("Location Shared");
                    sp.edit().putBoolean(Constants.SHARE_LOCATION, b);
                } else {
                    ParseUser user = ParseUser.getCurrentUser();
                    user.put("locationHidden", b);
                    user.saveEventually();
                    showToast("Location Shared Off");
                    sp.edit().putBoolean(Constants.SHARE_LOCATION, b);
                }
            }
        });

        mToolbar = mViewPager.getToolbar();

        setProfilePicture();

        if (mFlag != null) {
            if (mFlag.equals(Constants.NOTIFICATION_INTENT)) {
                onNotificationsButtonPressed();
            }
        }

    }

    private void showToast(String message) {
        Toast.makeText(HomeActivity.this, message, Toast.LENGTH_LONG).show();
    }

    private void setProfilePicture(){
        if (ParseUser.getCurrentUser() != null) {
            try {
                ParseFile profileUrl = ParseUser.getCurrentUser().getParseFile("profilePicture");
                mViewPager.addProfilePicture(profileUrl.getUrl());
            } catch (NullPointerException e) {
                //No picture
            }
        }

    }

    private void setUpToolBar() {
        if (mToolbar != null) {

            final ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setDisplayShowHomeEnabled(true);
                actionBar.setDisplayShowTitleEnabled(true);
                actionBar.setDisplayUseLogoEnabled(false);
                actionBar.setHomeButtonEnabled(true);
            }
        }
    }

    private void goToFragmentForNotification(JSONObject data, String itemtype){
        // For push Notifications
        switch(itemtype) {
            case ("BuddyRequest"):
                startBuddiesFeed();
                break;
            case ("Comment"):
                startComments(data);
                break;
            case ("commentResponse"):
                startComments(data);
                break;
            case ("Like"):
                startPost(data);
                break;
            case ("Mention"):
                startPost(data);
                break;
        }
    }

    public void startBuddiesFeed(){
        onBuddiesButtonPressed();
    }

    public void startPost(JSONObject data){
        try {
            String objectId = data.getString("viewToPush");
            // Create fragment and give it an argument for the selected article
            NotificationPostFragment newFragment = NotificationPostFragment.newInstance(objectId, "post");
            startFragment(newFragment);

        } catch (JSONException e){
            //Log stuff
        }

    }

    public void startPopularityPoints() {
        PopularityPointsFragment fragment = new PopularityPointsFragment();
        startFragment(fragment);
    }

    public void startPost(String objectId) {
            NotificationPostFragment newFragment = NotificationPostFragment.newInstance(objectId, "post");
            startFragment(newFragment);
    }

    public void startComments(JSONObject data) {
        // Launch Fragment to see comment
        try {
            String objectId = data.getString("viewToPush");
            NotificationPostFragment newFragment = NotificationPostFragment.newInstance(objectId, "comment");
            startFragment(newFragment);

        } catch (JSONException e){
            //Log stuff
        }
    }

    @Override
    public void startNotificationsFragment(){
        NotificationsFragment fragment = new NotificationsFragment();
        startFragment(fragment);
    }

    @Override
    public void onNotificationClick(String notificationType, Notification notification) {
        // For non-push notififications. Will merge later
        switch(notificationType) {
            case ("BuddyRequest"):
                startBuddiesFeed();
                break;
            case ("Comment"):
                startCommentsFragment(notification.getPost().getObjectId(), notification.getSentFrom());
                break;
            case ("commentResponse"):
                startCommentsFragment(notification.getPost().getObjectId(), notification.getSentFrom());
                break;
            case ("Like"):
                startPost(notification.getPost().getObjectId());
                break;
            case ("Mention"):
                startPost(notification.getPost().getObjectId());
                break;
        }
    }

    @Override
    protected int setLayoutResourceIdentifier() {
        return R.layout.activity_home;
    }

    @Override
    protected int getTitleToolBar() {
        return R.string.app_name;
    }

    /**
     * Replace current fragment with
     * post fragment when post button is pressed
     */
    @Override
    public void onPostButtonPressed() {

        // Create fragment and give it an argument for the selected article
        PostFragment newFragment = new PostFragment();
        startFragment(newFragment);

    }

    @Override
    public void onCommentButtonSelected(String string, String authorName) {
        startCommentsFragment(string, authorName);
    }

    @Override
    public void startCommentsFragment(String objectId, String username) {

        CommentsFragment fragment = CommentsFragment.newInstance(objectId, username);
        startFragment(fragment);
    }

    @Override
    public void onPhotoClick(String url) {
        DisplayPhotoFragment photoFragment = DisplayPhotoFragment.newInstance(url, 0);
        startFragment(photoFragment);
    }

    @Override
    public void onBuddiesClick(String objectId, ArrayList<String> mBuddies) {

        SeeCurrentBuddiesFragment newFragment = SeeCurrentBuddiesFragment.newInstance(objectId, mBuddies);
        startFragment(newFragment);
    }

    public void onSearchButtonClicked(){
        SearchFragment fragment = new SearchFragment();
        startFragment(fragment);
    }

    @Override
    public void onPhotosClick(String objectId) {

        GalleryFragment newFragment = GalleryFragment.newInstance(objectId);
        startFragment(newFragment);
    }

    @Override
    public void onPreviousPostsClick(String objectId) {

        FeedFragment newFragment = FeedFragment.newInstance(FeedFragment.SHOW_USERS_OLD_POSTS, isNetworkAvailable(getIntent()));
        startFragment(newFragment);
    }

    @Override
    public void onMessageUserClick(String objectId, String username) {

        MessageFragment newFragment = MessageFragment.newInstance(null, objectId, username);
        startFragment(newFragment);
    }

    @Override
    public void onSettingsClick() {
        SettingsFragment settingsFragment = new SettingsFragment();
        startFragment(settingsFragment);
    }

    @Override
    public void addPhoto() {
        CameraDialog dialog = new CameraDialog();
        dialog.show(getSupportFragmentManager(), "galleryPicture");
    }

    @Override
    public void onPhotoClick(String[] url, int position) {
        DisplayPhotoFragment photoFragment = DisplayPhotoFragment.newInstance(url,position);
        startFragment(photoFragment);
    }

    @Override
    public void onUserClicked(String userObjectId, String username) {
        // Around Me
        ProfileFragment profileFragment = ProfileFragment.newInstance(userObjectId,username);
        startFragment(profileFragment);
    }

    @Override
    public void onNewMessageClick() {
        SearchFragment searchFragment = new SearchFragment();
        startFragment(searchFragment);
    }

    @Override
    public void onMessageHeadClick(String objectId) {
        MessageFragment detailFragment =
                MessageFragment.newInstance(objectId,null,null);
        startFragment(detailFragment);
    }

    @Override
    public void onSearchResultClick(ParseUser parseObject) {
        MessageFragment detailFragment =
                MessageFragment.newInstance(null,parseObject.getObjectId(), parseObject.getUsername());
        startFragment(detailFragment);
    }

    @Override
    public void onBuddiesButtonPressed() {
        BuddiesFragment buddiesFragment = new BuddiesFragment();
        startFragment(buddiesFragment);
    }

    @Override
    public void onNotificationsButtonPressed() {
        NotificationsFragment notificationsFragment = new NotificationsFragment();
        startFragment(notificationsFragment);
    }

    public void startFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_home_content_main, fragment);
        transaction.addToBackStack("Main");
        transaction.commit();
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        NotificationService.NotificationBinder binder = (NotificationService.NotificationBinder) iBinder;

        mService = binder.getService();
        mBound = true;
        mService.setCallBack(this);
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        mBound = false;
    }

    @Override
    public void onFragmentFinish() {
        getSupportFragmentManager().popBackStack();
    }

    @Override
    public void onEditProfileClick() {
        EditProfileFragment fragment = new EditProfileFragment();
        startFragment(fragment);
    }

    @Override
    public void onBlockedUsersClick() {

    }

    @Override
    public void onDialogGalleryClick(DialogFragment dialog) {
        if (dialog.getTag().equals("galleryPicture")) {
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, Constants.REQUEST_GALLARY_PHOTO); //Grab photo from gallery to add into bubble
        } else if (dialog.getTag().equals("profilePicture")) {
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, Constants.REQUEST_GALLERY_PHOTO_FOR_PROFILE); //Choose profile photo from gallery
        }
    }

    @Override
    public void onDialogCameraClick(DialogFragment dialog) {
        if (dialog.getTag().equals("galleryPicture")) {
            Intent intent = setActionImageCaptureIntent();
            startActivityForResult(intent, Constants.REQUEST_PHOTO); //Take picture to add to the bubble gallery
        } else if (dialog.getTag().equals("profilePicture")) {
            Intent intent = setActionImageCaptureIntent();
            startActivityForResult(intent, Constants.REQUEST_PHOTO_FOR_PROFILE); //Take picture to add to profile
        }
    }

    public Intent setActionImageCaptureIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File bubbleImageFolder = new File(Environment.getExternalStorageDirectory() + File.separator + "bubble");

        if (!bubbleImageFolder.exists()){
            bubbleImageFolder.mkdirs();
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String fileName = "bubble_picture_" + timeStamp + ".jpg";
        File output = new File(bubbleImageFolder, fileName);
        mSavedPhotoUri = Uri.fromFile(output);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mSavedPhotoUri);
        return intent;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.REQUEST_PHOTO) {
            if (resultCode == Activity.RESULT_OK) {
                onImageChosen(mSavedPhotoUri);
            }
        } else if (requestCode == Constants.REQUEST_GALLARY_PHOTO) {
            if (resultCode == Activity.RESULT_OK) {
                onImageChosen(data.getData());
            }
        } else if (requestCode == Constants.REQUEST_PHOTO_FOR_POST) {
            if (resultCode == Activity.RESULT_OK) {
                onPostPictureTaken();
            }
        } else if (requestCode == Constants.REQUEST_PHOTO_FOR_PROFILE) {
            if (resultCode == Activity.RESULT_OK) {
                setProfilePic(mSavedPhotoUri);
            }
        } else if (requestCode == Constants.REQUEST_GALLERY_PHOTO_FOR_PROFILE) {
            if (resultCode == Activity.RESULT_OK) {
                setProfilePic(data.getData());
            }
        } else if (requestCode ==  Constants.PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                String toastMsg = String.format("Place: %s", place.getName());
                Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();
                setPlace(place);
            }

        } else if (requestCode == Constants.REQUEST_PHOTO_FOR_COMMENT) {
            if (resultCode == Activity.RESULT_OK) {
                onCommentPictureTaken();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void onCommentPictureTaken() {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                // Call Fragment method and add picture
                CommentsFragment commentsFragment = (CommentsFragment)
                        getSupportFragmentManager().findFragmentById(R.id.fragment_home_content_main);
                commentsFragment.setCommentPicture();
            }
        });
    }

    public void onPostPictureTaken() {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                // Call Fragment method and add picture
                PostFragment postFragment = (PostFragment)
                        getSupportFragmentManager().findFragmentById(R.id.fragment_home_content_main);
                postFragment.setPostPicture();
            }
        });
    }

    public void onImageChosen(final Uri image) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if (image != null) {
                    // Call Fragment method and add picture
                    GalleryFragment galleryFragment = (GalleryFragment)
                            getSupportFragmentManager().findFragmentById(R.id.fragment_home_content_main);
                    galleryFragment.addPictureToGallery(image);
                }
            }
        });
    }

    @Override
    public void onScrolledInFragment(int dy) {
        // This is The poppin' feed fragment
        AroundMeAndPopularFeedHolderFragment fragment = (AroundMeAndPopularFeedHolderFragment)
            mAdapter.getRegisteredFragment(1);
        fragment.onScrolled(dy);
    }

    public void setProfilePic(Uri image) {
        final ParseUser currentUser = ParseUser.getCurrentUser();

        mViewPager.getProfileImageView().setImageURI(image);

        try {
            byte[] photoData = Util.UriToByte(HomeActivity.this, image);
            photoData = Util.scalePhoto(photoData);
            final ParseFile savePhoto = new ParseFile("profilePic",photoData);
            savePhoto.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e != null) {
                        Toast.makeText(HomeActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                        currentUser.put("profilePicture", savePhoto);
                        currentUser.saveInBackground();
                    }
                }

            });

        } catch (IOException e) {
            Log.d("Error converting", e.getMessage());
        }
    }

    public void setPlace(Place place) {
        // Call Fragment method and add picture
        PostFragment postFragment = (PostFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragment_home_content_main);
        postFragment.setPlace(place);
    }

    @Override
    public void onMentionClick(String string) {
        // Call Fragment method and add text
        PostFragment postFragment = (PostFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragment_home_content_main);
        postFragment.addMention(string);
    }

    @Override
    public void onPostSendClicked(Post post) {

        getSupportFragmentManager().popBackStack();

        FeedFragment feedFragment = (FeedFragment)
                mAdapter.getRegisteredFragment(0);
        feedFragment.addNewPost(post);
    }
}



















//        View.OnTouchListener,

//        loadNotificationBubble();

//    private void loadNotificationBubble(){
//        mRootLayout = (ViewGroup) findViewById(R.id.fragment_home_content_main);
//        mNotificationTextView = (TextView) findViewById(R.id.notification_textview);
//
//        int notificationBubbleSizePixels = (int) Util.dpToPx(this, 50);
//        RelativeLayout.LayoutParams layoutParams =
//                new RelativeLayout.LayoutParams(notificationBubbleSizePixels, notificationBubbleSizePixels);
//
//        // Notification Bubble
//        mNotificationTextView.setVisibility(View.GONE);
//
//        mNotificationTextView.setLayoutParams(layoutParams);
//        mNotificationTextView.setOnTouchListener(this);
//
//    }

//    @Override
//    public boolean onTouch(View view, MotionEvent motionEvent) {
//        final int X = (int) motionEvent.getRawX();
//        final int Y = (int) motionEvent.getRawY();
//        switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
//            case MotionEvent.ACTION_DOWN:
//                RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
//                _xDelta = X - lParams.leftMargin;
//                _yDelta = Y - lParams.topMargin;
//                break;
//            case MotionEvent.ACTION_UP:
//                break;
//            case MotionEvent.ACTION_POINTER_DOWN:
//                break;
//            case MotionEvent.ACTION_POINTER_UP:
//                break;
//            case MotionEvent.ACTION_MOVE:
//                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view
//                        .getLayoutParams();
//                layoutParams.leftMargin = X - _xDelta;
//                layoutParams.topMargin = Y - _yDelta;
//                layoutParams.rightMargin = -250;
//                layoutParams.bottomMargin = -250;
//                view.setLayoutParams(layoutParams);
//                break;
//        }
//        mRootLayout.invalidate();
//        return true;
//    }