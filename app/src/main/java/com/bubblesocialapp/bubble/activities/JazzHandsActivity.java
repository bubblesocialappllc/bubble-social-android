package com.bubblesocialapp.bubble.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.common.Constants;
import com.bubblesocialapp.bubble.fragments.JazzHandsFragment;

public class JazzHandsActivity extends AppCompatActivity {

    public static String[] drawableNames = {"jazzhands1", "jazzhands2", "jazzhands3",
                                            "jazzhands4", "jazzhands5", "jazzhands6"};

    private ViewPager mViewPager;
    private Button mSkipButton;
    private Button mMakePopButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jazz_hands);

        mSkipButton = (Button) findViewById(R.id.jazz_hands_skip);
        mViewPager = (ViewPager) findViewById(R.id.jazz_hands_view_pager);
        mViewPager.setAdapter(new JazzHandsPagerAdapter(getSupportFragmentManager()));
        mMakePopButton = (Button) findViewById(R.id.start_post_fragment);

        mSkipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSkipClicked();
            }
        });

        mMakePopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSkipClicked();
            }
        });
    }

    private void onSkipClicked() {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        sp.edit().putBoolean(Constants.USER_FIRST_TIME, false).apply();

        Intent feed = new Intent(this, HomeActivity.class);
        feed.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        feed.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        if (isNetworkAvailable()) {
            feed.putExtra(Constants.CONNECTION_FLAG, Constants.NETWORK_AVAILABLE_FLAG);
        } else {
            feed.putExtra(Constants.CONNECTION_FLAG, Constants.NO_NETWORK_AVAILABLE_FLAG);
        }
        startActivity(feed);

    }

    public class JazzHandsPagerAdapter extends FragmentPagerAdapter {

        public JazzHandsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return JazzHandsFragment.newInstance(drawableNames[position]);
        }

        @Override
        public int getCount() {
            return drawableNames.length;
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
