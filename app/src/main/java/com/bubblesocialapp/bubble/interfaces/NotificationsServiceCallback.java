package com.bubblesocialapp.bubble.interfaces;

/**
 * Created by sadaf on 7/25/15.
 */
public interface NotificationsServiceCallback {

    /**Launch notifications fragment*/

    public void startNotificationsFragment();

}
