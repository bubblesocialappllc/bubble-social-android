package com.bubblesocialapp.bubble.interfaces;

import com.bubblesocialapp.bubble.bean.HeaderItemBean;

import java.util.List;

/**
 * Created by sadaf on 8/4/15.
 */
public interface ReturnSearchResultsInterface {

    public void onSearchResultsFound(List<HeaderItemBean> list);
}
