package com.bubblesocialapp.bubble.interfaces;

import org.json.JSONObject;

/**
 * Created by Sadaf Mackertich on 8/17/2015.
 */
public interface ReturnPlacesInterface {

    public void onPlacesReturned(JSONObject jsonObject);
}
