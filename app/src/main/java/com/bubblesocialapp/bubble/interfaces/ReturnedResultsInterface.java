package com.bubblesocialapp.bubble.interfaces;

import com.parse.ParseObject;

import java.util.List;

/**
 * Created by sadaf on 8/1/15.
 */
public interface ReturnedResultsInterface {

    public void returnedResults(List<ParseObject> parseObjects);
}
