package com.bubblesocialapp.bubble.interfaces;

import android.graphics.Bitmap;

/**
 * Created by sadaf on 8/3/15.
 */
public interface ReturnBitmapResults{

    public void returnBitmap(Bitmap bitmap);
}
