package com.bubblesocialapp.bubble.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import com.bubblesocialapp.bubble.R;


/**
 * Created by Sadaf on 2/3/2015.
 */
public class CameraDialog extends DialogFragment {

    DialogFragmentClickListener mListener;

    public interface DialogFragmentClickListener{

        public void onDialogCameraClick(android.support.v4.app.DialogFragment dialog);

        public void onDialogGalleryClick(android.support.v4.app.DialogFragment dialog);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Source")
                .setPositiveButton(R.string.camera, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mListener.onDialogCameraClick(CameraDialog.this);
                    }
                })
                .setNegativeButton(R.string.gallery, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mListener.onDialogGalleryClick(CameraDialog.this);
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (DialogFragmentClickListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement DialogFragmentClickListener");
        }
    }


}



