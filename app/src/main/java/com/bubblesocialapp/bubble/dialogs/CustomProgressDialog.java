package com.bubblesocialapp.bubble.dialogs;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;

import com.bubblesocialapp.bubble.R;

/**
 * Created by sadaf on 7/31/15.
 */
public class CustomProgressDialog extends ProgressDialog {

    public static ProgressDialog newInstance(Context context) {
        CustomProgressDialog customProgressDialog = new CustomProgressDialog(context);
        customProgressDialog.setIndeterminate(true);
        customProgressDialog.setCancelable(false);
        return customProgressDialog;
    }

    public CustomProgressDialog(Context context) {
        super(context);
    }

    public CustomProgressDialog(Context context, int theme) {
        super(context, theme);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_loading);
    }
}
