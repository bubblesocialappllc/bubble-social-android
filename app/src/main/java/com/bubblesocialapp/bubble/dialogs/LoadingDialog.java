package com.bubblesocialapp.bubble.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.Window;

import com.bubblesocialapp.bubble.R;

/**
 * Created by sadaf on 6/8/15.
 */
public class LoadingDialog extends DialogFragment {

    public LoadingDialog() {
        super();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Dialog progressDialog = new Dialog(getActivity(),android.R.style.Theme_Translucent);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_loading);

        return progressDialog;
    }
}
