//package com.bubblesocialapp.bubble.obsoleteCode;
//
//import android.app.Activity;
//import android.content.Intent;
//import android.net.Uri;
//import android.os.Bundle;
//import android.provider.MediaStore;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.app.FragmentTransaction;
//
//import com.bubblesocialapp.bubble.activities.BaseActivity;
//import com.bubblesocialapp.bubble.dialogs.CameraDialog;
//import com.bubblesocialapp.bubble.R;
//import com.bubblesocialapp.bubble.fragments.FeedFragment;
//import com.bubblesocialapp.bubble.fragments.PhotoFragment;
//import com.bubblesocialapp.bubble.fragments.ProfileFragment;
//import com.bubblesocialapp.bubble.fragments.SeeCurrentBuddiesFragment;
//import com.parse.ParseUser;
//
//
//public class ProfileActivity extends BaseActivity implements
//        CameraDialog.DialogFragmentClickListener, ProfileFragment.OnProfileClickListener{
//
//    private static final String SHOW_POSTS_HAPPENING_NOW = "Show_current_posts";
//
//    private static final String SHOW_POPULAR_POSTS = "Popular_posts";
//
//    private static final String SHOW_USERS_OLD_POSTS = "Old_posts";
//
//    Fragment fragment = null;
//    Bundle args = new Bundle();
//    private static final int REQUEST_PHOTO = 100;
//    private static final int REQUEST_GALLARY_PHOTO = 200;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//        fragment = ProfileFragment.newInstance(ParseUser.getCurrentUser().getObjectId(), ParseUser.getCurrentUser().getUsername());
//        FragmentManager fragmentManager = getSupportFragmentManager();
//        fragmentManager.beginTransaction()
//                .replace(R.id.screen_default_container, fragment)
//                .commit();
//
//    }
//
//    @Override
//    protected int setLayoutResourceIdentifier() {
//        return R.layout.activity_profile;
//    }
//
//    @Override
//    protected int getTitleToolBar() {
//        return R.string.app_name;
//    }
//
//
//
//    @Override
//    public void onDialogCameraClick(android.support.v4.app.DialogFragment dialog) {
//        startCamera();
//    }
//
//    @Override
//    public void onDialogGalleryClick(android.support.v4.app.DialogFragment dialog) {
//        chooseImage();
//    }
//
//    public void startCamera() {
//        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        startActivityForResult(intent, REQUEST_PHOTO);
//    }
//
//
//    private void chooseImage() {
//        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//        startActivityForResult(intent, REQUEST_GALLARY_PHOTO);
//    }
//
//
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == REQUEST_PHOTO) {
//            if (resultCode == Activity.RESULT_OK) {
//                onImageChosen(data.getData());
//            }
//        } else if (requestCode == REQUEST_GALLARY_PHOTO) {
//            if (resultCode == Activity.RESULT_OK) {
//                onImageChosen(data.getData());
//            }
//        }
//    }
//
//    public void onImageChosen(final Uri image) {
//        runOnUiThread(new Runnable() {
//
//            @Override
//            public void run() {
//                    if (image != null) {
//                        // Call Fragment method and add picture
//                        ProfileFragment profileFrag = (ProfileFragment)
//                                getSupportFragmentManager().findFragmentById(R.id.screen_default_container);
//                        profileFrag.setProfilePic(image);
//                    }
//                }
//        });
//    }
//
//    @Override
//    public void onBuddiesClick(String username) {
//        SeeCurrentBuddiesFragment fragment = new SeeCurrentBuddiesFragment();
//        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        transaction.replace(R.id.screen_default_container, fragment);
//        transaction.addToBackStack("CurrentBuddies");
//        transaction.commit();
//    }
//
//    @Override
//    public void onPhotosClick(String username) {
//        PhotoFragment fragment = new PhotoFragment();
//        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        transaction.replace(R.id.screen_default_container, fragment);
//        transaction.addToBackStack("Photos");
//        transaction.commit();
//    }
//
//    @Override
//    public void onPreviousPostsClick() {
//        FeedFragment fragment = FeedFragment.newInstance(SHOW_USERS_OLD_POSTS);
//        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        transaction.replace(R.id.screen_default_container, fragment);
//        transaction.addToBackStack("PreviousPosts");
//        transaction.commit();
//    }
//
//    @Override
//    public void onMessageUserClick(String username) {
//
//    }
//}
