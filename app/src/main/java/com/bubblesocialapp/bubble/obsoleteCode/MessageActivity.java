//package com.bubblesocialapp.bubble.obsoleteCode;
//
//import android.net.Uri;
//import android.os.Bundle;
//import android.support.v4.app.FragmentTransaction;
//import android.view.Menu;
//import android.view.MenuItem;
//
//import com.bubblesocialapp.bubble.R;
//import com.bubblesocialapp.bubble.activities.BaseActivity;
//import com.bubblesocialapp.bubble.adapters.SearchAdapter;
//import com.bubblesocialapp.bubble.fragments.CommentsFragment;
//import com.bubblesocialapp.bubble.fragments.MessageDetailFragment;
//import com.bubblesocialapp.bubble.fragments.MessageFragment;
//import com.bubblesocialapp.bubble.fragments.SearchFragment;
//import com.parse.ParseObject;
//import com.parse.ParseUser;
//
//public class MessageActivity extends BaseActivity
//        implements MessageFragment.OnMessageClickListener, SearchAdapter.onSearchResultClickListener {
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        if (savedInstanceState == null) {
//            getSupportFragmentManager().beginTransaction()
//                    .add(R.id.screen_default_container, new MessageFragment())
//                    .commit();
//        }
//    }
//
//
//    @Override
//    protected int setLayoutResourceIdentifier() {
//        return R.layout.activity_message;
//    }
//
//    @Override
//    protected int getTitleToolBar() {
//        return R.string.app_name;
//    }
//
//    @Override
//    public void onMessageHeadClick(String objectId) {
//        MessageDetailFragment detailFragment =
//                MessageDetailFragment.newInstance(objectId,null,null);
//        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        transaction.replace(R.id.screen_default_container, detailFragment);
//        transaction.addToBackStack(null);
//        transaction.commit();
//    }
//
//    @Override
//    public void onNewMessageClick() {
//        SearchFragment searchFragment = new SearchFragment();
//        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        transaction.replace(R.id.screen_default_container, searchFragment);
//        transaction.addToBackStack(null);
//        transaction.commit();
//    }
//
//
//    @Override
//    public void onSearchResultClick(ParseUser parseObject) {
//        MessageDetailFragment detailFragment =
//                MessageDetailFragment.newInstance(null,parseObject.getObjectId(), parseObject.getUsername());
//        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        transaction.replace(R.id.screen_default_container, detailFragment);
//        transaction.commit();
//    }
//
//}
