///**
// * Copyright 2014-present Amberfog
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *    http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
// */
//
//package com.bubblesocialapp.bubble.obsoleteCode;
//
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentTransaction;
//
//
//import com.bubblesocialapp.bubble.R;
//
//import com.bubblesocialapp.bubble.activities.BaseActivity;
//import com.bubblesocialapp.bubble.common.ParseCloudCode;
//import com.bubblesocialapp.bubble.fragments.ProfileFragment;
//import com.parse.ParseUser;
//
//
//public class AroundMeActivity extends BaseActivity
//        implements AroundMeAdapter.OnUserClickListener,
//        ProfileFragment.OnProfileClickListener{
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        if (savedInstanceState == null) {
//            getSupportFragmentManager().beginTransaction()
//                    .add(R.id.screen_default_container, new AroundMeBaseFragment())
//                    .commit();
//        }
//    }
//
//    @Override
//    protected int setLayoutResourceIdentifier() {
//        return R.layout.activity_around_me;
//    }
//
//    @Override
//    protected int getTitleToolBar() {
//        return R.string.app_name;
//    }
//
//    @Override
//    public void onUserClicked(String user, String username) {
//        Fragment profileFragment = ProfileFragment.newInstance(user, username);
//        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        transaction.replace(R.id.screen_default_container, profileFragment);
//        transaction.addToBackStack("aroundMeLogin");
//        transaction.commit();
//    }
//
//    @Override
//    public void onMessageUserClick(String username) {
//
//    }
//
//    @Override
//    public void onBuddiesClick(String username) {
//        ParseCloudCode.setPendingBuddy(this, username, ParseUser.getCurrentUser().getUsername());
//    }
//
//    @Override
//    public void onPhotosClick(String username) {
//
//    }
//
//    @Override
//    public void onPreviousPostsClick() {
//
//    }
//}
