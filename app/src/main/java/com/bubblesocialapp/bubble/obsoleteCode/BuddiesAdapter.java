//package com.bubblesocialapp.bubble.adapters;
//
//import android.content.Context;
//import android.graphics.Bitmap;
//import android.graphics.Color;
//import android.graphics.Typeface;
//import android.graphics.drawable.Drawable;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.amulyakhare.textdrawable.TextDrawable;
//import com.amulyakhare.textdrawable.util.ColorGenerator;
//import com.beardedhen.androidbootstrap.BootstrapCircleThumbnail;
//import com.bubblesocialapp.bubble.R;
//import com.bubblesocialapp.bubble.common.ParseCloudCode;
//import com.bubblesocialapp.bubble.parseObjects.Post;
//import com.parse.ParseObject;
//import com.parse.ParseUser;
//import com.squareup.picasso.Picasso;
//import com.squareup.picasso.Target;
//
//import java.util.List;
//
//import de.hdodenhof.circleimageview.CircleImageView;
//
//
///**
// * Created by Sadaf on 2/19/2015.
// */
//public class BuddiesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
//
//    private List<ParseObject> mParseObjects;
//    private final int Buddy_Requests = 0;
//    private final int Post_View = 1;
//    private Context mContext;
//    private View view;
//    private String nameToDisplay;
//    private ParseUser currentUser;
//    private AdapterCallBack mCallBack;
//
//
//    public interface AdapterCallBack {
//        public void startCommentsFragment(String string, String authorUsername);
//    }
//
//    public BuddiesAdapter(Context context, List<ParseObject> objects){
//
//        mParseObjects = objects;
//        mContext = context;
//        currentUser = ParseUser.getCurrentUser();
//        try {
//            this.mCallBack = ((AdapterCallBack) context);
//        } catch (ClassCastException e) {
//            throw new ClassCastException("Activity must implement AdapterCallback.");
//        }
//    }
//
//    @Override
//    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        switch (viewType) {
//            case (Buddy_Requests):
//                view = LayoutInflater.from(mContext).inflate(R.layout.buddy_request, parent, false);
//                return new ViewHolderBuddies(view);
//            case (Post_View):
//                view = LayoutInflater.from(mContext).inflate(R.layout.fragment_feed_item, parent, false);
//                return new ViewHolderPost(view);
//        }
//
//        return null;
//    }
//
//
//    @Override
//    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
//
//        int viewType = getItemViewType(position);
//
//        if (viewType == Buddy_Requests){
//            // View Type Buddy Request
//            populateBuddies(holder, position);
//
//        } else if (viewType == Post_View){
//            // View Type Post!
//            populateFeed(holder, position);
//
//        }
//
//    }
//
//    private void populateBuddies(RecyclerView.ViewHolder holder, final int position){
//        ViewHolderBuddies mHolder = (ViewHolderBuddies) holder;
//        ParseUser user = (ParseUser) mParseObjects.get(position);
//        final String username = user.getUsername();
//        final String currentUser = ParseUser.getCurrentUser().getUsername();
//
//        String profilePicFile = user.getParseFile("profilePicture").getUrl();
//
//        if (profilePicFile != null) {
//            mHolder.profilePicture.setVisibility(View.VISIBLE);
//            Picasso.with(mHolder.profilePicture.getContext())
//                    .load(profilePicFile)
//                    .error(R.drawable.ic_error)
//                    .placeholder(R.drawable.ic_stub)
//                    .resize(52, 52).centerCrop()
//                    .into(mHolder.profilePicture);
//        }
//        mHolder.username.setText(username);
//
//        mHolder.denyButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ParseCloudCode.denyBuddy(username, currentUser);
//                mParseObjects.remove(position);
//                notifyDataSetChanged();
//            }
//        });
//
//        mHolder.acceptButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ParseCloudCode.acceptBuddy(username, currentUser);
//                mParseObjects.remove(position);
//                notifyDataSetChanged();
//            }
//        });
//    }
//
//    private void populateFeed(RecyclerView.ViewHolder holder, final int position) {
//        final ViewHolderPost mHolder = (ViewHolderPost) holder;
//        final Post post = (Post) mParseObjects.get(position);
//
//        mHolder.profileImageActualImage.setVisibility(View.GONE);
//        mHolder.profileImage.setVisibility(View.INVISIBLE);
//        mHolder.postImage.setImageDrawable(null);
//        mHolder.authorName.setText(null);
//        mHolder.comments.setText(null);
//        mHolder.dateString.setText(null);
//        mHolder.locationName.setText(null);
//        mHolder.numberOfLikes.setText(null);
//        mHolder.textContent.setText(null);
//        mHolder.objectIdString.setText(null);
//
//        String profilePicFile = post.getProfilePicURL();
//
//        if (profilePicFile != null) {
//            mHolder.profileImageActualImage.setVisibility(View.VISIBLE);
//            Picasso.with(mHolder.profileImageActualImage.getContext())
//                    .load(profilePicFile)
//                    .error(R.drawable.ic_error)
//                    .placeholder(R.drawable.ic_stub)
//                    .resize(52, 52).centerCrop()
//                    .into(mHolder.profileImageActualImage);
//        } else {
//            setUpTemporaryPicture(mHolder,post);
//        }
//
//        String imageFile = post.getPostImageURL();
//
//        if (imageFile != null) {
//            mHolder.postImage.setVisibility(View.VISIBLE);
//            Picasso.with(mHolder.postImage.getContext())
//                    .load(imageFile)
//                    .error(R.drawable.ic_error)
//                    .placeholder(R.drawable.ic_stub)
//                    .into(mHolder.postImage);
//        } else {
//            mHolder.postImage.setVisibility(View.GONE);
//        }
//
//        mHolder.authorName.setText(post.getAuthorUsername());
//        mHolder.locationName.setText(post.getLocationName());
//        mHolder.textContent.setText(post.getText());
//        mHolder.dateString.setText(post.getPostTime());
//        mHolder.objectIdString.setText(post.getObjectId());
//
//        String objectIdText = mHolder.objectIdString.getText().toString();
//
//        setUpLikeClick(mHolder, post, objectIdText);
//
//        setUpComments(mHolder, post, objectIdText);
//
//    }
//
//    private void setUpTemporaryPicture(ViewHolderPost mHolder, Post post){
//        // Add Auto Profile Picture
//        ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
//        // generate random color
//        int color1 = generator.getRandomColor();
//        mHolder.profileImage.setVisibility(View.VISIBLE);
//        String profileName = post.getAuthorUsername();
//        if (profileName != null) {
//            String[] parseName = profileName.split(" ", 0);
//            if (parseName.length != 1) {
//                nameToDisplay = parseName[0].substring(0, 1) +
//                        parseName[1].substring(0, 1);
//            } else {
//                nameToDisplay = parseName[0].substring(0, 2);
//            }
//
//        } else {
//            nameToDisplay = "NA";
//        }
//
//        TextDrawable drawable = TextDrawable.builder()
//                .beginConfig()
//                .textColor(Color.WHITE)
//                .useFont(Typeface.DEFAULT)
//                .fontSize(30) /* size in px */
//                .bold()
//                .toUpperCase()
//                .endConfig()
//                .buildRound(nameToDisplay, color1);
//
//        mHolder.profileImage.setImageDrawable(drawable);
//    }
//
//    private void setUpComments(final ViewHolderPost mHolder, Post post, final String objectIdText){
//        mHolder.commenters = post.getCommenters();
//        if (mHolder.commenters != null) {
//            if (mHolder.commenters.contains(currentUser.getUsername())) {
//                mHolder.comments.setBackgroundResource(R.drawable.buttonshape_clicked);
//            } else {
//                mHolder.comments.setBackgroundResource(R.drawable.buttonshape);
//            }
//        }
//
//        mHolder.comments.setText("Comments | " + Integer.toString(post.getNumberOfComments()));
//        mHolder.comments.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mCallBack.startCommentsFragment(objectIdText, mHolder.authorName.getText().toString());
//            }
//        });
//    }
//
//    private void setUpLikeClick(final ViewHolderPost mHolder, Post post, final String objectIdText){
//        // Check if already liked
//        mHolder.likers = post.getLikedUsers();
//        if (mHolder.likers != null) {
//            if (mHolder.likers.contains(currentUser.getUsername())) {
//                mHolder.liked = true;
//                mHolder.numberOfLikes.setBackgroundResource(R.drawable.buttonshape_clicked);
//            } else {
//                mHolder.liked = false;
//                mHolder.numberOfLikes.setBackgroundResource(R.drawable.buttonshape);
//            }
//        }
//
//        mHolder.numberLiked = post.getLikes();
//        if (post.getLikes() != 0) {
//            mHolder.numberOfLikes.setText("Likes | " + Integer.toString(mHolder.numberLiked));
//        } else {
//            mHolder.numberOfLikes.setText("Likes | 0");
//        }
//
//        // Set ClickListener
//        mHolder.numberOfLikes.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (!mHolder.liked) {
//                    ParseCloudCode.likePost(objectIdText);
//                    mHolder.numberOfLikes.setBackgroundResource(R.drawable.buttonshape_clicked);
//                    mHolder.liked = true;
//                    mHolder.numberLiked += 1;
//                    mHolder.numberOfLikes.setText("Likes | " + Integer.toString(mHolder.numberLiked));
//                } else {
//                    ParseCloudCode.unlikePost(currentUser.toString(), objectIdText);
//                    mHolder.numberOfLikes.setBackgroundResource(R.drawable.buttonshape);
//                    mHolder.liked = false;
//                    mHolder.numberLiked -= 1;
//                    mHolder.numberOfLikes.setText("Likes | " + Integer.toString(mHolder.numberLiked));
//                }
//            }
//        });
//    }
//
//    @Override
//    public int getItemCount() {
//        return mParseObjects.size();
//    }
//
//    @Override
//    public int getItemViewType(int position) {
//        String viewType = mParseObjects.get(position).getClassName();
//        switch (viewType){
//            case ("_User"):
//                return Buddy_Requests;
//            case ("Post"):
//                return Post_View;
//            default:
//                return Buddy_Requests;
//        }
//    }
//
//
//    public static class ViewHolderPost extends RecyclerView.ViewHolder{
//        public ImageView postImage;
//        public ImageView profileImage;
//        public CircleImageView profileImageActualImage;
//        public TextView authorName;
//        public TextView locationName;
//        public TextView textContent;
//        public TextView dateString;
//        public TextView objectIdString;
//        public Button numberOfLikes;
//        public Button comments;
//        public Boolean liked;
//        public List<String> likers;
//        public int numberLiked;
//        public List<String> commenters;
//
//        public ViewHolderPost(View v) {
//            super(v);
//
//            profileImageActualImage = (CircleImageView) v.findViewById(R.id.profile_picture_circle_imageView);
//            postImage = (ImageView) v.findViewById(R.id.shared_image_imageView);
//            profileImage = (ImageView) v.findViewById(R.id.profile_picture_imageView);
//            authorName = (TextView) v.findViewById(R.id.username_textView);
//            locationName = (TextView) v.findViewById(R.id.post_location_textView);
//            textContent = (TextView) v.findViewById(R.id.comment_textView);
//            dateString = (TextView) v.findViewById(R.id.post_time_textView);
//            objectIdString = (TextView) v.findViewById(R.id.storeObjectId);
//            numberOfLikes = (Button) v.findViewById(R.id.likeButton);
//            comments = (Button) v.findViewById(R.id.commentButton);
//            liked = false;
//            likers = null;
//            numberLiked = 0;
//            commenters = null;
//        }
//
//
//    }
//
//    public static class ViewHolderBuddies extends RecyclerView.ViewHolder{
//        TextView username;
//        Button acceptButton;
//        Button denyButton;
//        ImageView profilePicture;
//
//        public ViewHolderBuddies(View v){
//            super(v);
//
//            username = (TextView) v.findViewById(R.id.buddy_request_username);
//            acceptButton = (Button) v.findViewById(R.id.buddy_request_accept);
//            denyButton = (Button) v.findViewById(R.id.buddy_request_deny);
//            profilePicture = (ImageView) v.findViewById(R.id.buddy_request_profile_picture);
//        }
//    }
//
//
//}
//
//
//
//
//
//
//
//
//
//
//
//
//
