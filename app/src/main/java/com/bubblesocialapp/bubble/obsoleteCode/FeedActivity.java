//package com.bubblesocialapp.bubble.obsoleteCode;
//
//import android.support.v4.app.FragmentTransaction;
//import android.os.Bundle;
//import android.support.v4.view.ViewPager;
//
//import com.bubblesocialapp.bubble.activities.BaseActivity;
//import com.bubblesocialapp.bubble.obsoleteCode.AroundMeAdapter;
//import com.bubblesocialapp.bubble.adapters.BuddiesAdapter;
//import com.bubblesocialapp.bubble.adapters.FeedAdapter;
//import com.bubblesocialapp.bubble.adapters.HomeScreenPagerAdapter;
//import com.bubblesocialapp.bubble.fragments.FeedFragment;
//import com.bubblesocialapp.bubble.obsoleteCode.BaseFragment;
//import com.bubblesocialapp.bubble.fragments.CommentsFragment;
//import com.bubblesocialapp.bubble.fragments.PostFragment;
//import com.bubblesocialapp.bubble.R;
//import com.bubblesocialapp.bubble.fragments.ProfileFragment;
//import com.parse.ParseUser;
//
//public class FeedActivity extends BaseActivity
//        implements FeedFragment.PostListener,
//        FeedAdapter.AdapterCallBack,
//        BuddiesAdapter.AdapterCallBack,
//        AroundMeAdapter.OnUserClickListener,
//        ProfileFragment.OnProfileClickListener{
//
//
//    ParseUser user = new ParseUser();
//    public final static String extraMessage = "PostId";
//    private ViewPager mViewPager;
//    private HomeScreenPagerAdapter mFeedBuddyNotificationAdapter;
//    private static final String FEED_DATA = "feed_data";
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        String dataString = getIntent().getStringExtra(FEED_DATA);
//        if (dataString != null) {
//            getSupportFragmentManager().beginTransaction()
//                    .add(R.id.screen_default_container, BaseFragment.newInstance(dataString))
//                    .commit();
//        } else {
//            if (savedInstanceState == null) {
//                getSupportFragmentManager().beginTransaction()
//                        .add(R.id.screen_default_container, new BaseFragment())
//                        .commit();
//            }
//        }
//
//    }
//
//    @Override
//    protected int setLayoutResourceIdentifier() {
//        return R.layout.activity_feed;
//    }
//
//    @Override
//    protected int getTitleToolBar() {
//        return R.string.app_name;
//    }
//
//    /**
//     * Replace current fragment with
//     * post fragment when post button is pressed
//     */
//    @Override
//    public void onPostButtonPressed() {
//
//        // Create fragment and give it an argument for the selected article
//        PostFragment newFragment = new PostFragment();
//        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        transaction.replace(R.id.screen_default_main_container, newFragment);
//        transaction.addToBackStack("Main");
//        // Commit the transaction
//        transaction.commit();
//
//    }
//
//    @Override
//    public void startCommentsFragment(String objectId) {
//        CommentsFragment commentsFragment = new CommentsFragment();
//        Bundle args = new Bundle();
//        args.putString(extraMessage, objectId);
//        commentsFragment.setArguments(args);
//        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        transaction.replace(R.id.screen_default_main_container, commentsFragment);
//        transaction.addToBackStack(null);
//        transaction.commit();
//    }
//
//    @Override
//    public void onBuddiesClick(String username) {
//
//    }
//
//    @Override
//    public void onPhotosClick(String username) {
//
//    }
//
//    @Override
//    public void onPreviousPostsClick() {
//
//    }
//
//    @Override
//    public void onMessageUserClick(String username) {
//
//    }
//
//    @Override
//    public void onUserClicked(String userObjectId, String username) {
//
//    }
//}
//
//// Obsolete code: Ignore for now
//// Inflate a menu to be displayed in the toolbar
////        Toolbar toolbar = (Toolbar) findViewById(R.id.main_activity_toolbar);
////        setSupportActionBar(toolbar);
////        Toolbar.LayoutParams layout = new Toolbar.LayoutParams(Toolbar.LayoutParams.MATCH_PARENT, Toolbar.LayoutParams.MATCH_PARENT);
////        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
////        View view = inflater.inflate(R.layout.main_user_menu, null);
////        getSupportActionBar().setCustomView(view, layout);
////        //View view =getSupportActionBar().getCustomView();
////        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
