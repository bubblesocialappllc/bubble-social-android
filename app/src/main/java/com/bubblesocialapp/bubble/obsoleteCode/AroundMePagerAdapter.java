//package com.bubblesocialapp.bubble.obsoleteCode;
//
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.app.FragmentPagerAdapter;
//import android.view.ViewGroup;
//
//import com.bubblesocialapp.bubble.fragments.AroundMeFragment;
//import com.bubblesocialapp.bubble.fragments.MapFragment;
//
///**
// * Created by sadaf on 4/28/15.
// */
//public class AroundMePagerAdapter extends FragmentPagerAdapter {
//    private static final String[] CONTENT = new String[]{"Bubblers", "Event Map"};
//    public AroundMePagerAdapter(FragmentManager fragmentManager) {
//        super(fragmentManager);
//    }
//
//    @Override
//    public Fragment getItem(int position) {
//        switch (position) {
//            case (0):
//                return new AroundMeFragment();
//            case (1):
//                return new MapFragment();
//            default:
//                return new AroundMeFragment();
//        }
//    }
//
//    @Override
//    public int getCount() {
//        return 2;
//    }
//
//    @Override
//    public CharSequence getPageTitle(int position) {
//        switch (position) {
//            case (0):
//                return CONTENT[position % CONTENT.length];
//            case (1):
//                return CONTENT[position % CONTENT.length];
//            default:
//                return CONTENT[position % CONTENT.length];
//        }
//    }
//
//}
