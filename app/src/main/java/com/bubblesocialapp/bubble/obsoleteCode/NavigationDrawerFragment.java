//package com.bubblesocialapp.bubble.fragments;
//
//
//import android.app.Activity;
//import android.content.Context;
//import android.content.SharedPreferences;
//import android.content.res.Configuration;
//import android.content.res.Resources;
//import android.graphics.Color;
//import android.graphics.Typeface;
//import android.os.Bundle;
//import android.preference.PreferenceManager;
//import android.support.v4.app.Fragment;
//import android.support.v4.widget.DrawerLayout;
//import android.support.v7.app.ActionBarDrawerToggle;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.MenuItem;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.AdapterView;
//import android.widget.CompoundButton;
//import android.widget.ImageView;
//import android.widget.ListView;
//import android.widget.TextView;
//
//import com.amulyakhare.textdrawable.TextDrawable;
//import com.bubblesocialapp.bubble.R;
//import com.bubblesocialapp.bubble.obsoleteCode.DrawerMenuAdapter;
//import com.bubblesocialapp.bubble.obsoleteCode.bean.DrawerMenuBean;
//import com.parse.ParseUser;
//
//import java.util.ArrayList;
//
//import de.hdodenhof.circleimageview.CircleImageView;
//
//
//public class NavigationDrawerFragment extends Fragment {
//    private static final String TAG = NavigationDrawerFragment.class.getSimpleName();
//
//
//    private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";
//
//
//    private static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";
//
//    private static final String USER_PICKED_SEARCH_RADIUS = "user_picked_search_radius";
//
//    private static final String SHARE_LOCATION = "Share_location";
//
//    /**
//     * A pointer to the current callbacks instance (the Activity).
//     */
//    private NavigationDrawerCallbacks mCallbacks;
//
//    protected Context mContext;
//    protected Activity mActivity;
//
//    private ActionBarDrawerToggle mDrawerToggle;
//    private DrawerLayout mDrawerLayout;
//    private View mDrawerView;
//    private View mFragmentContainerView;
//    private int mCurrentSelectedPosition = 0;
//    private boolean mFromSavedInstanceState;
//    private boolean mUserLearnedDrawer;
////    private SeekArc mSeekArc;
//    private TextView mRangeTextView;
//    private CircleImageView mProfilePicture;
//    private ImageView mDistanceView;
//    private ListView mDrawerListView;
////    private SwitchButton mSwitch;
//    private int mUserSearchRadius;
//    private boolean mShareLocation;
//    private int mRange;
//
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//        // Read in the flag indicating whether or not the user has
//        // s demonstrated awareness of the
//        // drawer. See PREF_USER_LEARNED_DRAWER for details.
//        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
//        mUserLearnedDrawer = sp.getBoolean(PREF_USER_LEARNED_DRAWER, false);
//        mUserSearchRadius = sp.getInt(USER_PICKED_SEARCH_RADIUS, 5);
//        mShareLocation = sp.getBoolean(SHARE_LOCATION, true);
//
//        if (savedInstanceState != null) {
//            mCurrentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION);
//            mFromSavedInstanceState = true;
//        }
//
//    }
//
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        final SharedPreferences sp = PreferenceManager
//                .getDefaultSharedPreferences(getActivity());
//
//        mDrawerView = inflater.inflate(R.layout.fragment_drawer_menu, container, false);
//
//        mDrawerListView = (ListView) mDrawerView.findViewById(R.id.fragment_drawerMenu_listView);
//        mProfilePicture = (CircleImageView) mDrawerView.findViewById(R.id.profile_picture);
//        mDistanceView = (ImageView) mDrawerView.findViewById(R.id.bubble_distance);
//        mSwitch = (SwitchButton) mDrawerView.findViewById(R.id.share_location_switch);
//
//        mSwitch.setChecked(true);
//
//        mSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                sp.edit().putBoolean(SHARE_LOCATION, b);
//            }
//        });
//
//        try {
//            String profileUrl = ParseUser.getCurrentUser().getParseFile("profilePicture").getUrl();
//            Picasso.with(mProfilePicture.getContext())
//                    .load(profileUrl)
//                    .error(R.drawable.ic_error)
//                    .placeholder(R.drawable.ic_stub)
//                    .into(mProfilePicture);
//
//        } catch (NullPointerException e){
//            Log.d("error", "Profile Picture does not exist");
//        }
//
//
//        TextDrawable drawable = TextDrawable.builder()
//                .beginConfig()
//                .textColor(Color.WHITE)
//                .useFont(Typeface.DEFAULT)
//                .fontSize(30) /* size in px */
//                .bold()
//                .toUpperCase()
//                .endConfig()
//                .buildRound("0",R.color.bubble_pink);
//
//        mDistanceView.setImageDrawable(drawable);
//
//
//        // Seek Bar. Progress goes from 0-100. Divide by 10
//        mSeekArc = (SeekArc) mDrawerView.findViewById(R.id.distance_seek_arc);
//        mSeekArc.setArcRotation(0);
//        mSeekArc.setTouchInSide(false);
//        mSeekArc.setClockwise(true);
//        mSeekArc.setSweepAngle(360);
//        mSeekArc.setStartAngle(0);
//        mSeekArc.setProgressWidth(10);
//        mSeekArc.setArcWidth(5);
//        mSeekArc.setProgress(mUserSearchRadius*10);
//        mSeekArc.setOnSeekArcChangeListener(new SeekArc.OnSeekArcChangeListener() {
//            @Override
//            public void onProgressChanged(SeekArc seekArc, int range, boolean b) {
//                mUserSearchRadius = range;
//                TextDrawable drawable = TextDrawable.builder()
//                        .beginConfig()
//                        .textColor(Color.WHITE)
//                        .useFont(Typeface.DEFAULT)
//                        .fontSize(30) /* size in px */
//                        .bold()
//                        .toUpperCase()
//                        .endConfig()
//                        .buildRound(String.valueOf(range / 10), R.color.bubble_pink); // Transparent
//
//                mRange = range;
//                mDistanceView.setImageDrawable(drawable);
//            }
//
//            @Override
//            public void onStartTrackingTouch(SeekArc seekArc) {
//                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN);
//            }
//
//            @Override
//            public void onStopTrackingTouch(SeekArc seekArc) {
//                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
//                sp.edit().putInt(USER_PICKED_SEARCH_RADIUS, mRange / 10).apply();
//            }
//        });
//
//        loadListeners();
//        loadInfoView();
//
//        return mDrawerView;
//    }
//
//    @Override
//    public void onActivityCreated(Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//        setHasOptionsMenu(true);
//    }
//
//    @Override
//    public void onAttach(Activity activity) {
//        super.onAttach(activity);
//        this.mActivity = activity;
//        this.mContext = mActivity.getApplicationContext();
//        try {
//            mCallbacks = (NavigationDrawerCallbacks) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
//        }
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mCallbacks = null;
//    }
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        mActivity = null;
//        mContext = null;
//    }
//
//    @Override
//    public void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
//        outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);
//    }
//
//    @Override
//    public void onConfigurationChanged(Configuration newConfig) {
//        super.onConfigurationChanged(newConfig);
//        // Forward the new configuration the drawer toggle component.
//        mDrawerToggle.onConfigurationChanged(newConfig);
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        if (item.getItemId() == android.R.id.home) {
//            if (mDrawerLayout != null) {
//                if (isDrawerOpen()) {
//                    mDrawerLayout.closeDrawer(mFragmentContainerView);
//                } else {
//                    mDrawerLayout.openDrawer(mFragmentContainerView);
//                }
//            } else {
//                getActivity().finish();
//            }
//        }
//        return super.onOptionsItemSelected(item);
//    }
//
//    private void loadListeners() {
//        mDrawerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                selectItem(position, view);
//            }
//        });
//    }
//
//    private void loadInfoView() {
//        ArrayList<DrawerMenuBean> menuDrawerListItems = loadMenuDrawerItems();
//        if (menuDrawerListItems != null) {
//            DrawerMenuAdapter drawerMenuAdapter = new DrawerMenuAdapter(mContext, menuDrawerListItems);
//
//            mDrawerListView.setAdapter(drawerMenuAdapter);
//            mDrawerListView.setItemChecked(mCurrentSelectedPosition, true);
//        }
//    }
//
//    private ArrayList<DrawerMenuBean> loadMenuDrawerItems() {
//        String[] menuDrawerTitleArray;
//        String[] menuDrawerImagesArray;
//        ArrayList<DrawerMenuBean> menuDrawerListItems = null;
//
//        try {
//            menuDrawerTitleArray = getActivity().getResources().getStringArray(R.array.fragment_drawerMenu_title);
//            menuDrawerImagesArray = getActivity().getResources().getStringArray(R.array.fragment_drawerMenu_images);
//
//            menuDrawerListItems = new ArrayList<>();
//            for (int position = 0; position < menuDrawerTitleArray.length; position++) {
//                String aMenuDrawerTitleArray = menuDrawerTitleArray[position];
//                String aMenuDrawerImagesArray = menuDrawerImagesArray[position];
//                menuDrawerListItems.add(new DrawerMenuBean(aMenuDrawerTitleArray, aMenuDrawerImagesArray));
//            }
//
//
//            return menuDrawerListItems;
//        } catch (Resources.NotFoundException notFoundExcepetion) {
//            Log.e(TAG, "Error Getting The Array", notFoundExcepetion);
//        }
//        return menuDrawerListItems;
//    }
//
//
//    public boolean isDrawerOpen() {
//        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
//    }
//
//    public void setUp(int fragmentId, DrawerLayout drawerLayout) {
//        mFragmentContainerView = getActivity().findViewById(fragmentId);
//        mDrawerLayout = drawerLayout;
//
//        // set a custom shadow that overlays the main content when the drawer opens
//        //mDrawerLayout.setDrawerShadow(R.drawable.ic_drawer_menu_shadow, GravityCompat.START);
//        // set up the drawer's list view with items and click listener
//
//
//        // ActionBarDrawerToggle ties together the the proper interactions
//        // between the navigation drawer and the action bar app icon.
//        mDrawerToggle = new ActionBarDrawerToggle(
//                getActivity(),                    /* host Activity */
//                mDrawerLayout,                    /* DrawerLayout object */
//                R.string.app_name,  /* "open drawer" description for accessibility */
//                R.string.app_name  /* "close drawer" description for accessibility */
//        ) {
//            @Override
//            public void onDrawerClosed(View drawerView) {
//                super.onDrawerClosed(drawerView);
//                if (!isAdded()) {
//                    return;
//                }
//
//                getActivity().supportInvalidateOptionsMenu(); // calls onPrepareOptionsMenu()
//            }
//
//            @Override
//            public void onDrawerOpened(View drawerView) {
//                super.onDrawerOpened(drawerView);
//                if (!isAdded()) {
//                    return;
//                }
//
//                if (!mUserLearnedDrawer) {
//                    // The user manually opened the drawer; store this flag to prevent auto-showing
//                    // the navigation drawer automatically in the future.
//                    mUserLearnedDrawer = true;
//                    SharedPreferences sp = PreferenceManager
//                            .getDefaultSharedPreferences(getActivity());
//                    sp.edit().putBoolean(PREF_USER_LEARNED_DRAWER, true).apply();
//                }
//
//                getActivity().supportInvalidateOptionsMenu(); // calls onPrepareOptionsMenu()
//            }
//        };
//
//        // If the user hasn't 'learned' about the drawer, open it to introduce them to the drawer,
//        // per the navigation drawer design guidelines.
//        if (!mUserLearnedDrawer && !mFromSavedInstanceState) {
//            mDrawerLayout.openDrawer(mFragmentContainerView);
//        }
//
//        // Defer code dependent on restoration of previous instance state.
//        mDrawerLayout.post(new Runnable() {
//            @Override
//            public void run() {
//                mDrawerToggle.syncState();
//            }
//        });
//
//        mDrawerLayout.setDrawerListener(mDrawerToggle);
//    }
//
//    private void selectItem(int position, View view) {
//        mCurrentSelectedPosition = position;
//        if (mDrawerListView != null) {
//            mDrawerListView.setItemChecked(position, true);
//            view.setBackgroundColor(getResources().getColor(R.color.bubble_pink));
//        }
//        if (mDrawerLayout != null) {
//            mDrawerLayout.closeDrawer(mFragmentContainerView);
//        }
//        if (mCallbacks != null) {
//            mCallbacks.onNavigationDrawerItemSelected(position);
//        }
//    }
//
//    public static interface NavigationDrawerCallbacks {
//        /**
//         * Called when an item in the navigation drawer is selected.
//         */
//        void onNavigationDrawerItemSelected(int position);
//    }
//
//}
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
