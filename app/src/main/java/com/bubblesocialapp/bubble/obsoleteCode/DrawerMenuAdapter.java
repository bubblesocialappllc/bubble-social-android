//package com.bubblesocialapp.bubble.obsoleteCode;
//
//import android.app.Activity;
//import android.content.Context;
//import android.content.res.TypedArray;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.bubblesocialapp.bubble.R;
//import com.bubblesocialapp.bubble.obsoleteCode.bean.DrawerMenuBean;
//
//import java.util.ArrayList;
//
//
//public class DrawerMenuAdapter extends BaseAdapter {
//    private Context mContext;
//    private ArrayList<DrawerMenuBean> mListItemsDrawerMenuBean;
//
//    public DrawerMenuAdapter(Context mContext, ArrayList<DrawerMenuBean> mListItemsDrawer) {
//        this.mContext = mContext;
//        this.mListItemsDrawerMenuBean = mListItemsDrawer;
//    }
//
//    @Override
//    public int getCount() {
//        return mListItemsDrawerMenuBean.size();
//    }
//
//    @Override
//    public Object getItem(int position) {
//        return mListItemsDrawerMenuBean.get(position);
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return position;
//    }
//
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        ViewHolder holder;
//
//        if (convertView == null) {
//            LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
//            convertView = mInflater.inflate(R.layout.fragment_drawer_menu_comp, null);
//
//            holder = new ViewHolder();
//            holder.mTitle = (TextView) convertView.findViewById(R.id.fragment_drawerMenu_comp_title);
//            holder.mPicture = (ImageView) convertView.findViewById(R.id.fragment_drawerMenu_comp_image);
//            convertView.setTag(holder);
//        } else {
//            holder = (ViewHolder) convertView.getTag();
//        }
//        holder.mTitle.setText(mListItemsDrawerMenuBean.get(position).getTitle());
//
//        TypedArray images = mContext.getResources().obtainTypedArray(R.array.fragment_drawerMenu_images);
//        holder.mPicture.setImageResource(images.getResourceId(position,-1));
//        images.recycle();
//
//        return convertView;
//    }
//
//    private static class ViewHolder {
//        TextView mTitle;
//        ImageView mPicture;
//    }
//}
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
