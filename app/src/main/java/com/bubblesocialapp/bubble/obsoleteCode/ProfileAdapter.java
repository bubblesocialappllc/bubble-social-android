//package com.bubblesocialapp.bubble.adapters;
//
//import android.content.Context;
//import android.graphics.Color;
//import android.graphics.Typeface;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.inputmethod.InputMethodManager;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.amulyakhare.textdrawable.TextDrawable;
//import com.amulyakhare.textdrawable.util.ColorGenerator;
//import com.beardedhen.androidbootstrap.BootstrapCircleThumbnail;
//import com.bubblesocialapp.bubble.R;
//import com.bubblesocialapp.bubble.common.ParseCloudCode;
//import com.bubblesocialapp.bubble.dialogs.CameraDialog;
//import com.bubblesocialapp.bubble.parseObjects.Post;
//import com.parse.ParseUser;
//import com.squareup.picasso.Picasso;
//
//import org.w3c.dom.Text;
//
//import java.util.List;
//
//import de.hdodenhof.circleimageview.CircleImageView;
//
///**
// * Created by sadaf on 7/14/15.
// */
//public class ProfileAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
//
//    private final int PROFILE_INFORMATION = 0;
//
//    private final int USER_POSTS = 1;
//
//    private ParseUser currentUser;
//    private List<Post> posts;
//    private Context mContext;
//    private View view;
//    private AdapterCallBack mCallBack;
//    private String nameToDisplay;
//    private ParseUser mUser;
//
//    public interface AdapterCallBack {
//        public void startCommentsFragment(String string);
//    }
//
//    public ProfileAdapter(Context context, List<Post> posts, ParseUser user) {
//        super();
//        this.posts = posts;
//        this.mContext = context;
//        this.mUser = user;
//
//        currentUser = ParseUser.getCurrentUser();
//        try {
//            this.mCallBack = ((AdapterCallBack) context);
//        } catch (ClassCastException e) {
//            throw new ClassCastException("Activity must implement AdapterCallback.");
//        }
//    }
//
//    @Override
//    public int getItemViewType(int position) {
//        switch (position){
//            case (0):
//                return PROFILE_INFORMATION;
//            default:
//                return USER_POSTS;
//        }
//    }
//
//    @Override
//    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        switch (viewType) {
//            case (PROFILE_INFORMATION):
//                view = LayoutInflater.from(mContext).inflate(R.layout.profile_user_info_cardview, parent, false);
//                return new ViewHolderProfile(view);
//            case (USER_POSTS):
//                view = LayoutInflater.from(mContext).inflate(R.layout.fragment_feed_item, parent, false);
//                return new ViewHolderPost(view);
//        }
//        return null;
//    }
//
//    @Override
//    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
//
//        int viewType = getItemViewType(position);
//
//        if (viewType == PROFILE_INFORMATION){
//            populateProfile(holder, position);
//        } else if (viewType == USER_POSTS) {
//            populateFeed(holder, position);
//        }
//    }
//
//    private void populateProfile(RecyclerView.ViewHolder holder, int position) {
//        final ViewHolderProfile mHolder = (ViewHolderProfile) holder;
//        mHolder.userName.setText(mUser.getString("realName"));
//        if (mUser.get("posts") != null) {
//            mHolder.postCount.setText(mUser.get("posts").toString() + " Pops");
//        } else {
//            mHolder.postCount.setText("0 Pops");
//        }
//
//        if ( mUser.getString("aboutMe") != null && !mUser.getString("aboutMe").isEmpty() ) {
//            mHolder.aboutMeTextView.setText(mUser.getString("aboutMe"));
//        }
//
//            mHolder.aboutMeTextView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    CharSequence text = mHolder.aboutMeTextView.getText();
//                    mHolder.aboutMeTextView.setVisibility(View.GONE);
//                    mHolder.aboutMeTextView.setVisibility(View.VISIBLE);
//                    mHolder.aboutMeTextView.setText(text);
//                    InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
//                    imm.showSoftInput(mHolder.aboutMeEditView, InputMethodManager.SHOW_IMPLICIT);
//                }
//            });
//
//
//            mHolder.aboutMeEditView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//                @Override
//                public void onFocusChange(View v, boolean hasFocus) {
//                    if (!hasFocus) {
//                        //Change Back to TexView
//                        CharSequence text = mHolder.aboutMeEditView.getText();
//                        mHolder.aboutMeEditView.setVisibility(View.GONE);
//                        mHolder.aboutMeTextView.setVisibility(View.VISIBLE);
//                        mHolder.aboutMeTextView.setText(text);
//
//                        // Hide Keyboard
//                        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
//                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
//
//
//                        ParseUser currentUser = ParseUser.getCurrentUser();
//                        currentUser.put("aboutMe", mHolder.aboutMeTextView.getText().toString());
//                        currentUser.saveInBackground();
//
//                    }
//                }
//            });
//
//        }
//
//    private void populateFeed(RecyclerView.ViewHolder holder, final int position) {
//        final ViewHolderPost mHolder = (ViewHolderPost) holder;
//        final Post post = posts.get(position - 1); // Subtract one because of the first view
//
//        mHolder.profileImageActualImage.setVisibility(View.GONE);
//        mHolder.profileImage.setVisibility(View.INVISIBLE);
//        mHolder.postImage.setImageDrawable(null);
//        mHolder.authorName.setText(null);
//        mHolder.comments.setText(null);
//        mHolder.dateString.setText(null);
//        mHolder.locationName.setText(null);
//        mHolder.numberOfLikes.setText(null);
//        mHolder.textContent.setText(null);
//        mHolder.objectIdString.setText(null);
//
//        String profilePicFile = post.getProfilePicURL();
//
//        if (profilePicFile != null) {
//            mHolder.profileImageActualImage.setVisibility(View.VISIBLE);
//            Picasso.with(mHolder.profileImageActualImage.getContext())
//                    .load(profilePicFile)
//                    .error(R.drawable.ic_error)
//                    .placeholder(R.drawable.ic_stub)
//                    .resize(52, 52).centerCrop()
//                    .into(mHolder.profileImageActualImage);
//        } else {
//            setUpTemporaryPicture(mHolder,post);
//        }
//
//        String imageFile = post.getPostImageURL();
//
//        if (imageFile != null) {
//            mHolder.postImage.setVisibility(View.VISIBLE);
//            Picasso.with(mHolder.postImage.getContext())
//                    .load(imageFile)
//                    .error(R.drawable.ic_error)
//                    .placeholder(R.drawable.ic_stub)
//                    .into(mHolder.postImage);
//        } else {
//            mHolder.postImage.setVisibility(View.GONE);
//        }
//
//        mHolder.authorName.setText(post.getAuthorUsername());
//        mHolder.locationName.setText(post.getLocationName());
//        mHolder.textContent.setText(post.getText());
//        mHolder.dateString.setText(post.getPostTime());
//        mHolder.objectIdString.setText(post.getObjectId());
//
//        String objectIdText = mHolder.objectIdString.getText().toString();
//
//        setUpLikeClick(mHolder, post, objectIdText);
//
//        setUpComments(mHolder, post, objectIdText);
//
//    }
//
//    private void setUpTemporaryPicture(ViewHolderPost mHolder, Post post){
//        // Add Auto Profile Picture
//        ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
//        // generate random color
//        int color1 = generator.getRandomColor();
//        mHolder.profileImage.setVisibility(View.VISIBLE);
//        String profileName = post.getAuthorUsername();
//        if (profileName != null) {
//            String[] parseName = profileName.split(" ", 0);
//            if (parseName.length != 1) {
//                nameToDisplay = parseName[0].substring(0, 1) +
//                        parseName[1].substring(0, 1);
//            } else {
//                nameToDisplay = parseName[0].substring(0, 2);
//            }
//
//        } else {
//            nameToDisplay = "NA";
//        }
//
//        TextDrawable drawable = TextDrawable.builder()
//                .beginConfig()
//                .textColor(Color.WHITE)
//                .useFont(Typeface.DEFAULT)
//                .fontSize(30) /* size in px */
//                .bold()
//                .toUpperCase()
//                .endConfig()
//                .buildRound(nameToDisplay, color1);
//
//        mHolder.profileImage.setImageDrawable(drawable);
//    }
//
//    private void setUpComments(ViewHolderPost mHolder, Post post, final String objectIdText){
//        mHolder.commenters = post.getCommenters();
//        if (mHolder.commenters != null) {
//            if (mHolder.commenters.contains(currentUser.getUsername())) {
//                mHolder.comments.setBackgroundResource(R.drawable.buttonshape_clicked);
//            } else {
//                mHolder.comments.setBackgroundResource(R.drawable.buttonshape);
//            }
//        }
//        String formattedString = mContext.getString(R.string.format_comments_likes, post.getNumberOfComments());
//        mHolder.comments.setText(formattedString);
//        mHolder.comments.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mCallBack.startCommentsFragment(objectIdText);
//            }
//        });
//    }
//
//    private void setUpLikeClick(final ViewHolderPost mHolder, Post post, final String objectIdText){
//        // Check if already liked
//        mHolder.likers = post.getLikedUsers();
//        if (mHolder.likers != null) {
//            if (mHolder.likers.contains(currentUser.getUsername())) {
//                mHolder.liked = true;
//                mHolder.numberOfLikes.setBackgroundResource(R.drawable.buttonshape_clicked);
//            } else {
//                mHolder.liked = false;
//                mHolder.numberOfLikes.setBackgroundResource(R.drawable.buttonshape);
//            }
//        }
//
//        mHolder.numberLiked = post.getLikes();
//        String formattedString = mContext.getString(R.string.format_comments_likes,mHolder.numberLiked);
//        if (post.getLikes() != 0) {
//            mHolder.numberOfLikes.setText(formattedString);
//        } else {
//            mHolder.numberOfLikes.setText(formattedString);
//        }
//
//        // Set ClickListener
//        mHolder.numberOfLikes.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String formattedString = mContext.getString(R.string.format_comments_likes,mHolder.numberLiked);
//                if (!mHolder.liked) {
//                    ParseCloudCode.likePost(currentUser.toString(), objectIdText);
//                    mHolder.numberOfLikes.setBackgroundResource(R.drawable.buttonshape_clicked);
//                    mHolder.liked = true;
//                    mHolder.numberLiked += 1;
//                    mHolder.numberOfLikes.setText(formattedString);
//                } else {
//                    ParseCloudCode.unlikePost(currentUser.toString(), objectIdText);
//                    mHolder.numberOfLikes.setBackgroundResource(R.drawable.buttonshape);
//                    mHolder.liked = false;
//                    mHolder.numberLiked -= 1;
//                    mHolder.numberOfLikes.setText(formattedString);
//                }
//            }
//        });
//    }
//
//    @Override
//    public int getItemCount() {
//
//            return posts.size() + 1;
//    }
//
//    private static class ViewHolderProfile extends RecyclerView.ViewHolder {
//
//        public TextView userName;
//        public TextView postCount;
//        public TextView aboutMeTextView;
//        public EditText aboutMeEditView;
//
//        public ViewHolderProfile(View itemView) {
//            super(itemView);
//
//            userName = (TextView) itemView.findViewById(R.id.fullUserName);
//            postCount = (TextView) itemView.findViewById(R.id.popCountTextView);
//            aboutMeTextView = (TextView) itemView.findViewById(R.id.aboutme_textbox_Display);
//            aboutMeEditView = (EditText) itemView.findViewById(R.id.aboutme_textbox_Edit);
//        }
//    }
//
//    public static class ViewHolderPost extends RecyclerView.ViewHolder{
//
//        public ImageView postImage;
//        public ImageView profileImage;
//        public CircleImageView profileImageActualImage;
//        public TextView authorName;
//        public TextView locationName;
//        public TextView textContent;
//        public TextView dateString;
//        public TextView objectIdString;
//        public Button numberOfLikes;
//        public Button comments;
//        public Boolean liked;
//        public List<String> likers;
//        public int numberLiked;
//        public List<String> commenters;
//
//        public ViewHolderPost(View v) {
//            super(v);
//
//            profileImageActualImage = (CircleImageView) v.findViewById(R.id.profile_picture_circle_imageView);
//            postImage = (ImageView) v.findViewById(R.id.shared_image_imageView);
//            profileImage = (ImageView) v.findViewById(R.id.profile_picture_imageView);
//            authorName = (TextView) v.findViewById(R.id.username_textView);
//            locationName = (TextView) v.findViewById(R.id.post_location_textView);
//            textContent = (TextView) v.findViewById(R.id.comment_textView);
//            dateString = (TextView) v.findViewById(R.id.post_time_textView);
//            objectIdString = (TextView) v.findViewById(R.id.storeObjectId);
//            numberOfLikes = (Button) v.findViewById(R.id.likeButton);
//            comments = (Button) v.findViewById(R.id.commentButton);
//            liked = false;
//            likers = null;
//            numberLiked = 0;
//            commenters = null;
//        }
//
//
//    }
//}
