//package com.bubblesocialapp.bubble.obsoleteCode;
//
///**
// * Copyright 2014-present Amberfog
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *    http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
// */
//
//
//import android.net.Uri;
//import android.os.Bundle;
//import android.support.v4.app.FragmentTransaction;
//
//import com.bubblesocialapp.bubble.R;
//import com.bubblesocialapp.bubble.activities.BaseActivity;
//import com.bubblesocialapp.bubble.adapters.SearchAdapter;
//import com.bubblesocialapp.bubble.fragments.ProfileFragment;
//import com.bubblesocialapp.bubble.fragments.SearchFragment;
//import com.parse.ParseObject;
//import com.parse.ParseUser;
//
//
//public class SearchActivity extends BaseActivity implements SearchAdapter.onSearchResultClickListener {
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        if (savedInstanceState == null) {
//            getSupportFragmentManager().beginTransaction()
//                    .add(R.id.screen_default_container, new SearchFragment())
//                    .commit();
//        }
//    }
//
//    @Override
//    protected int setLayoutResourceIdentifier() {
//        return R.layout.activity_buddy_search;
//    }
//
//    @Override
//    protected int getTitleToolBar() {
//        return R.string.app_name;
//    }
//
//    @Override
//    public void onSearchResultClick(ParseUser parseUser) {
//        ProfileFragment profileFragment = ProfileFragment.newInstance(parseUser.getObjectId(),parseUser.getUsername());
//        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        transaction.replace(R.id.screen_default_container, profileFragment);
//        transaction.addToBackStack(null);
//        transaction.commit();
//    }
//}
//
//
