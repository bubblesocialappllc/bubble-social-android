//package com.bubblesocialapp.bubble.obsoleteCode;
//
//
//import android.app.Activity;
//import android.content.Context;
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.support.v7.widget.Toolbar;
//import android.view.LayoutInflater;
//import android.view.MotionEvent;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import com.bubblesocialapp.bubble.R;
//import com.bubblesocialapp.bubble.adapters.HomeScreenPagerAdapter;
//import com.bubblesocialapp.bubble.common.Util;
//import com.bubblesocialapp.bubble.customLayouts.customViewPager.MaterialViewPager;
//
//
//// Used to store the three main fragments of the app {Notification, Feed, Buddies)
//
//public class BaseFragment extends Fragment  implements View.OnTouchListener {
//
//    private static final String FEED_DATA = "feed_data";
//
//    private static final String TAG = BaseFragment.class.getSimpleName();
//    private View mViewHome;
////    private PagerSlidingTabStrip mPagerSlidingTabStrip;
////    private ViewPager mViewPager;
//    private MaterialViewPager mViewPager;
//    protected Context mContext;
//    protected Activity mActivity;
//    private TextView mNotificationTextView;
//    private Toolbar mToolbar;
//    private ViewGroup mRootLayout;
//    private int _xDelta;
//    private int _yDelta;
//    private String feedDataFromSplashScreen;
//
//    public static BaseFragment newInstance(String feedData) {
//        BaseFragment fragment = new BaseFragment();
//        Bundle bundle = new Bundle();
//        bundle.putString(FEED_DATA, feedData);
//        return fragment;
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//
//        if (getArguments() != null) {
//            feedDataFromSplashScreen = getArguments().getString(FEED_DATA);
//        }
//
//        mViewHome = inflater.inflate(R.layout.fragment_base, container, false);
//
//        loadViewComponents();
//
//        return mViewHome;
//    }
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        mActivity = null;
//        mContext = null;
//    }
//
//    public void loadViewComponents() {
//        // Top Sliding Tab
////        mPagerSlidingTabStrip = (PagerSlidingTabStrip) mViewHome.findViewById(R.id.tab_indicator);
////        mPagerSlidingTabStrip.setDividerColor(getResources().getColor(R.color.bubble_pink));
////        mPagerSlidingTabStrip.setIndicatorColor(getResources().getColor(R.color.bubble_pink));
//
//        // Viewpager
//        mViewPager = (MaterialViewPager) mViewHome.findViewById(R.id.materialViewPager);
//
//
//        mViewPager.getViewPager().setAdapter(new HomeScreenPagerAdapter(getFragmentManager(), feedDataFromSplashScreen));
//        mViewPager.getPagerTitleStrip().setViewPager(mViewPager.getViewPager());
//        mViewPager.setMapFragment(getFragmentManager());
//
//
////        mPagerSlidingTabStrip.setViewPager(mViewPager);
//
//        mRootLayout = (ViewGroup) mViewHome.findViewById(R.id.fragment_home_content_main);
//        mNotificationTextView = (TextView) mViewHome.findViewById(R.id.notification_textview);
//
//        int notificationBubbleSizePixels = (int) Util.dpToPx(getActivity(),50);
//        RelativeLayout.LayoutParams layoutParams =
//                new RelativeLayout.LayoutParams(notificationBubbleSizePixels, notificationBubbleSizePixels);
//
//        // Notification Bubble
//        mNotificationTextView.setVisibility(View.GONE);
//
//        mNotificationTextView.setLayoutParams(layoutParams);
//        mNotificationTextView.setOnTouchListener(this);
//
//    }
//
//    @Override
//    public boolean onTouch(View view, MotionEvent motionEvent) {
//        final int X = (int) motionEvent.getRawX();
//        final int Y = (int) motionEvent.getRawY();
//        switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
//            case MotionEvent.ACTION_DOWN:
//                RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
//                _xDelta = X - lParams.leftMargin;
//                _yDelta = Y - lParams.topMargin;
//                break;
//            case MotionEvent.ACTION_UP:
//                break;
//            case MotionEvent.ACTION_POINTER_DOWN:
//                break;
//            case MotionEvent.ACTION_POINTER_UP:
//                break;
//            case MotionEvent.ACTION_MOVE:
//                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view
//                        .getLayoutParams();
//                layoutParams.leftMargin = X - _xDelta;
//                layoutParams.topMargin = Y - _yDelta;
//                layoutParams.rightMargin = -250;
//                layoutParams.bottomMargin = -250;
//                view.setLayoutParams(layoutParams);
//                break;
//        }
//        mRootLayout.invalidate();
//        return true;
//    }
//
//}