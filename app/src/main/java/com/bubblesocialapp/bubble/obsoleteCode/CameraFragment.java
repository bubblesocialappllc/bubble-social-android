//package com.bubblesocialapp.bubble.fragments;
//
//import android.app.Activity;
//import android.content.Context;
//import android.content.Intent;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.graphics.Matrix;
//import android.hardware.Camera;
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.SurfaceHolder;
//import android.view.SurfaceView;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.ImageButton;
//import android.widget.Toast;
//
//import com.bubblesocialapp.bubble.R;
//import com.parse.ParseFile;
//
//import java.io.ByteArrayOutputStream;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.util.List;
//import java.util.UUID;
//
//
////TODO: Incorporate camera2 package for newer devices
//
//public class CameraFragment extends Fragment {
//
//    public static final String TAG = "CameraFragment";
//
//    public static final String EXTRA_PHOTO_FILENAME = "post.filename";
//
//    private Camera camera;
//    private SurfaceView surfaceView;
//    private ParseFile photoFile;
//    private Button photoButton;
//
//    public interface PictureListener {
//        /** Called when posting */
//        public void onPictureTaken();
//    }
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup parent,
//                             Bundle savedInstanceState) {
//        View v = inflater.inflate(R.layout.fragment_camera, parent, false);
//
////        photoButton = (Button) v.findViewById(R.id.camera_photo_button);
//
//        if (camera == null) {
//            try {
//                camera = Camera.open();
//                photoButton.setEnabled(true);
//            } catch (Exception e) {
//                Log.e(TAG, "No camera with exception: " + e.getMessage());
//                photoButton.setEnabled(false);
//                Toast.makeText(getActivity(), "No camera detected",
//                        Toast.LENGTH_LONG).show();
//            }
//        }
//
//        photoButton.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                if (camera == null)
//                    return;
//                camera.takePicture(new Camera.ShutterCallback() {
//
//                    @Override
//                    public void onShutter() {
//                        // nothing to do
//                    }
//
//                }, null, new Camera.PictureCallback() {
//
//                    @Override
//                    public void onPictureTaken(byte[] data, Camera camera) {
////                        saveScaledPhoto(data, camera);
//                    }
//
//                });
//
//            }
//        });
//
//        surfaceView = (SurfaceView) v.findViewById(R.id.camera_surface_view);
//        SurfaceHolder holder = surfaceView.getHolder();
//        // Deprecated but needed for Android before 3.0
//        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
//        holder.addCallback(new SurfaceHolder.Callback() {
//
//            public void surfaceCreated(SurfaceHolder holder) {
//                try {
//                    if (camera != null) {
//                        camera.setDisplayOrientation(90);
//                        camera.setPreviewDisplay(holder);
//                    }
//                } catch (IOException e) {
//                    Log.e(TAG, "Error setting up preview", e);
//                }
//            }
//
//            public void surfaceChanged(SurfaceHolder holder, int format,
//                                       int width, int height) {
//                if (camera == null) return;
//
//                // the surface has changed size; update the camera preview size
//                Camera.Parameters parameters = camera.getParameters();
//                Camera.Size s = getBestSupportedSize(parameters.getSupportedPreviewSizes(), width, height);
//                parameters.setPreviewSize(s.width, s.height);
//                s = getBestSupportedSize(parameters.getSupportedPictureSizes(), width, height);
//                parameters.setPictureSize(s.width, s.height);
//                camera.setParameters(parameters);
//                try {
//                    camera.startPreview();
//                } catch (Exception e) {
//                    Log.e(TAG, "Could not start preview", e);
//                    camera.release();
//                    camera = null;
//                }
//            }
//
//            public void surfaceDestroyed(SurfaceHolder holder) {
//                if (camera != null) {
//                    camera.stopPreview();
//                }
//            }
//
//        });
//
//        return v;
//    }
//
//
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        if (camera == null) {
//            try {
//                camera = Camera.open();
//                photoButton.setEnabled(true);
//            } catch (Exception e) {
//                Log.i(TAG, "No camera: " + e.getMessage());
//                photoButton.setEnabled(false);
//                Toast.makeText(getActivity(), "No camera detected",
//                        Toast.LENGTH_LONG).show();
//            }
//        }
//    }
//
//
//    @Override
//    public void onPause() {
//        super.onPause();
//
//        if (camera != null) {
//            camera.release();
//            camera = null;
//        }
//    }
//
//    /** a simple algorithm to get the largest size available. For a more
//     * robust version, see CameraPreview.java in the ApiDemos
//     * sample app from Android. */
//    private Camera.Size getBestSupportedSize(List<Camera.Size> sizes, int width, int height) {
//        Camera.Size bestSize = sizes.get(0);
//        int largestArea = bestSize.width * bestSize.height;
//        for (Camera.Size s : sizes) {
//            int area = s.width * s.height;
//            if (area > largestArea) {
//                bestSize = s;
//                largestArea = area;
//            }
//        }
//        return bestSize;
//    }
//
//}
//
//// Obsolete Code
//
////    /*
////     * ParseQueryAdapter loads ParseFiles into a ParseImageView at whatever size
////     * they are saved. Since we never need a full-size image in our app, we'll
////     * save a scaled one right away.
////     */
////    private void saveScaledPhoto(byte[] data, Camera arg1) {
////
////        BitmapFactory.Options opt;
////
////        opt = new BitmapFactory.Options();
////        opt.inTempStorage = new byte[16 * 1024];
////        Camera.Parameters parameters = arg1.getParameters();
////        Camera.Size size = parameters.getPictureSize();
////
////        int height11 = size.height;
////        int width11 = size.width;
////        float mb = (width11 * height11) / 1024000;
////
////        if (mb > 4f)
////            opt.inSampleSize = 4;
////        else if (mb > 3f)
////            opt.inSampleSize = 2;
////
////        // Resize photo from camera byte array
////        Bitmap userImage = BitmapFactory.decodeByteArray(data, 0, data.length, opt);
////        Bitmap userImageScaled = Bitmap.createScaledBitmap(userImage, 200, 200
////                * userImage.getHeight() / userImage.getWidth(), false);
////
////        // Override Android default landscape orientation and save portrait
////        Matrix matrix = new Matrix();
////        matrix.postRotate(90);
////        Bitmap rotatedScaledImage = Bitmap.createBitmap(userImageScaled, 0,
////                0, userImageScaled.getWidth(), userImageScaled.getHeight(),
////                matrix, true);
////
////        ByteArrayOutputStream bos = new ByteArrayOutputStream();
////        rotatedScaledImage.compress(Bitmap.CompressFormat.JPEG, 100, bos);
////
////        byte[] scaledData = bos.toByteArray();
////
////
////        // create a filename
////        String filename = UUID.randomUUID().toString() + ".jpg";
////        // save the jpeg data to disk
////        FileOutputStream os = null;
////        boolean success = true;
////        try {
////            os = getActivity().openFileOutput(filename, Context.MODE_PRIVATE);
////            os.write(scaledData);
////        } catch (Exception e) {
////            Log.e(TAG, "Error writing to file " + filename, e);
////            success = false;
////        } finally {
////            try {
////                if (os != null)
////                    os.close();
////            } catch (Exception e) {
////                Log.e(TAG, "Error closing file " + filename, e);
////                success = false;
////            }
////        }
////
////        if (success) {
////            // set the photo filename on the result intent
////            if (success) {
////                Intent i = new Intent();
////                i.putExtra(EXTRA_PHOTO_FILENAME, filename);
////                getActivity().setResult(Activity.RESULT_OK, i);
////            } else {
////                getActivity().setResult(Activity.RESULT_CANCELED);
////            }
////        }
////        arg1.stopPreview();
////        getActivity().finish();
////
//////        // Save the scaled image to Parse
//////        photoFile = new ParseFile("meal_photo.jpg", scaledData);
//////        photoFile.saveInBackground(new SaveCallback() {
//////
//////            public void done(ParseException e) {
//////                if (e != null) {
//////                    Toast.makeText(getActivity(),
//////                            "Error saving: " + e.getMessage(),
//////                            Toast.LENGTH_LONG).show();
//////                } else {
//////                    // Do stuff
//////                }
//////            }
//////        });
////    }
