package com.bubblesocialapp.bubble.fragments;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.common.Constants;
import com.bubblesocialapp.bubble.common.ParseCloudCode;
import com.bubblesocialapp.bubble.common.Util;

public class PopularityPointsFragment extends Fragment {

    private String mPopularityPoints;
    private View mView;
    private RelativeLayout mCircleLayout;
    private RelativeLayout mRulesLayout;
    private TextView mRankExplanation;
    private TextView mPoints;

//    public static PopularityPointsFragment newInstance(String popularityPoints) {
//        PopularityPointsFragment fragment = new PopularityPointsFragment();
//        Bundle args = new Bundle();
//        args.putString(Constants.POPULARITY_POINTS, popularityPoints);
//        fragment.setArguments(args);
//        return fragment;
//    }

    public PopularityPointsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            mPopularityPoints = getArguments().getString(Constants.POPULARITY_POINTS);
//        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        int height = Util.getDisplaySize(getActivity()).y;
        String formattedString = getActivity().getString(R.string.Rank_expression, 1);

        mView = inflater.inflate(R.layout.fragment_popularity_points, container, false);
        mCircleLayout = (RelativeLayout) mView.findViewById(R.id.popularity_points_view);
        mPoints = (TextView) mView.findViewById(R.id.points);
        mRulesLayout = (RelativeLayout) mView.findViewById(R.id.popularity_rules_view);
        mRankExplanation = (TextView) mView.findViewById(R.id.rank_explanation_textview);

        //Set the populairty related text non-visible until the data has arrived
        mPoints.setAlpha(0);
        mRankExplanation.setAlpha(0);

        //We pass the Activity to the function so that on it's asynchronous callback, it can set the Activity's text view
        ParseCloudCode.rank(this.getActivity());

        //Setting from the Cloud call
        mRankExplanation.setText(formattedString);

        RelativeLayout.LayoutParams circleParams = (RelativeLayout.LayoutParams) mCircleLayout.getLayoutParams();
        RelativeLayout.LayoutParams rulesParams = (RelativeLayout.LayoutParams) mRulesLayout.getLayoutParams();

        circleParams.height = height;
        rulesParams.height = height;
        rulesParams.setMargins(0,height,0,0);

        mCircleLayout.setLayoutParams(circleParams);
        mRulesLayout.setLayoutParams(rulesParams);

        return mView;
    }

}
