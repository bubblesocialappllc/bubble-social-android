package com.bubblesocialapp.bubble.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.adapters.MessageAdapter;
import com.bubblesocialapp.bubble.common.ParseCloudCode;
import com.bubblesocialapp.bubble.parseObjects.Message;
import com.bubblesocialapp.bubble.parseObjects.MessageHead;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;


/**
 * A fragment representing a list of Items.
 * <p/>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p/>

 * interface.
 */
public class MessageFragment extends Fragment {
    //TODO: Create pagination static function
    private static final String MessageHeadId = "MessageHeadObjectId";
    private static final String UserId = "SecondUserObjectId";
    private static final String UserIdName = "SecondUserObjectIdName";
    private String secondUserUserName;
    private String secondUserObjectId;
    private Button sendButton;
    private EditText messageText;
    private String messageHeadObjectId;
    private ArrayList<Message> mMessages;
    private int OUTGOING_MESSAGE = 0;
    private int INCOMING_MESSAGE = 1;

//    private OnUserProfileClickListener mListener;

    private RecyclerView mRecyclerView;

    /**
     * The Adapter which will be used to populate the ListView/GridView with
     * Views.
     */
    private MessageAdapter mAdapter;

    public static MessageFragment newInstance(String messageObjectId, String userObjectId, String username) {
        MessageFragment fragment = new MessageFragment();
        Bundle args = new Bundle();
        args.putString(MessageHeadId, messageObjectId);
        args.putString(UserId, userObjectId);
        args.putString(UserIdName, username);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public MessageFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            messageHeadObjectId = getArguments().getString(MessageHeadId);
            secondUserObjectId = getArguments().getString(UserId);
            secondUserUserName = getArguments().getString(UserIdName);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_message, container, false);

        final ParseObject firstUser = ParseUser.getCurrentUser();
        final ParseObject secondUser = ParseObject.createWithoutData("_User", secondUserObjectId);

        mMessages = new ArrayList<Message>();

        // Locate Views
        mRecyclerView = (RecyclerView) view.findViewById(R.id.listMessages);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(layoutManager);

        sendButton = (Button) view.findViewById(R.id.sendMessageButton);
        messageText = (EditText) view.findViewById(R.id.messageBodyField);

        // Pull Previous Messages
        if (messageHeadObjectId != null) {

            previousMessages(messageHeadObjectId);

            // New messagehead has been created

        } else if (secondUserObjectId != null && secondUserUserName != null) {

            ParseQuery<MessageHead> query = new ParseQuery<MessageHead>("MessageHead");
            query.whereEqualTo("secondUser", secondUser);
            query.whereEqualTo("firstUser", firstUser);
            query.findInBackground(new FindCallback<MessageHead>() {
                @Override
                public void done(List<MessageHead> messageHeads, ParseException e) {
                    // Check if MessageHead already exist otherwise create one
                    if (e == null && messageHeads.size() != 0) {
                        attachMessageHeadToButton(messageHeads.get(0));
                        previousMessages(messageHeads.get(0).getObjectId());
                    } else if (e == null) {
                        newMessageHead(firstUser, secondUser);
                    } else {
                        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }
        return view;
    }

    public void previousMessages(String messageHeadId){

        MessageHead messageHead = (MessageHead) ParseObject.createWithoutData("MessageHead", messageHeadId);
        attachMessageHeadToButton(messageHead);
        ParseQuery<Message> query = new ParseQuery<Message>("Message");
        query.whereEqualTo("parent", messageHead);
        query.orderByAscending("createdAt");
        query.setLimit(500); // Only get the last ten Messages TODO: Paginate
        query.findInBackground(new FindCallback<Message>() {
            @Override
            public void done(List<Message> messages, ParseException e) {
                mMessages.addAll(messages);
                mAdapter = new MessageAdapter(getActivity(), mMessages);
                mRecyclerView.setAdapter(mAdapter);
            }
        });
    }

    public void newMessageHead(ParseObject firstUser, ParseObject secondUser) {
        MessageHead messageHead = new MessageHead();
        messageHead.setFirstUserUsername(ParseUser.getCurrentUser().getUsername());
        messageHead.setUser(firstUser);
        messageHead.setSecondUserUsername(secondUserUserName);
        messageHead.setSecondUser(secondUser);
        messageHead.saveInBackground();
        attachMessageHeadToButton(messageHead);
    }

    public void attachMessageHeadToButton(final MessageHead messageHead) {
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Message message = new Message();
                message.setMessageText(messageText.getText().toString());
                message.setSender(ParseUser.getCurrentUser());
                message.setParent(messageHead);
                message.saveInBackground();
                mMessages.add(message);
                mAdapter.notifyDataSetChanged();
                sendPushNotification(message);
                messageText.clearComposingText();
                mRecyclerView.scrollToPosition(mMessages.size()-1);
            }
        });

    }

    public void sendPushNotification(Message message){
        String alert = secondUserUserName + ": " + message.getMessageText();
//        ParseCloudCode.pushNotification(getActivity(), secondUserUserName, alert,
//                ParseUser.getCurrentUser().getUsername());

    }

}
