package com.bubblesocialapp.bubble.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.adapters.AroundMeAdapter;
import com.bubblesocialapp.bubble.common.Constants;
import com.bubblesocialapp.bubble.customLayouts.customRecyclerView.CustomRecyclerView;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sadaf on 5/16/15.
 */
public class SeeCurrentBuddiesFragment extends Fragment {

    private CustomRecyclerView mRecyclerView;
    private ArrayList<String> mBuddiesList;
    private String userObjectId = null;

    public static SeeCurrentBuddiesFragment newInstance(String userObjectId, ArrayList<String> buddies) {
        SeeCurrentBuddiesFragment currentBuddiesFragment = new SeeCurrentBuddiesFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.USER_OBJECT_ID, userObjectId);
        bundle.putStringArrayList(Constants.GET_BUDDIES_ARRAY, buddies);
        currentBuddiesFragment.setArguments(bundle);
        return currentBuddiesFragment;
    }

    public SeeCurrentBuddiesFragment() {
        super();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            userObjectId = getArguments().getString(Constants.USER_OBJECT_ID);
            mBuddiesList = getArguments().getStringArrayList(Constants.GET_BUDDIES_ARRAY);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_current_buddies, container, false);
        mRecyclerView = (CustomRecyclerView) rootView.findViewById(R.id.fragment_recycler_view_current_buddies);
        loadInfoView();
        getData();
        return rootView;
    }

    private void loadInfoView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(layoutManager);
    }

    private void getData() {
        //TODO: Remove Query by using query from ProfileFragment
        if (userObjectId.equals(ParseUser.getCurrentUser().getObjectId()) || userObjectId == null) {
            List<String> buddies = ParseUser.getCurrentUser().getList("buddies");
            mBuddiesList = (ArrayList<String>) buddies;
            startQuery(mBuddiesList);
        } else {
            startQuery(mBuddiesList);
        }
    }

    public void startQuery(ArrayList<String> buddiesList) {
        if (buddiesList != null) {
            ParseQuery<ParseUser> query = ParseQuery.getQuery("_User");
            query.whereContainedIn("objectId", buddiesList);
            query.addAscendingOrder("username");
            query.findInBackground(new FindCallback<ParseUser>() {
                @Override
                public void done(List<ParseUser> list, ParseException e) {
                    if (e == null) {
                        setBuddiesAdapter(list);
                    }
                }
            });
        }
    }

    private void setBuddiesAdapter(List<ParseUser> list) {
        //Reusing aroundMeAdapter since views are the same
        AroundMeAdapter meAdapter = new AroundMeAdapter(getActivity(),list);
        mRecyclerView.setAdapter(meAdapter);
    }
}














