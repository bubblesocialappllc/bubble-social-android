package com.bubblesocialapp.bubble.fragments;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.Toast;

import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.adapters.ColorAdapter;
import com.bubblesocialapp.bubble.bean.ColorBean;
import com.bubblesocialapp.bubble.common.Util;
import com.bubblesocialapp.bubble.parseObjects.User;
import com.google.gson.Gson;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParsePush;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

import java.io.IOException;
import java.util.ArrayList;

public class SignUpChooseColorFragment extends Fragment {

    private static final String ARG_PARAM1 = "userJSonObject";

    private String userJSonObject;
    private ParseUser mCurrentUser;
    private GridView mGridView;
    private User mUser;

    private OnSignUpColorPickerListener mListener;

    public static SignUpChooseColorFragment newInstance(String jSonObject) {
        SignUpChooseColorFragment fragment = new SignUpChooseColorFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, jSonObject);
        fragment.setArguments(args);
        return fragment;
    }

    public SignUpChooseColorFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null){
            userJSonObject = getArguments().getString(ARG_PARAM1);
            Gson gSon = new Gson();
            mUser = gSon.fromJson(userJSonObject, User.class);
            setCurrentUserInfo(mUser);
            }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setHasOptionsMenu(true);
        View rootView = inflater.inflate(R.layout.fragment_signup_color, container, false);
        mGridView = (GridView)  rootView.findViewById(R.id.color_grid);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setColorsToGridView();

    }

    @Override
    public void onActivityCreated(Bundle saveInstanceState){
        super.onCreate(saveInstanceState);

    }

    public void setColorsToGridView(){
        ArrayList<ColorBean> allColors = new ArrayList<>();
        int[] colorArray = getActivity().getResources().getIntArray(R.array.fragment_color_picker);
        for (int i = 0; i < colorArray.length; i++){
            allColors.add(new ColorBean(colorArray[i]));
        }
        final ColorAdapter adapter = new ColorAdapter(getActivity(),0,allColors);
        mGridView.setAdapter(adapter);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_color_fragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case (R.id.complete_sign_up):
                signUp();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnSignUpColorPickerListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnSignUpColorPickerListener {
        // TODO: Update argument type and name
        public void onColorPicked(String jsonObject);
    }

    public void setCurrentUserInfo(User user){
        mCurrentUser = new ParseUser();
        mCurrentUser.setPassword(user.getPassword());
        mCurrentUser.setUsername(user.getUserName());
        mCurrentUser.setEmail(user.getEmail());
        mCurrentUser.put("aboutMe", user.getAboutMe());
        mCurrentUser.put("birthday", user.getBirthday());
        mCurrentUser.put("beta", user.getBeta());
        mCurrentUser.put("staff", user.getStaff());
        mCurrentUser.put("gender", user.getGender());
        mCurrentUser.put("color", user.getColor());
        mCurrentUser.put("realName", user.getRealName());
        Uri profileImageUri = Uri.parse(user.getProfilePic());
        savePhoto(profileImageUri);
    }

    private void savePhoto(Uri image){
        try {
            byte[] photoData = Util.UriToByte(getActivity(), image);
            photoData = Util.scalePhoto(photoData);
            final ParseFile savePhoto = new ParseFile("profilePic",photoData);
            savePhoto.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e != null) {
                        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                    } else {
                        mCurrentUser.put("profilePicture", savePhoto);
                    }
                }
            });

        } catch (IOException e) {
            Log.d(e.getMessage(), "Error converting to uri to byte");
        }
    }

    public void signUp() {
        mCurrentUser.signUpInBackground(new SignUpCallback() {
            public void done(ParseException e) {
                if (e == null) {
                    subscribeToParse();
                } else {
                    // Sign up didn't succeed. Look at the ParseException
                    // to figure out what went wrong
                    Toast.makeText(getActivity().getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void subscribeToParse(){
        String username = ParseUser.getCurrentUser().getUsername();

        ParsePush.subscribeInBackground(username, new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    mListener.onColorPicked(userJSonObject);
                    Log.d("com.parse.push", "successfully subscribed to the broadcast channel.");

                } else {
                    Log.e("com.parse.push", "failed to subscribe for push", e);
                }
            }
        });
    }
}
