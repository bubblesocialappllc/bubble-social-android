package com.bubblesocialapp.bubble.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.FontAwesomeText;
import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.dialogs.DatePickerCustomDialog;
import com.bubblesocialapp.bubble.parseObjects.User;
import com.google.gson.Gson;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Sadaf Mackertich on 11/6/2014.
 */

public class SignUpFragment extends Fragment {

    private static final String SAVED_USERNAME = "saved_username";
    private static final String SAVED_PASSWORD = "saved_password";
    private static final String SAVED_FULLNAME = "saved_password";
    private static final String SAVED_EMAIL = "saved_password";
    private static final String SAVED_BIRTHDAY = "saved_password";
    private static final String SAVED_GENDER = "saved_password";

    private EditText mUsername;
    private EditText mPassword;
    private EditText mFullName;
    private EditText mEmail;
    private TextView mBirthday;
    private Button mMaleButton;
    private Button mFemaleButton;
    private Date mBirthdayDate;
    private DateFormat mDateFormat;
    private String mBirthdayString;
    private String mGenderString;
    private onSignUpStartListener mListener;
    private FontAwesomeText mAwesomeTextView;
    private String mCurrentUsernameTyped;

    public interface onSignUpStartListener{
         void onSignUpStart();
         void onSignUpComplete(String userJsonObject);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(SAVED_USERNAME,mUsername.getText().toString());
        outState.putString(SAVED_BIRTHDAY,mBirthdayString);
        outState.putString(SAVED_EMAIL, mEmail.getText().toString());
        outState.putString(SAVED_FULLNAME, mFullName.getText().toString());
        outState.putString(SAVED_GENDER,mGenderString);
        outState.putString(SAVED_PASSWORD,mPassword.getText().toString());

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

        if (savedInstanceState != null) {
            mUsername.setText(savedInstanceState.getString(SAVED_USERNAME));
            mBirthday.setText(savedInstanceState.getString(SAVED_BIRTHDAY));
            mEmail.setText(savedInstanceState.getString(SAVED_EMAIL));
            mFullName.setText(savedInstanceState.getString(SAVED_FULLNAME));
            mGenderString = savedInstanceState.getString(SAVED_GENDER);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            mListener.onSignUpStart();
            setHasOptionsMenu(true);

            View rootView = inflater.inflate(R.layout.fragment_signup, container, false);

            mDateFormat = new SimpleDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss.SSS'Z'", Locale.ENGLISH);

            mUsername = (EditText) rootView.findViewById(R.id.username);
            mPassword = (EditText) rootView.findViewById(R.id.password);
            mFullName = (EditText) rootView.findViewById(R.id.full_name);
            mEmail = (EditText) rootView.findViewById(R.id.email);
            mBirthday = (TextView) rootView.findViewById(R.id.birthday);

            mAwesomeTextView = (FontAwesomeText) rootView.findViewById(R.id.check_user_info_view);
            mAwesomeTextView.setVisibility(View.INVISIBLE);

            mMaleButton = (Button) rootView.findViewById(R.id.male_button);
            mFemaleButton = (Button) rootView.findViewById(R.id.female_button);
        if (savedInstanceState != null) {
            if (savedInstanceState.getString(SAVED_GENDER).equals("male")) {
                mMaleButton.setBackgroundResource(R.drawable.buttonshape_pink);
            } else if (savedInstanceState.getString(SAVED_GENDER).equals("female")) {
                mFemaleButton.setBackgroundResource(R.drawable.buttonshape_pink);
            }
        } else {
            mMaleButton.setBackgroundResource(R.drawable.buttonshape_grey);
            mFemaleButton.setBackgroundResource(R.drawable.buttonshape_grey);
        }

            setButtonClickListeners();

            return rootView;
        }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        checkUsername();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_sign_up, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case(R.id.sign_up):
                checkCurrentInfo();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void checkUsername(){
        mUsername.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (i2 == 0) {
                    mAwesomeTextView.setVisibility(View.INVISIBLE);
                }  else if (i2 > 0) {
                    mAwesomeTextView.setVisibility(View.VISIBLE);
                    int color = getResources().getColor(android.R.color.darker_gray);
                    mAwesomeTextView.setTextColor(color);
                    mAwesomeTextView.setIcon("fa-spinner");
                    //TODO: Animation issue. Search too fast
//                    mAwesomeTextView.startRotate(getActivity(), true, FontAwesomeText.AnimationSpeed.MEDIUM);
                    mCurrentUsernameTyped = charSequence.toString();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (mCurrentUsernameTyped != null) {
                    queryParse();
                }
            }
        });
    }

    private void queryParse(){
        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.whereEqualTo("username",mCurrentUsernameTyped.toLowerCase());
        query.getFirstInBackground(new GetCallback<ParseUser>() {
            @Override
            public void done(ParseUser parseUser, ParseException e) {
                if (e == null) {
//                    mAwesomeTextView.stopAnimation();
                    if (parseUser != null) {
                        mAwesomeTextView.setIcon("fa-times-circle");
                        int color = getActivity().getResources().getColor(android.R.color.holo_red_dark);
                        mAwesomeTextView.setTextColor(color);
                    }
                } else {
//                    Toast.makeText(getActivity(),e.getMessage(),Toast.LENGTH_LONG).show();
//                    mAwesomeTextView.stopAnimation();
                    mAwesomeTextView.setIcon("fa-check-circle");
                    int color = getActivity().getResources().getColor(android.R.color.holo_green_dark);
                    mAwesomeTextView.setTextColor(color);
                }
            }
        });
    }

    private void setDateOnEditTextView(){
        DatePickerCustomDialog datePickerFragment = new DatePickerCustomDialog();
        datePickerFragment.show(getChildFragmentManager(), "datePicker");
    }

    public void checkCurrentInfo(){
        User user = new User();
        user.setUserName(mUsername.getText().toString());
        user.setEmail(mEmail.getText().toString());

        //TODO: Check to make sure values are not null

        user.setBirthday(mBirthdayDate);
        user.setRealName(mFullName.getText().toString());
        user.setColor("bubblePink");
        user.setBeta(true);
        user.setStaff(false);
        user.setGender(mGenderString);
        user.setPassword(mPassword.getText().toString());

        Gson gSon = new Gson();
        String userJson = gSon.toJson(user);

        mListener.onSignUpComplete(userJson);

    }

    public void setPickedBirthdayDate(int year, int month, int day) {
        mBirthdayString = year+"-"+month+"-"+day+"T00:00:00.320Z";
        String mBirthdayStringToShow = month+"-"+day+"-"+year;
        try {
            mBirthdayDate = mDateFormat.parse(mBirthdayString);
            mBirthday.setText(mBirthdayStringToShow);
        } catch (java.text.ParseException e){
            // do something
        }
    }

    private void setButtonClickListeners(){

        mFemaleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFemaleButton.setBackgroundResource(R.drawable.buttonshape_pink);
                mMaleButton.setBackgroundResource(R.drawable.buttonshape_grey);
                mGenderString = "female";
            }
        });

        mMaleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFemaleButton.setBackgroundResource(R.drawable.buttonshape_grey);
                mMaleButton.setBackgroundResource(R.drawable.buttonshape_pink);
                mGenderString = "male";
            }
        });

        mBirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDateOnEditTextView();
            }
        });

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (onSignUpStartListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement onSearchResultClickListener");
        }
    }


}

