package com.bubblesocialapp.bubble.fragments;


import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.adapters.FeedAdapter;
import com.bubblesocialapp.bubble.adapters.ProfileAdapter;
import com.bubblesocialapp.bubble.common.ParseCloudCode;
import com.bubblesocialapp.bubble.common.Util;
import com.bubblesocialapp.bubble.customLayouts.customRecyclerView.CustomRecyclerView;
import com.bubblesocialapp.bubble.customLayouts.customViewPager.MaterialViewPagerHelper;
import com.bubblesocialapp.bubble.customLayouts.customViewPager.RecyclerViewMaterialAdapter;
import com.bubblesocialapp.bubble.dialogs.CameraDialog;
import com.bubblesocialapp.bubble.parseObjects.Post;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sadaf on 2/7/2015.
 */
//implements ReturnBitmapResults
public class ProfileFragment extends Fragment {
    // TODO: Code can be minimized
    private static final String USER_OBJECTID = "User";

    private static final String USERNAME = "Username";

    private ImageView mProfilePic;

    private TextView aboutMeTextDisplay;
    private CameraDialog mDialog;
    private ImageView staticMapPicture;
    private TextView nameTextView;
    private TextView popCountTextView;
    private String profileId;
    private String profileUsername;
    private String currentUserId;
    private FloatingActionButton mBuddies;
    private FloatingActionButton mPhotos;
    private FloatingActionButton mMessagesPost;
    private FloatingActionButton mAddBuddy;
    private OnProfileClickListener mProfileClickListener;
    private RecyclerView.Adapter mProfileAdapter;
    private CustomRecyclerView mRecyclerView;
    private View mViewRecyclerCardsView;
    private FloatingActionMenu mFloatingActionMenu;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private FloatingActionButton mSettings;
    private Point size;
    private ParseUser userProfileData;
    private View mProfilePictureBackgroundColor;
    private boolean foundInPendingBuddyList;
    private boolean foundInBuddyList;
    private SharedPreferences mPreferences;
    private int mUserColor;

    public interface OnProfileClickListener {
        public void onBuddiesClick(String objectId, ArrayList<String> buddies);
        public void onPhotosClick(String objectId);
        public void onPreviousPostsClick(String objectId);
        public void onMessageUserClick(String objectId, String username);
        public void onSettingsClick();

    }

    /**
     * Use to pass ParseUser object. Pass user objectId.
     */

    public static ProfileFragment newInstance(String userId, String username) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle bundle = new Bundle();
        bundle.putString(USER_OBJECTID, userId);
        bundle.putString(USERNAME, username);
        fragment.setArguments(bundle);
        return fragment;
    }

    public ProfileFragment() {
        // Empty Constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null){
            profileId = getArguments().getString(USER_OBJECTID);
            profileUsername = getArguments().getString(USERNAME);
        }
        size = Util.getDisplaySize(getActivity());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Another profile is being viewed
        if (!profileId.equals(ParseUser.getCurrentUser().getObjectId())) {
            View rootView = inflater.inflate(R.layout.fragment_profile, container, false);

            currentUserId = ParseUser.getCurrentUser().getObjectId();

            mProfilePic = (ImageView) rootView.findViewById(R.id.profile_picture_Profile);
            aboutMeTextDisplay = (TextView) rootView.findViewById(R.id.aboutme_textbox_Display);
            nameTextView = (TextView) rootView.findViewById(R.id.fullUserName);
            popCountTextView = (TextView) rootView.findViewById(R.id.popCountTextView);
            mProfilePictureBackgroundColor = (View) rootView.findViewById(R.id.profile_picture_background);
            aboutMeTextDisplay.setVisibility(View.VISIBLE);

            loadViewComponentsForAnotherProfile(rootView);

            return rootView;

        } else {

            mViewRecyclerCardsView = inflater.inflate(R.layout.fragment_profile_recycler_view, container, false);
            mSwipeRefreshLayout = (SwipeRefreshLayout) mViewRecyclerCardsView.findViewById(R.id.swipe_container);
            mRecyclerView = (CustomRecyclerView)
                    mViewRecyclerCardsView.findViewById(R.id.fragment_recycler_view_profile);

            currentUserId = ParseUser.getCurrentUser().getObjectId();

            loadViewComponentsForCurrentUser(mViewRecyclerCardsView);

            setScrollListener();

            loadInfoView();

            List<String> buddiesList = ParseUser.getCurrentUser().getList("buddies");

            setupFloatingMenu(buddiesList);

            return mViewRecyclerCardsView;
        }
    }

    private void setScrollListener() {
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0){
                    mFloatingActionMenu.hideMenuButton(true);
                } else {
                    mFloatingActionMenu.showMenuButton(true);
                }
            }
        });
    }

    private void loadViewComponentsForAnotherProfile(View view) {
        mFloatingActionMenu = (FloatingActionMenu)
                view.findViewById(R.id.floating_action_menu);
        mBuddies = (FloatingActionButton) view.findViewById(R.id.buddies_Button);
        mMessagesPost = (FloatingActionButton) view.findViewById(R.id.userposts_messageuser_button);
        mPhotos = (FloatingActionButton) view.findViewById(R.id.photos_button);
        mAddBuddy = (FloatingActionButton) view.findViewById(R.id.add_buddies_Button);

    }

    private void loadViewComponentsForCurrentUser(View view) {
        mFloatingActionMenu = (FloatingActionMenu)
                view.findViewById(R.id.floating_action_menu);
        mBuddies = (FloatingActionButton) view.findViewById(R.id.buddies_Button);
        mMessagesPost = (FloatingActionButton) view.findViewById(R.id.userposts_messageuser_button);
        mPhotos = (FloatingActionButton) view.findViewById(R.id.photos_button);
        mSettings = (FloatingActionButton) view.findViewById(R.id.settings_button); //Color not changed for settings
        mSettings.setColorNormal(getResources().getColor(R.color.material_lighter_blue));
    }

    private void setUserColor() {
        mFloatingActionMenu.setMenuButtonColorNormal(mUserColor);
        mBuddies.setColorNormal(mUserColor);
        mMessagesPost.setColorNormal(mUserColor);
        mPhotos.setColorNormal(mUserColor);
        mAddBuddy.setColorNormal(mUserColor);
    }

    private void loadInfoView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(layoutManager);
    }

    public void setupFloatingMenu(final List<String> buddyList) {
        // Convert List to ArrayList
        final ArrayList<String> buddies = (ArrayList<String>) buddyList;

        // Different buttons for current user and other users
        if (profileId.equals(currentUserId)) {
            mSettings.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mProfileClickListener.onSettingsClick();
                }
            });

            mMessagesPost.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mProfileClickListener.onPreviousPostsClick(profileId);
                }
            });

            mBuddies.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mProfileClickListener.onBuddiesClick(profileId, buddies);
                }
            });

            mPhotos.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mProfileClickListener.onPhotosClick(profileId);
                }
            });

        } else {
            // Until we reintroduce messages
            mMessagesPost.setVisibility(View.GONE);
            mMessagesPost.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mProfileClickListener.onMessageUserClick(profileId, profileUsername);
                }
            });
            mBuddies.setVisibility(View.GONE); //Initially gone. If already friend then can see buddies
            mBuddies.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mProfileClickListener.onBuddiesClick(profileId, buddies);
                }
            });

            mPhotos.setVisibility(View.GONE);
            mPhotos.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mProfileClickListener.onPhotosClick(profileId);
                }
            });

            // Add addBuddy click listener after we find out if they have this user as a buddy or not
        }

    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.whereEqualTo("objectId", profileId);
        query.getFirstInBackground(new GetCallback<ParseUser>() {
            @Override
            public void done(ParseUser userData, ParseException e) {
                if (e == null) {
                    // Set profile pic to parse image view
                    userProfileData = userData;
                    setData(userData);
                    if (!profileId.equals(ParseUser.getCurrentUser().getObjectId())) {
                        List<String> buddiesList = userData.getList("buddies");

                        // For other profiles first query then set to OnClick
                        setupFloatingMenu(buddiesList);

                        checkIfAlreadyBuddy();
                    }
                } else {
                    Log.d("error", "Error: " + e.getMessage());
                }
            }
        });
    }

    public void checkIfAlreadyBuddy(){
        // TODO: Possible security issue. Do not form views if buddy does not exist
        foundInBuddyList = false;
        foundInPendingBuddyList = false;
        if (userProfileData.getList("pendingBuddies") != null) {
            if (userProfileData.getList("pendingBuddies").contains(ParseUser.getCurrentUser().getObjectId())) {
                mAddBuddy.setLabelText("Pending");
                foundInPendingBuddyList = true;
            }
        }
        if (userProfileData.getList("buddies") != null && !foundInPendingBuddyList) {
             if (userProfileData.getList("buddies").contains(ParseUser.getCurrentUser().getObjectId())) {
                 mPhotos.setVisibility(View.VISIBLE);
                 mBuddies.setVisibility(View.VISIBLE);
                 foundInBuddyList = true;
                 mAddBuddy.setLabelText("Remove Buddy");
                 mAddBuddy.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        removeBuddy();
                        mAddBuddy.setLabelText("Pending");

                    }
                });
            }
        }
        if (!foundInPendingBuddyList && !foundInBuddyList){
            mAddBuddy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addBuddy();
                    // On Click change to Pending
                    mAddBuddy.setLabelText("Pending");
                }
            });
        }

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception.
        try {
            mProfileClickListener = (OnProfileClickListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnProfileClickListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mProfileClickListener = null;
    }

    public void setData(ParseUser userData) {

        String ColorResource = userData.getString("color");
        mUserColor = getActivity().getResources().getColor(Util.getResolvedUserColor(getActivity(), ColorResource));

        // If it's a different user show full profile fragment
        if (!profileId.equals(ParseUser.getCurrentUser().getObjectId())) {

            // User Name
            nameTextView.setText(userData.getString("realName"));

            //Pop count
            if (userData.get("posts") != null) {
                popCountTextView.setText(userData.get("posts").toString() + " Pops");
            } else {
                popCountTextView.setText("0");
            }

            if (userData.getParseFile("profilePicture") != null) {

//                DownloadImageAndBlurTask task = new DownloadImageAndBlurTask(this);
//                task.execute(userData.getParseFile("profilePicture"));

                String profileUrl = userData.getParseFile("profilePicture").getUrl();
                Picasso.with(mProfilePic.getContext())
                        .load(profileUrl)
                        .error(R.drawable.ic_error)
                        .placeholder(R.drawable.ic_stub)
                        .into(mProfilePic);
            }

            if (mUserColor != 0) {
                mProfilePictureBackgroundColor.setBackgroundColor(mUserColor);
                setUserColor();
            }

            if ( userData.getString("aboutMe") != null && !userData.getString("aboutMe").isEmpty() ) {
                aboutMeTextDisplay.setText(userData.getString("aboutMe"));
            }

        } else {

            // User information is set in HeaderProfileAdapter
            getOldPostsData(userData);

        }

    }

    public void getOldPostsData(final ParseUser userData) {
        ParseQuery<Post> query = new ParseQuery<>("Post");
        ParseObject userObject = ParseObject.createWithoutData("_User", userData.getObjectId());
        query.whereEqualTo("author", userObject);
        query.setLimit(3);
        query.orderByDescending("likes");
        query.findInBackground(new FindCallback<Post>() {
            @Override
            public void done(List<Post> list, ParseException e) {
                if (e == null) {
                    FeedAdapter feedAdapter = new FeedAdapter(getActivity(), list, size, FeedFragment.SHOW_USERS_OLD_POSTS);
                    ProfileAdapter mHeaderAdapter = new ProfileAdapter(getActivity(), feedAdapter, userData);
                    mProfileAdapter = new RecyclerViewMaterialAdapter(mHeaderAdapter);
                    mRecyclerView.setAdapter(mProfileAdapter);
                    MaterialViewPagerHelper.registerRecyclerView(getActivity(), mRecyclerView, null);
                } else {
                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void addBuddy() {
        String username = userProfileData.getUsername();
        String requestingUserName = ParseUser.getCurrentUser().getUsername();
        ParseCloudCode.setPendingBuddy(username, requestingUserName);
    }

    public void removeBuddy() {
        String username = userProfileData.getUsername();
        ParseCloudCode.removeBuddy(username);
    }

//    @Override
//    public void returnBitmap(Bitmap bitmap) {
//        mProfilePictureBackgroundColor.setImageBitmap(bitmap);
//    }

}