package com.bubblesocialapp.bubble.fragments;

import android.app.Activity;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.adapters.ColorAdapter;
import com.bubblesocialapp.bubble.bean.ColorBean;
import com.bubblesocialapp.bubble.common.Constants;
import com.bubblesocialapp.bubble.common.Util;
import com.bubblesocialapp.bubble.dialogs.CameraDialog;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnEditProfileFragmentListener} interface
 * to handle interaction events.
 * Use the {@link EditProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EditProfileFragment extends Fragment {

    private CircleImageView mProfileImageView;
    private EditText mUsername;
    private EditText mAboutMe;
    private ParseUser mCurrentUser = ParseUser.getCurrentUser();
    private RelativeLayout mFirstScreen;
    private RelativeLayout mColorEditScreen;
    private GridView mGridView;
    private SharedPreferences.OnSharedPreferenceChangeListener mSharedPreferencesListener;
    private View mToolbarView;
    private int mColorChosen;
    private boolean mProfilePictureChanged = false;

    public static EditProfileFragment newInstance(String param1, String param2) {
        EditProfileFragment fragment = new EditProfileFragment();
        return fragment;
    }

    public EditProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_profile, container, false);

        loadViewComponents(view);

        setClickListeners();

        setColorsToGridView();

        setUserDataToViews();

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mColorChosen = sp.getInt(Constants.USER_COLOR, R.color.bubble_pink);

        mSharedPreferencesListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
            public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
                if (key.equals(Constants.USER_COLOR) ) {
                    mColorChosen = prefs.getInt(key, R.color.bubble_pink);
                    mToolbarView.setBackgroundColor(mColorChosen);
                }
            }
        };
        sp.registerOnSharedPreferenceChangeListener(mSharedPreferencesListener);

        return view;
    }

    private void setClickListeners() {
        mProfileImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CameraDialog dialog = new CameraDialog();
                dialog.show(getActivity().getSupportFragmentManager(), "editScreenProfilePicture");
            }
        });
    }

    private void loadViewComponents(View view) {
        mProfileImageView = (CircleImageView) view.findViewById(R.id.profile_image_edit_fragment);
        mUsername = (EditText) view.findViewById(R.id.username_edit_fragment);
        mAboutMe = (EditText) view.findViewById(R.id.about_me_edit_fragment);
        mFirstScreen = (RelativeLayout) view.findViewById(R.id.picture_about_me_layout);
        mColorEditScreen = (RelativeLayout) view.findViewById(R.id.customize_profile_layout);
        mGridView = (GridView) view.findViewById(R.id.edit_profie_colors_gridview);
        mToolbarView = (View) view.findViewById(R.id.toolbar_color_view);

        int width = Util.getDisplaySize(getActivity()).x;
        int height = Util.getDisplaySize(getActivity()).y;
        FrameLayout.LayoutParams firstScreenParams = (FrameLayout.LayoutParams) mFirstScreen.getLayoutParams();
        RelativeLayout.LayoutParams colorEditParams = (RelativeLayout.LayoutParams) mColorEditScreen.getLayoutParams();

        firstScreenParams.width = width;
        firstScreenParams.height = height;
        colorEditParams.width = width;
        colorEditParams.setMargins(width, 0, 0, 0);

        mFirstScreen.setLayoutParams(firstScreenParams);
        mColorEditScreen.setLayoutParams(colorEditParams);

    }

    public void setColorsToGridView(){
        ArrayList<ColorBean> allColors = new ArrayList<>();
        int[] colorArray = getActivity().getResources().getIntArray(R.array.fragment_color_picker);
        for (int i = 0; i < colorArray.length; i++){
            allColors.add(new ColorBean(colorArray[i]));
        }
        final ColorAdapter adapter = new ColorAdapter(getActivity(),0,allColors);
        mGridView.setAdapter(adapter);
    }

    private void setUserDataToViews() {
        String profileUrl = mCurrentUser.getParseFile("profilePicture").getUrl();
        Picasso.with(mProfileImageView.getContext())
                .load(profileUrl)
                .error(R.drawable.ic_error)
                .placeholder(R.drawable.ic_stub)
                .into(mProfileImageView);

        mUsername.setText(mCurrentUser.getUsername());
        mAboutMe.setText(mCurrentUser.getString("aboutMe"));
    }

    @Override
    public void onPause() {
        super.onPause();
        saveDataToParse();
    }

    private void saveDataToParse() {
        String colorString = Util.getColorStringFromResource(getActivity(),mColorChosen);
        ParseUser mCurrentUser = ParseUser.getCurrentUser();
        mCurrentUser.put("color", colorString);
        mCurrentUser.put("username", mUsername.getText().toString());
        mCurrentUser.put("aboutMe", mAboutMe.getText().toString());
        if (mProfilePictureChanged) {
//            mCurrentUser.put("profilePicture", profilePicture);
            mCurrentUser.saveInBackground();
        } else {
            mCurrentUser.saveInBackground();
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnEditProfileFragmentListener {
        // TODO: Update argument type and name
        public void onEditProfile(Uri uri);
    }

}
