package com.bubblesocialapp.bubble.fragments;

import android.content.Context;
import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.adapters.SearchAdapter;
import com.bubblesocialapp.bubble.backgroundTasks.SearchTask;
import com.bubblesocialapp.bubble.bean.HeaderItemBean;
import com.bubblesocialapp.bubble.common.LocationController;
import com.bubblesocialapp.bubble.interfaces.ReturnSearchResultsInterface;

import java.util.ArrayList;
import java.util.List;

public class SearchFragment extends Fragment implements ReturnSearchResultsInterface{

    private RecyclerView mRecyclerView;
    private LocationController mLocationController;
    private Location mCurrentLocation;
    private List<HeaderItemBean> allData = new ArrayList<HeaderItemBean>();
    private SearchAdapter mAdapter;

    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_search_recycler_view, container, false);
    }

    private void loadInfoView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.search_recycler_view);

        loadInfoView();

        final EditText searchBox = (EditText) view.findViewById(R.id.search_box);

        searchBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (searchBox.getText() != null) {
                    queryParse(getActivity(), searchBox.getText().toString());
                }
            }
        });
    }

    public void queryParse(Context context, String searchString){
        SearchTask searchTask = new SearchTask(context,this);
        searchTask.execute(searchString);
    }

    @Override
    public void onSearchResultsFound(List<HeaderItemBean> list) {
        mAdapter = new SearchAdapter(getActivity(),list);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.invalidate();
    }
}

//        setHeaderMode(LayoutManager.LayoutParams.HEADER_INLINE);
//        setHeadersOverlaid(false);
//        setHeadersSticky(true);

//    public void setHeaderMode(int mode) {
//        mHeaderDisplay = mode | (mHeaderDisplay & LayoutManager.LayoutParams.HEADER_OVERLAY) | (
//                mHeaderDisplay & LayoutManager.LayoutParams.HEADER_STICKY);
////        mAdapter.setHeaderDisplay(mHeaderDisplay);
//    }
//
//    public void setHeadersOverlaid(boolean areHeadersOverlaid) {
//        mHeaderDisplay = areHeadersOverlaid ? mHeaderDisplay
//                | LayoutManager.LayoutParams.HEADER_OVERLAY
//                : mHeaderDisplay & ~LayoutManager.LayoutParams.HEADER_OVERLAY;
////        mAdapter.setHeaderDisplay(mHeaderDisplay);
//    }
//
//    public void setHeadersSticky(boolean areHeadersSticky) {
//        mHeaderDisplay = areHeadersSticky ? mHeaderDisplay
//                | LayoutManager.LayoutParams.HEADER_STICKY
//                : mHeaderDisplay & ~LayoutManager.LayoutParams.HEADER_STICKY;
////        mAdapter.setHeaderDisplay(mHeaderDisplay);
//    }