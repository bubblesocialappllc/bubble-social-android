package com.bubblesocialapp.bubble.fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ScaleDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.common.ParseCloudCode;
import com.bubblesocialapp.bubble.common.Util;
import com.bubblesocialapp.bubble.parseObjects.Post;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**4
 * Created by sadaf on 7/23/15.
 *
 * This fragment is opened when a user clicks a push notification.
 * This fragment then shows the post in a separate view.
 */
public class NotificationPostFragment extends Fragment {

    private static final String FlAG = "flag";

    private static final String OBJECT_ID = "objectId";

    private static final String POST = "post";

    private static final String COMMENT = "comment";

    private String mObjectId;
    private String mFlag;

    private ImageView postImage;
    private ImageView profileImage;
    private CircleImageView profileImageActualImage;
    private TextView authorName;
    private TextView locationName;
    private TextView textContent;
    private TextView dateString;
    private TextView objectIdString;
    private Button numberOfLikes;
    private Button comments;
    private Boolean liked;
    private List<String> likers;
    private int numberLiked;
    private List<String> commenters;
    private View mView;
    private String nameToDisplay;
    private Point mSize;
    private NotificationPostListener mListener;

    public interface NotificationPostListener {
        /**
         * Called when posting
         */
        public void onCommentButtonSelected(String string, String postAuthorUsername);
    }

    public static NotificationPostFragment newInstance(String objectId, String flag){
        NotificationPostFragment fragment = new NotificationPostFragment();
        Bundle args = new Bundle();
        args.putString(FlAG, flag);
        args.putString(OBJECT_ID, objectId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mObjectId = getArguments().getString(OBJECT_ID);
            mFlag = getArguments().getString(FlAG);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.fragment_feed_item, container, false);

        // Get Screen size to expand picture
        WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        mSize = new Point();
        display.getSize(mSize);
        mSize.x = (int) Util.pxToDp(getActivity(), mSize.x);
        mSize.y = (int) Util.pxToDp(getActivity(),mSize.y);

        if (mFlag.equals(POST)) {
            getDataPost();
        } else if (mFlag.equals(COMMENT)) {
            getDataComment();
        }


        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void getDataComment(){
        //Do nothing at the moment
    }

    private void getDataPost(){
        ParseQuery<Post> query = new ParseQuery<Post>("Post");
        query.whereEqualTo("objectId", mObjectId);
        query.getFirstInBackground(new GetCallback<Post>() {
            @Override
            public void done(Post post, ParseException e) {
                if (e != null) {
                    Toast.makeText(getActivity(),e.getMessage(),Toast.LENGTH_LONG).show();
                } else {
                    setData(post);
                }
            }
        });
    }

    private void findAndSetViews(){
        profileImageActualImage = (CircleImageView) mView.findViewById(R.id.profile_picture_circle_imageView);
        postImage = (ImageView) mView.findViewById(R.id.shared_image_imageView);
        profileImage = (ImageView) mView.findViewById(R.id.profile_picture_imageView);
        authorName = (TextView) mView.findViewById(R.id.username_textView);
        locationName = (TextView) mView.findViewById(R.id.post_location_textView);
        textContent = (TextView) mView.findViewById(R.id.comment_textView);
        dateString = (TextView) mView.findViewById(R.id.post_time_textView);
        objectIdString = (TextView) mView.findViewById(R.id.storeObjectId);
        numberOfLikes = (Button) mView.findViewById(R.id.likeButton);
        comments = (Button) mView.findViewById(R.id.commentButton);
        liked = false;
        likers = null;
        numberLiked = 0;
        commenters = null;

        profileImageActualImage.setVisibility(View.INVISIBLE);
        profileImage.setVisibility(View.INVISIBLE);
        postImage.setImageDrawable(null);
        authorName.setText(null);
        comments.setText(null);
        dateString.setText(null);
        locationName.setText(null);
        numberOfLikes.setText(null);
        textContent.setText(null);
        objectIdString.setText(null);
    }

    private void setData(Post post){

        findAndSetViews();

        if (post.getProfilePicURL() != null) {
            String profilePicFile = post.getProfilePicURL();
            profileImageActualImage.setVisibility(View.VISIBLE);
            Picasso.with(profileImageActualImage.getContext())
                    .load(profilePicFile)
                    .error(R.drawable.ic_error)
                    .placeholder(R.drawable.ic_stub)
                    .resize(100, 100).centerCrop()
                    .into(profileImageActualImage);
        } else {
            setRandomProfilePicture(post);
        }

        String imageFile = post.getPostImageURL();

        if (imageFile != null) {
            postImage.setVisibility(View.VISIBLE);
            Picasso.with(postImage.getContext())
                    .load(imageFile)
                    .error(R.drawable.ic_error)
                    .resize(mSize.x*2,0)
                    .placeholder(R.drawable.ic_stub)
                    .into(postImage);
        } else {
            postImage.setVisibility(View.GONE);
        }

        authorName.setText(post.getAuthorUsername());
        locationName.setText(post.getLocationName());
        textContent.setText(post.getText());
        dateString.setText(post.getPostTime());
        objectIdString.setText(post.getObjectId());

        String objectIdText = objectIdString.getText().toString();

        setUpLikes(post, objectIdText);
        setUpComments(post,objectIdText);

    }

    private void setUpComments(Post post, final String objectIdText){
        Drawable drawable = getActivity().getResources().getDrawable(R.drawable.comment_icon_white);
        drawable.setBounds(0,0, (int)(drawable.getIntrinsicWidth()*0.08),
                (int)(drawable.getIntrinsicHeight()*0.08));
        ScaleDrawable scaleDrawable = new ScaleDrawable(drawable,0,
                comments.getWidth(),comments.getHeight());
        comments.setCompoundDrawables(scaleDrawable.getDrawable(), null, null, null);

        commenters = post.getCommenters();
        if (commenters != null) {
            if (commenters.contains(ParseUser.getCurrentUser().getUsername())) {
                comments.setBackgroundResource(R.drawable.buttonshape_pink);
            } else {
                comments.setBackgroundResource(R.drawable.buttonshape_grey);
            }
        }

        String formattedString = getActivity().getString(R.string.format_comments_likes, post.getNumberOfComments());

        comments.setText(formattedString);
        comments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onCommentButtonSelected(objectIdText, authorName.getText().toString());
            }
        });
    }

    private void setUpLikes(Post post, final String objectIdText){

        Drawable drawable = getActivity().getResources().getDrawable(R.drawable.like_icon_white);
        drawable.setBounds(0,0, (int)(drawable.getIntrinsicWidth()*0.05),
                (int)(drawable.getIntrinsicHeight()*0.05));
        ScaleDrawable scaleDrawable = new ScaleDrawable(drawable,0,
                numberOfLikes.getWidth(),numberOfLikes.getHeight());
        numberOfLikes.setCompoundDrawables(scaleDrawable.getDrawable(), null, null, null);

        // Check if already liked
        likers = post.getLikedUsers();
        if (likers != null) {
            if (likers.contains(ParseUser.getCurrentUser().getUsername())) {
                liked = true;
                numberOfLikes.setBackgroundResource(R.drawable.buttonshape_pink);
            } else {
                liked = false;
                numberOfLikes.setBackgroundResource(R.drawable.buttonshape_grey);
            }
        }

        numberLiked = post.getLikes();
        String formattedString = getActivity().getString(R.string.format_comments_likes, numberLiked);
        if (numberLiked != 0) {
            numberOfLikes.setText(formattedString);
        } else {
            numberOfLikes.setText(formattedString);
        }

        numberOfLikes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!liked) {
                    ParseCloudCode.likePost(objectIdText);
                    numberOfLikes.setBackgroundResource(R.drawable.buttonshape_pink);
                    liked = true;
                    numberLiked += 1;
                    String formattedString = getActivity().getString(R.string.format_comments_likes, numberLiked);
                    numberOfLikes.setText(formattedString);
                } else {
                    ParseCloudCode.unlikePost(ParseUser.getCurrentUser().toString(), objectIdText);
                    numberOfLikes.setBackgroundResource(R.drawable.buttonshape_grey);
                    liked = false;
                    numberLiked -= 1;
                    String formattedString = getActivity().getString(R.string.format_comments_likes, numberLiked);
                    numberOfLikes.setText(formattedString);
                }
            }
        });

    }

    private void setRandomProfilePicture(Post post){
        // Add Auto Profile Picture
        ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
        // generate random color
        int color1 = generator.getRandomColor();

        profileImage.setVisibility(View.VISIBLE);
        String profileName = post.getAuthorUsername();
        if (profileName != null) {
            String[] parseName = profileName.split(" ", 0);
            if (parseName.length != 1) {
                nameToDisplay = parseName[0].substring(0, 1) +
                        parseName[1].substring(0, 1);
            } else {
                nameToDisplay = parseName[0].substring(0, 2);
            }

        } else {
            nameToDisplay = "NA";
        }

        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .textColor(Color.WHITE)
                .useFont(Typeface.DEFAULT)
                .fontSize(30) /* size in px */
                .bold()
                .toUpperCase()
                .endConfig()
                .buildRound(nameToDisplay, color1);

        profileImage.setImageDrawable(drawable);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception.
        try {
            mListener = (NotificationPostListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement NotificationPostListener");
        }
    }


}
