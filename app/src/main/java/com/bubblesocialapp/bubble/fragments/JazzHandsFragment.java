package com.bubblesocialapp.bubble.fragments;


import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bubblesocialapp.bubble.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class JazzHandsFragment extends Fragment {

    private static final String DRAWABLE = "drawable";

    private String mDrawableString;
    private ImageView mImageView;

    public static JazzHandsFragment newInstance(String drawableName) {
        JazzHandsFragment fragment = new JazzHandsFragment();
        Bundle args = new Bundle();
        args.putString(DRAWABLE, drawableName);
        fragment.setArguments(args);
        return fragment;
    }

    public JazzHandsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mDrawableString = getArguments().getString(DRAWABLE);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_jazz_hands_screen_one, container, false);

        mImageView = (ImageView) view.findViewById(R.id.jazz_hands_image_view);

        Context context = mImageView.getContext();
        int id = context.getResources().getIdentifier(mDrawableString, "drawable", context.getPackageName());
        mImageView.setImageResource(id);

        return view;
    }


}
