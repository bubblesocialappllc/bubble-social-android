package com.bubblesocialapp.bubble.fragments;

import android.app.Activity;
import android.content.Intent;
import android.location.Address;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapEditText;
import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.adapters.CommentsAdapter;
import com.bubblesocialapp.bubble.common.Constants;
import com.bubblesocialapp.bubble.common.LocationController;
import com.bubblesocialapp.bubble.common.Util;
import com.bubblesocialapp.bubble.customLayouts.customRecyclerView.CustomRecyclerView;
import com.bubblesocialapp.bubble.customLayouts.swipeToDismiss.SwipeDismissTouchListener;
import com.bubblesocialapp.bubble.parseObjects.Comment;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.bubblesocialapp.bubble.parseObjects.Post;
import com.parse.SaveCallback;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.bubblesocialapp.bubble.common.ParseCloudCode.postComment;

public class CommentsFragment extends Fragment
        implements LocationController.LocationCallBack{

    private static final String POST_ID = "PostId";
    private static final String POST_USERNAME = "Post_username";

    public String commentsForPost;
    private byte[] photoFile = null;
    private Post mPost;
    private ParseFile userProfile;
    private CustomRecyclerView mRecyclerView;
    private BootstrapEditText postText;
    private ImageButton sendButton;
    private CommentsAdapter mAdapter;
    private List<Comment> mComments;
    private boolean mAddressFound = false;
    private Location mLastLocation;
    private CardView mCardView;
    private OnCommentsFragmentListener mListener;
    private Address mAddress;
    private String mCityState;
    private String mPostAuthorUsername;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private LocationController mLocationController;
    private ImageButton mImageButton;
    private Uri saveImageUri;
    private ImageView mCommentPicture;
    private ParseFile savePhoto;

    public interface OnCommentsFragmentListener {
        public void onFragmentFinish();
    }

    public static CommentsFragment newInstance(String objectId, String postAuthorUsername) {
        CommentsFragment commentsFragment = new CommentsFragment();
        Bundle args = new Bundle();
        args.putString(POST_ID, objectId);
        args.putString(POST_USERNAME, postAuthorUsername);
        commentsFragment.setArguments(args);
        return commentsFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            commentsForPost = getArguments().getString(POST_ID);
            mPostAuthorUsername = getArguments().getString(POST_USERNAME);
        }

        mLocationController = new LocationController(getActivity(),this,LocationController.FIND_ADDRESS);
        mLocationController.connectToGoogleApiClient();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_comment_recycler_view, container, false);

        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.bubble_pink));
        mCardView = (CardView) rootView.findViewById(R.id.material_com_card_view_content_main);
        mRecyclerView = (CustomRecyclerView) rootView.findViewById(R.id.fragment_recycler_view_comment);
        mCommentPicture = (ImageView) rootView.findViewById(R.id.comment_picture_taken);

        postText = (BootstrapEditText) rootView.findViewById(R.id.comment_post_textview);
        sendButton = (ImageButton) rootView.findViewById(R.id.send_button);
        mImageButton = (ImageButton) rootView.findViewById(R.id.image_button);

        loadInfoView();

        getData();

        setClickListeners();


        setScrollListener();

        return rootView;
    }

    public void setClickListeners() {
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postToParse();
                postText.getText().clear();
            }
        });

        mImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startCamera();
            }
        });
    }

    public void setScrollListener() {

        mRecyclerView.setOnTouchListener(new SwipeDismissTouchListener(mCardView, null,
                new SwipeDismissTouchListener.DismissCallbacks() {
                    @Override
                    public boolean canDismiss(Object token) {
                        return true;
                    }

                    @Override
                    public void onDismiss(View view, Object token) {
                        mListener.onFragmentFinish();
                    }
                }));

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                initiateRefresh();
            }
        });
    }

    private void loadInfoView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(layoutManager);
    }

    private void addComment() {
        Comment addComment = new Comment();
        addComment.setComment(postText.getText().toString());
        if (ParseUser.getCurrentUser().get("profilePicture") != null) {
            addComment.setProfilePicture((ParseFile) ParseUser.getCurrentUser().get("profilePicture"));
        }
        addComment.setCommenter(ParseUser.getCurrentUser().getUsername());
        addComment.setLocationName(mCityState);
        mComments.add(addComment);
        mAdapter.notifyDataSetChanged();
        mRecyclerView.scrollToPosition(mComments.size() - 1);
    }

    public void postToParse() {

        if (mCommentPicture.getDrawable() != null) {

            try {
                photoFile = Util.UriToByte(getActivity(), saveImageUri);
                photoFile = Util.scalePhoto(photoFile);
                savePhoto = new ParseFile("photo",photoFile);
                savePhoto.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e != null) {
                            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }

                });

            } catch (IOException e) {
                //Do Something
            }

        } else {
            photoFile = null;
        }

        if (mLastLocation != null && mAddressFound) {

            if (mAddress.getAdminArea() != null) {
                mCityState = mAddress.getLocality() + ", " + Util.getStateAbbreviation(getActivity(), mAddress.getAdminArea());
            } else {
                mCityState = mAddress.getLocality();
            }
            ParseGeoPoint currLocation = new ParseGeoPoint(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            postComment(commentsForPost, postText.getText().toString(), photoFile, currLocation, mCityState, mPostAuthorUsername);
            // Change to pop back to feed
            addComment();
        }else {
            Toast.makeText(getActivity(),"Cannot find location. Wait a few seconds and try again.", Toast.LENGTH_LONG).show();
        }

    }

    public void getData() {
        // Get comments data
        ParseQuery<Comment> queryComment = ParseQuery.getQuery("Comment");
        ParseObject obj = ParseObject.createWithoutData("Post", commentsForPost);
        queryComment.whereEqualTo("post", obj);
        queryComment.addAscendingOrder("createdAt");
        queryComment.findInBackground(new FindCallback<Comment>() {
            public void done(List<Comment> list, ParseException e) {
                if (e == null) {
                    mComments = list;
                    mAdapter = new CommentsAdapter(getActivity(), list);
                    mRecyclerView.setAdapter(mAdapter);
                    mRecyclerView.scrollToPosition(mComments.size() - 1);
                } else {
                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                }
                onRefreshComplete();
            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnCommentsFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement PostListener");
        }
    }

    @Override
    public void onNewLocation(Location location, float accuracy) {
        mLastLocation = location;
        getData();
    }

    @Override
    public void onAddressFound(List<Address> address) {
        if (address != null) {
            mAddressFound = true;
            mAddress = address.get(0);
        }
    }

    private void initiateRefresh() {
        mLocationController.startLocationUpdates();
        mSwipeRefreshLayout.setRefreshing(true);
        //TODO: Put in network time out error
    }


    private void onRefreshComplete() {

        // Stop the refreshing indicator
        mAdapter.notifyDataSetChanged();
        mSwipeRefreshLayout.setRefreshing(false);
    }

    private void startCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File bubbleImageFolder = new File(Environment.getExternalStorageDirectory() + File.separator + "bubble");

        if (!bubbleImageFolder.exists()){
            bubbleImageFolder.mkdirs();
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String fileName = "bubble_picture_" + timeStamp + ".jpg";
        File output = new File(bubbleImageFolder, fileName);
        saveImageUri = Uri.fromFile(output);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, saveImageUri);

        getActivity().startActivityForResult(intent, Constants.REQUEST_PHOTO_FOR_COMMENT);
    }

    public void setCommentPicture() {
        if (saveImageUri != null) {
            mCommentPicture.setVisibility(View.VISIBLE);
            mCommentPicture.setImageURI(saveImageUri);
        }
    }
}


