package com.bubblesocialapp.bubble.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Layout;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.backgroundTasks.GetSuggestionsTask;
import com.bubblesocialapp.bubble.common.Constants;
import com.bubblesocialapp.bubble.common.LocationController;
import com.bubblesocialapp.bubble.common.Util;
import com.bubblesocialapp.bubble.customLayouts.mentionLayout.MentionView;
import com.bubblesocialapp.bubble.interfaces.ReturnedResultsInterface;
import com.bubblesocialapp.bubble.parseObjects.Post;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseUser;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.bubblesocialapp.bubble.common.ParseCloudCode.postStatus;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class PostFragment extends Fragment
        implements LocationController.LocationCallBack,
        ReturnedResultsInterface{

    public interface PostSendListener {
        public void onPostSendClicked(Post post);
    }


    private byte[] photoFile = null;
    private ImageView pictureShownInPost;
    private Uri saveImageUri;
    private EditText postEditText;
    private ParseFile savePhoto;
    private Location mLastLocation;
    private boolean mAddressFound = false;
    private Address mAddress;
    private Toolbar mToolbar;
    private ImageButton mLocationButton;
    private ImageButton mImageCaptureButton;
    private TextView mLocationTextView;
    private MentionView mMentionView;
    private List<String> mMentionedList = new ArrayList<>();
    private PostSendListener mListener;

    public PostFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LocationController locationController = new LocationController(getActivity(),this,LocationController.FIND_ADDRESS);
        locationController.connectToGoogleApiClient();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_post, container, false);
        pictureShownInPost = (ImageView) v.findViewById(R.id.post_image);
        mToolbar = (Toolbar) v.findViewById(R.id.post_toolbar);
        postEditText = (EditText) v.findViewById(R.id.post_box);
        mImageCaptureButton = (ImageButton) v.findViewById(R.id.image_button);
        mLocationButton = (ImageButton) v.findViewById(R.id.location_button);
        mLocationTextView = (TextView) v.findViewById(R.id.location_textview);
        mMentionView = (MentionView) v.findViewById(R.id.mention_view);

        setClickListeners();

        // Suppose to open keyboard on startup. Doesn't work -_- TODO: Open Keyboard
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        locationAndMentionWatcher();

        return v;
    }

    public void setClickListeners() {

        mImageCaptureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startCamera();
            }
        });

        mLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startLocationPicker();
            }
        });

        mToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case (R.id.post_button):
                        checkPostToParse();
                        break;
                }
                return false;
            }
        });

        mToolbar.inflateMenu(R.menu.menu_post_fragment);
    }

    public void startLocationPicker() {
        try {

            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            getActivity().startActivityForResult(builder.build(getActivity()), Constants.PLACE_PICKER_REQUEST);

        } catch (GooglePlayServicesNotAvailableException | GooglePlayServicesRepairableException e) {
            // log message
        }
    }

    protected void locationAndMentionWatcher() {
        postEditText.addTextChangedListener(new TextWatcher() {
            boolean mention = false;
            String searchText = null;

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int atPosition = 0;
                try {
                    String value = Character.toString(charSequence.charAt(i));
                    if (value.equals("@")) {
                        atPosition = i;
                        mention = true;
                        searchText = charSequence.toString();
                    } else if (value.equals(" ")) { // Stop AutoComplete after first space after @
                        mention = false;
                        mMentionView.setVisibility(View.GONE);
                    }

                    // Implies @ was deleted therefore stop mention view
                    if (mention && !Character.toString(charSequence.charAt(atPosition)).equals("@")) {
                        mMentionView.setVisibility(View.GONE);
                    }
                } catch (IndexOutOfBoundsException e) {
                    e.getMessage();
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (mention) {
                    String text = editable.toString();
                    String searchString = text.substring(text.lastIndexOf("@") + 1);
                    // Don't search empty string
                    if (!searchString.equals("")) {
                        new GetSuggestionsTask(PostFragment.this).execute(searchString);
                    }
                }
            }
        });
    }

    public void startCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File bubbleImageFolder = new File(Environment.getExternalStorageDirectory() + File.separator + "bubble");

        if (!bubbleImageFolder.exists()){
            bubbleImageFolder.mkdirs();
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String fileName = "bubble_picture_" + timeStamp + ".jpg";
        File output = new File(bubbleImageFolder, fileName);
        saveImageUri = Uri.fromFile(output);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, saveImageUri);

        getActivity().startActivityForResult(intent, Constants.REQUEST_PHOTO_FOR_POST);
    }

    public void setPostPicture() {
        if (saveImageUri != null) {
            pictureShownInPost.setVisibility(View.VISIBLE);
            pictureShownInPost.setImageURI(saveImageUri);
        }
    }

    private static File getOutputMediaFile() {
        File bubbleStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "Bubble");

        if (!bubbleStorageDir.exists()) {
            bubbleStorageDir.mkdirs();
            if (!bubbleStorageDir.mkdirs()) {
                Log.d("Bubble", "failed to create directory");
                return null;
            }
        }

        // PictureName
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile = new File(bubbleStorageDir.getPath() + File.separator +
                "IMG_" + timeStamp + ".jpg");

        return mediaFile;
    }

    public void checkPostToParse() {
        if (mAddressFound) {
            if (postEditText.getText().length() == 0) {
                Toast.makeText(getActivity(),"Post must not be empty", Toast.LENGTH_LONG).show();
            } else {
                postToParse();
            }
        } else {
            Toast.makeText(getActivity(),"Address not found. Wait a few minutes and try again", Toast.LENGTH_LONG).show();
        }

    }

    public void postToParse() {

        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(postEditText.getWindowToken(), 0);

        if (pictureShownInPost.getDrawable() != null) {

            try {
                photoFile = Util.UriToByte(getActivity(), saveImageUri);
                photoFile = Util.scalePhoto(photoFile);
            } catch (IOException e) {
                //Do Something
            }

        } else {
            photoFile = null;
        }

        if (mLastLocation != null) {
            String mCityState;
            if (!mLocationTextView.getText().equals("My Location")) {
                mCityState = mLocationTextView.getText().toString();
            } else {
                if (mAddress.getAdminArea() != null) {
                    mCityState = mAddress.getLocality() + ", " + Util.getStateAbbreviation(getActivity(), mAddress.getAdminArea());
                } else {
                    mCityState = mAddress.getLocality();
                }
            }

            ParseGeoPoint currLocation = new ParseGeoPoint(mLastLocation.getLatitude(), mLastLocation.getLongitude());

            boolean shouldSavePhoto = false;

            postStatus(shouldSavePhoto, postEditText.getText().toString(), photoFile,
                    currLocation, mCityState, mMentionedList, "", "", null);

            Post sentPost = new Post();
            sentPost.setAuthor(ParseUser.getCurrentUser());
            sentPost.setPostTime();
            sentPost.setText(postEditText.getText().toString());
            sentPost.setAuthorUsername(ParseUser.getCurrentUser().getUsername());
            sentPost.setAuthorId(ParseUser.getCurrentUser().getObjectId());
            sentPost.setLocation(currLocation);
            sentPost.setLocationName(mCityState);
            sentPost.setPostImage(saveImageUri);
            sentPost.setProfilePic(ParseUser.getCurrentUser().getParseFile("profilePicture"));
            sentPost.setMentioned(mMentionedList);

            mListener.onPostSendClicked(sentPost);
        } else {
            Toast.makeText(getActivity(),"Location could not be found", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onAddressFound(List<Address> address) {
        if (address != null) {
            mAddressFound = true;
            mAddress = address.get(0);
        }

    }

    @Override
    public void onNewLocation(Location location, float accuracy) {
        mLastLocation = location;
    }

    @Override
    public void returnedResults(List<ParseObject> parseObjects) {
        if (parseObjects != null) {

            int pos = postEditText.getSelectionStart();
            Layout layout = postEditText.getLayout();
            int line = layout.getLineForOffset(pos);
            int baseline = layout.getLineBaseline(line);
            int ascent = layout.getLineAscent(line);
            float x = layout.getPrimaryHorizontal(pos);
            float y = baseline + ascent + Util.dpToPx(getActivity(),30);

            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mMentionView.getLayoutParams();
            params.setMargins((int) x ,(int) y,0,0);
            mMentionView.setLayoutParams(params);
            mMentionView.setMentionAdapter(parseObjects);
            mMentionView.getMentionAdapter().notifyDataSetChanged();
            mMentionView.setVisibility(View.VISIBLE);
        }
    }

    public void setPlace(Place place) {
        if (place.getName() != null)
            mLocationTextView.setText(place.getName().toString());
    }

    public void addMention(String string) {
        mMentionedList.add(string);
        String typedText = postEditText.getText().toString();
        // Get TypedSearchText
        String searchString = typedText.substring(typedText.lastIndexOf("@"));
        int positionOfAt = typedText.indexOf("@");
        // Remove typedSearchText
        postEditText.setText(postEditText.getText().delete(positionOfAt,positionOfAt+searchString.length()));
        // Add Mention
        postEditText.append(Util.makeToken(getActivity(),string));
        postEditText.setSelection(postEditText.getText().length());
        mMentionView.setVisibility(View.GONE);

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception.
        try {
            mListener = (PostSendListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement PostSendListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

}






