package com.bubblesocialapp.bubble.fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.activities.LoginActivity;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;

public class SettingsFragment extends Fragment {

    private View mView;
    private TextView mEditMyProfile;
    private TextView mShareWithFriends;
    private TextView mRateBubbleOnPlayStore;
    private TextView mFollowOnTwitter;
    private TextView mLikeOnFacebook;
    private TextView mFollowOnInstagram;
    private TextView mContactUs;
    private TextView mChangeMyPassword;
    private TextView mBlockedUsers;
    private TextView mLogOut;
    private TextView mTermsOfUse;
    private TextView mLicenses;
    private TextView mReportAbuse;
    private TextView mDeleteMyAccount;
    private ParseUser mCurrentUser;
    private OnSettingsClickListener mListener;


    public interface OnSettingsClickListener {
        // TODO: Update argument type and name
        public void onEditProfileClick();
        public void onBlockedUsersClick();
    }



    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_settings, container, false);
        mCurrentUser = ParseUser.getCurrentUser();
        loadViewComponents();
        setOnClickListeners();

        return mView;
    }

    public void loadViewComponents() {
        mEditMyProfile = (TextView) mView.findViewById(R.id.edit_my_profile);
        mShareWithFriends = (TextView) mView.findViewById(R.id.share_bubble_friends);
        mRateBubbleOnPlayStore = (TextView) mView.findViewById(R.id.rate_bubble_on_store);
        mFollowOnTwitter = (TextView) mView.findViewById(R.id.follow_twitter);
        mLikeOnFacebook = (TextView) mView.findViewById(R.id.like_on_facebook);
        mFollowOnInstagram = (TextView) mView.findViewById(R.id.follow_instagram);
        mContactUs = (TextView) mView.findViewById(R.id.contact_us);
        mChangeMyPassword = (TextView) mView.findViewById(R.id.change_my_password);
        mBlockedUsers = (TextView) mView.findViewById(R.id.blocked_users);
        mLogOut = (TextView) mView.findViewById(R.id.log_out);
        mTermsOfUse = (TextView) mView.findViewById(R.id.terms_of_use);
        mLicenses = (TextView) mView.findViewById(R.id.licenses);
        mReportAbuse = (TextView) mView.findViewById(R.id.report_abuse);
        mDeleteMyAccount = (TextView) mView.findViewById(R.id.delete_my_account);
    }

    public void setOnClickListeners() {
        mEditMyProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startEditProfileFragment();
            }
        });
        mShareWithFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startShareWithFriendsFragment();
            }
        });
        mRateBubbleOnPlayStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startPlayStore();
            }
        });
        mFollowOnTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startFollowOnTwitter();
            }
        });
        mLikeOnFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startLikeOnFacebook();
            }
        });
        mFollowOnInstagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startFollowOnInstagram();
            }
        });
        mContactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startContactUs();
            }
        });
        mChangeMyPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startChangeMyPasswordDialog();
            }
        });
        mBlockedUsers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startBlockUsersFragment();
            }
        });
        mLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startLogOut();
            }
        });
        mTermsOfUse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startTermsOfUse();
            }
        });
        mLicenses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startLicences();
            }
        });
        mReportAbuse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startReportAbuseFragment();
            }
        });
        mDeleteMyAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startDeleteMyAccountFragment();
            }
        });
    }

    public void startEditProfileFragment() {
        mListener.onEditProfileClick();
    }

    public void startShareWithFriendsFragment() {
        try
        { Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, "Bubble Social App");
            String sAux = "\nDownload this awesome app!\n\n";
            sAux = sAux + "https://play.google.com/store/apps/details?id=com.bubblesocialapp.bubble \n\n";
            i.putExtra(Intent.EXTRA_TEXT, sAux);
            startActivity(Intent.createChooser(i, "choose one"));
        }
        catch(Exception e)
        { //e.toString();
        }
    }

    public void startPlayStore() {
        String url = "market://details?id=com.bubblesocialapp.bubble";
        openURL(url);
    }

    public void startFollowOnTwitter() {
        String url = "https://twitter.com/bubblesocialapp";
        openURL(url);
    }

    public void startLikeOnFacebook() {
        String url = "https://www.facebook.com/bubblesocialapp";
        openURL(url);
    }

    public void startFollowOnInstagram() {
        String url = "https://instagram.com/bubblesocialapp/";
        openURL(url);
    }

    public void startContactUs() {
        String url = "http://bubblesocialapp.com/contact";
        openURL(url);
    }

    public void startChangeMyPasswordDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Reset Password");
        builder.setMessage("Enter Email Address");

        // Set up the input
        final EditText input = new EditText(getActivity());
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String m_Text = input.getText().toString();
                parseResetPassword(m_Text);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    public void startBlockUsersFragment() {
        mListener.onBlockedUsersClick();
    }

    public void startLogOut() {
        mCurrentUser.logOutInBackground();
        Intent login = new Intent(getActivity(), LoginActivity.class);
        login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(login);
        getActivity().finish();
    }

    public void startTermsOfUse() {
        String url = "http://bubblesocialapp.com/legal";
        openURL(url);
    }

    public void startLicences() {
        String url = "http://bubblesocialapp.com/legal";
        openURL(url);
    }

    public void startReportAbuseFragment() {
        String url = "http://bubblesocialapp.com/contact";
        openURL(url);
    }

    public void startDeleteMyAccountFragment() {
        startDialog();
    }

    public void openURL(String url) {
        Uri uri = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }

    public void startDialog() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        //Yes button clicked
                        deleteMyAccount();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    public void deleteMyAccount() {
        mCurrentUser.deleteEventually();
        startLogOut();
    }

    public void parseResetPassword(String email) {
        ParseUser.requestPasswordResetInBackground("myemail@example.com", new RequestPasswordResetCallback() {
            public void done(ParseException e) {
                if (e == null) {
                    // An email was successfully sent with reset instructions.
                    Toast.makeText(getActivity(),R.string.reset_password, Toast.LENGTH_LONG).show();
                } else {
                    // Something went wrong. Look at the ParseException to see what's up.
                }
            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnSettingsClickListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }



}
