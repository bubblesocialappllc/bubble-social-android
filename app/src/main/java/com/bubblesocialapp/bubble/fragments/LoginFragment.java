package com.bubblesocialapp.bubble.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bubblesocialapp.bubble.R;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParsePush;
import com.parse.ParseUser;
import com.parse.SaveCallback;

/**
 * Created by Sadaf Mackertich on 11/9/2014.
 */
public class LoginFragment extends Fragment {

    private Button mLogin;
    private Button mFacebook;
    private TextView mSignup;
    private TextView mUsername;
    private TextView mPassword;
    private onButtonFromLoginFragmentClick mListener;

    public interface onButtonFromLoginFragmentClick{
        void onSignUpClick();
        void onSignUpClickFillInfo();
        void onLoginClick();
        void onLoginWithFacebookClick();
        void hideActionBar();
    }

    public LoginFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mListener.hideActionBar();
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);
        mLogin = (Button) rootView.findViewById(R.id.login);
        mFacebook = (Button) rootView.findViewById(R.id.facebook_signup);
        mSignup = (TextView) rootView.findViewById(R.id.signUp);
        mUsername = (TextView) rootView.findViewById(R.id.username);
        mPassword = (TextView) rootView.findViewById(R.id.password);

        setOnClickListeners();

        return rootView;
    }

    private void setOnClickListeners(){
        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginParse();
            }
        });

        mSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onSignUpClick();
            }
        });

        mFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onLoginWithFacebookClick();
            }
        });
    }

    private void loginParse(){
        ParseUser.logInInBackground(mUsername.getText().toString(), mPassword.getText().toString(), new LogInCallback() {
            public void done(ParseUser user, ParseException e) {
                if (user != null) {
                    subscribeUserForPushNotifications();
                    mListener.onLoginClick();
                } else {
                    // Login failed. Look at the ParseException to see what happened.
                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void subscribeUserForPushNotifications(){
        String username = ParseUser.getCurrentUser().getUsername();

        ParsePush.subscribeInBackground(username, new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Log.d("com.parse.push", "successfully subscribed to the broadcast channel.");
                } else {
                    Log.e("com.parse.push", "failed to subscribe for push", e);
                }
            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (onButtonFromLoginFragmentClick) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement onSearchResultClickListener");
        }
    }

}
