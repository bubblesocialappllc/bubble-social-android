package com.bubblesocialapp.bubble.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.adapters.MessageHeadAdapter;
import com.bubblesocialapp.bubble.customLayouts.customRecyclerView.CustomRecyclerView;
import com.bubblesocialapp.bubble.customLayouts.customViewPager.MaterialViewPagerHelper;
import com.bubblesocialapp.bubble.customLayouts.customViewPager.RecyclerViewMaterialAdapter;
import com.bubblesocialapp.bubble.parseObjects.MessageHead;
import com.github.clans.fab.FloatingActionButton;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

/**
This class is the first Fragment opened when messages is selected. It displays all the conversations
 the user is having.
 */
public class MessageHeadFragment extends Fragment {

    private OnMessageClickListener mListener;
    private FloatingActionButton floatingButton;
    private CustomRecyclerView mRecyclerView;
    private List<MessageHead> mMessageHeads;
    private List<String> userObjectIds;
    private RecyclerView.Adapter mAdapter;

    public MessageHeadFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_messagehead_recycler_view, container, false);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        mRecyclerView = (CustomRecyclerView) view.findViewById(R.id.message_recycler_view);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(layoutManager);

        floatingButton = (FloatingActionButton) view.findViewById(R.id.fragment_message_float_action_button);
        floatingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onNewMessageClick();
            }
        });

        startQuery();

        return view;
    }

    private void startQuery() {

        List<ParseQuery<MessageHead>> queries = new ArrayList<>();
        ParseQuery<MessageHead> query1 = new ParseQuery<MessageHead>("MessageHead");
        ParseQuery<MessageHead> query2 = new ParseQuery<MessageHead>("MessageHead");
        query1.whereEqualTo("firstUser", ParseUser.getCurrentUser());
        query2.whereEqualTo("secondUser", ParseUser.getCurrentUser());
        queries.add(query1);
        queries.add(query2);
        ParseQuery.or(queries).findInBackground(new FindCallback<MessageHead>() {
            @Override
            public void done(List<MessageHead> messageHeads, ParseException e) {
                if (e == null) {
                    mMessageHeads = messageHeads;
                    getConversations(messageHeads);

                } else {
                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void getConversations(List<MessageHead> messageHeads) {

        userObjectIds = new ArrayList<>();
        for (MessageHead userId : messageHeads) {
            if (userId.getUser() == ParseUser.getCurrentUser()) {
                // If user started conversation then get second users info
                userObjectIds.add(userId.getSecondUser().getObjectId());
            } else {
                // If first user started conversation then get first user info
                userObjectIds.add(userId.getUser().getObjectId());
            }

        }

        ParseQuery<ParseUser> query = new ParseQuery<ParseUser>("_User");
        query.whereContainedIn("objectId", userObjectIds);
        query.findInBackground(new FindCallback<ParseUser>() {
            @Override
            public void done(List<ParseUser> list, ParseException e) {
                setupAdapter(list);
            }
        });

    }

    public void setupAdapter(List<ParseUser> users) {
        mAdapter = new RecyclerViewMaterialAdapter(new MessageHeadAdapter(getActivity(),mMessageHeads, users));
        mRecyclerView.setAdapter(mAdapter);
        MaterialViewPagerHelper.registerRecyclerView(getActivity(), mRecyclerView, null);

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnMessageClickListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement onSearchResultClickListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnMessageClickListener {
        public void onNewMessageClick();
    }


}
