package com.bubblesocialapp.bubble.fragments;

import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.adapters.BuddiesAdapter;
import com.bubblesocialapp.bubble.adapters.FeedAdapter;
import com.bubblesocialapp.bubble.common.Util;
import com.bubblesocialapp.bubble.customLayouts.customRecyclerView.CustomRecyclerView;
import com.bubblesocialapp.bubble.customLayouts.customViewPager.MaterialViewPagerHelper;
import com.bubblesocialapp.bubble.parseObjects.Post;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;



public class BuddiesFragment extends Fragment {
    private List<ParseObject> pendingBuddies;
    private CustomRecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    private RecyclerView.Adapter mAdapter;
    public BuddiesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_buddies_recycler_view, container, false);
        mRecyclerView = (CustomRecyclerView) rootView.findViewById(R.id.fragment_recycler_view_buddies);
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
        loadInfoView();

        return rootView;
    }

    private void loadInfoView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        searchPendingBuddies();

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(true);
                searchPendingBuddies();
            }
        });

        super.onViewCreated(view, savedInstanceState);
    }

    public void searchPendingBuddies() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("_User");
        query.whereMatches("username", ParseUser.getCurrentUser().getUsername());
        query.addDescendingOrder("createdAt");
        query.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                if (e == null) {
                    // Find pending buddies
                    if (parseObject.get("pendingBuddies") != null) {
                        // Get all Buddy Info
                        queryUsers(parseObject);
                    }
                } else {
                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void queryUsers(final ParseObject user) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("_User");
        query.whereContainedIn("objectId", user.getList("pendingBuddies"));
        query.addDescendingOrder("createdAt");
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                pendingBuddies = new ArrayList<ParseObject>();
                pendingBuddies.addAll(list);
                // Now query all Posts made by Buddies
                onBuddiesFound(user);
            }
        });
    }

    public void onBuddiesFound(ParseObject parseObjects){
        // Do you have any buddies. No? Well that sucks for you
        if (parseObjects.getList("buddies") == null){
            FeedAdapter feedAdapter = new FeedAdapter(getActivity(),null,
                    Util.getDisplaySize(getActivity()),FeedFragment.SHOW_POSTS_HAPPENING_NOW);
            mAdapter = new BuddiesAdapter(getActivity(),feedAdapter,pendingBuddies);
            mRecyclerView.setAdapter(mAdapter);
            MaterialViewPagerHelper.registerRecyclerView(getActivity(), mRecyclerView, null);
            onRefreshComplete();

            // Yay, you have buddies. Lets show their posts!
        } else {
            ParseQuery<Post> query = new ParseQuery<Post>("Post");
            query.whereContainedIn("authorId", parseObjects.getList("buddies"));
            query.addDescendingOrder("createdAt");
            query.findInBackground(new FindCallback<Post>() {
                @Override
                public void done(List<Post> parseObjects, ParseException e) {
                    //Make sure pending buddies shows up on top of buddies posts
                    FeedAdapter feedAdapter = new FeedAdapter(getActivity(),parseObjects,
                            Util.getDisplaySize(getActivity()),FeedFragment.SHOW_POSTS_HAPPENING_NOW);
                    mAdapter = new BuddiesAdapter(getActivity(),feedAdapter, pendingBuddies);
                    mRecyclerView.setAdapter(mAdapter);
                    onRefreshComplete();
                }
            });
        }
    }

    private void onRefreshComplete() {
        // Stop the refreshing indicator
        mAdapter.notifyDataSetChanged();
        mSwipeRefreshLayout.setRefreshing(false);
    }

}

// Obsolete Code
//    public List<ParseObject> convertToParseObject(ArrayList<String> list) {
//        List<ParseObject> pendingBuddiesObject = new ArrayList<>();
//        for (String pendingBuddy : list) {
//            ParseObject pending = new ParseObject("Buddies");
//            pending.put("pendingBuddies", pendingBuddy);
//            pending.put("ClassName", "_User");
//            pendingBuddiesObject.add(pending);
//        }
//        return pendingBuddiesObject;
//    }