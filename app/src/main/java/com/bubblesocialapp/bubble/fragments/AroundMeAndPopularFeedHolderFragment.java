package com.bubblesocialapp.bubble.fragments;


import android.content.Context;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bubblesocialapp.bubble.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AroundMeAndPopularFeedHolderFragment extends Fragment{

    private View mView;
    private TextView mWhosAroundMe;
    private TextView mPopularPosts;
    private Point mSize;
    private LinearLayout mLayout;
    private Boolean mHidden = false;

    public AroundMeAndPopularFeedHolderFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.fragment_around_me_and_popular_feed_holder, container, false);

        getScreenSize();
        loadViewCompenents();
        setClickListeners();

        return mView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        inflateFragment(new AroundMeFragment());

    }

    public void getScreenSize() {
        WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        mSize = new Point();
        display.getSize(mSize);
    }

    public void loadViewCompenents() {
        mLayout = (LinearLayout) mView.findViewById(R.id.poppin_toolbar);
        mWhosAroundMe = (TextView) mView.findViewById(R.id.whos_around_me_textview);
        mPopularPosts = (TextView) mView.findViewById(R.id.popular_posts_textview);
    }

    public void setClickListeners() {
        //TODO: Disable button if fragment is loaded
        mWhosAroundMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AroundMeFragment fragment = new AroundMeFragment();
                inflateFragment(fragment);
            }
        });

        mPopularPosts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FeedFragment fragment = FeedFragment.newInstance(FeedFragment.SHOW_POPULAR_POSTS, isNetworkAvailable());
                inflateFragment(fragment);
            }
        });
    }

    public void inflateFragment (Fragment fragment) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fragment_layout, fragment);
        transaction.commit();

    }

    public void onScrolled(int dy) {
        if (dy > 0) {
//            TranslateAnimation animation = new TranslateAnimation(0,0,0,mSize.y);
//            animation.setDuration(100);
//            mLayout.startAnimation(animation);
            mLayout.setVisibility(View.GONE);
        } else if (dy < 10) {
//            TranslateAnimation animation = new TranslateAnimation(0,0,mSize.y,0);
//            animation.setDuration(100);
//            mLayout.startAnimation(animation);
            mLayout.setVisibility(View.VISIBLE);
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
