package com.bubblesocialapp.bubble.fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Toast;

import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.adapters.GalleryAdapter;
import com.bubblesocialapp.bubble.common.Util;
import com.bubblesocialapp.bubble.parseObjects.Photo;
import com.felipecsl.asymmetricgridview.library.Utils;
import com.felipecsl.asymmetricgridview.library.widget.AsymmetricGridView;
import com.github.clans.fab.FloatingActionButton;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.io.IOException;
import java.util.List;

public class GalleryFragment extends Fragment implements AdapterView.OnItemClickListener{

    private static final String ARG_PARAM1 = "param1";

    private String mObjectId;
    private FloatingActionButton mFloatingActionButton;
    private OnPhotoClickListener mListener;
    private AsymmetricGridView mGridView;
    private GalleryAdapter mAdapter;
    private List<Photo> mPhoto;
    private String[] URL;

    public interface OnPhotoClickListener {
        // TODO: Update argument type and name
        public void onPhotoClick(String[] url, int position);
        public void addPhoto();
    }

    public static GalleryFragment newInstance(String userObjectId) {
        GalleryFragment fragment = new GalleryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, userObjectId);
        fragment.setArguments(args);
        return fragment;
    }

    public GalleryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mObjectId = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_photo, container, false);
        mFloatingActionButton = (FloatingActionButton) rootView.findViewById(R.id.fragment_float_action_button_photo);
        mGridView = (AsymmetricGridView) rootView.findViewById(R.id.photo_gridview);
        mFloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.addPhoto();
            }
        });

        setGridViewOptions();

        getPhotos();

        return rootView;
    }

    public void setGridViewOptions(){
        WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        size.x = (int) Util.pxToDp(getActivity(), size.x / 4);
        size.y = (int) Util.pxToDp(getActivity(), size.y / 4);

        mGridView.setRequestedColumnCount(3);
        mGridView.setRequestedColumnWidth(Utils.dpToPx(getActivity(), size.x));
        mGridView.setOnItemClickListener(this);
    }

    public void getPhotos() {
        ParseObject userObject = ParseObject.createWithoutData("_User", mObjectId);
        ParseQuery<Photo> query = new ParseQuery<>("Photo");
        query.whereEqualTo("user", userObject);
        query.orderByDescending("createdAt");
        query.findInBackground(new FindCallback<Photo>() {
            @Override
            public void done(List<Photo> list, ParseException e) {
                mPhoto = list;
                createAsymmetricItem(mPhoto);
                setPhotoAdapter(mPhoto);
            }
        });
    }


    public void setPhotoAdapter(List<Photo> list) {
        mAdapter = new GalleryAdapter(getActivity(),mGridView,list);
        mGridView.setAdapter(mAdapter);

        setGridClickListener();
    }

    public void setGridClickListener(){

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //TODO: Load from cache
                URL = retrieveUrlFromObjects(mPhoto);
                mListener.onPhotoClick(URL, i);
            }
        });
    }

    public String[] retrieveUrlFromObjects(List<Photo> photo) {
        String[] allUrl = new String[photo.size()];
        int j = 0;
        for(Photo pic:photo) {
            allUrl[j] = pic.getPhotoUrl();
            j += 1;
        }
        return allUrl;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnPhotoClickListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void createAsymmetricItem(List<Photo> list) {
        for (int i = 0; i < list.size(); i++) {
            if ((i+1) % 6 == 1) {
                list.get(i).setColumnSpan(2);
                list.get(i).setRowSpan(2);
                list.get(i).setPosition(i);
            } else if ((i+1) % 6 == 2) {
                list.get(i).setColumnSpan(1);
                list.get(i).setRowSpan(1);
                list.get(i).setPosition(i);
            } else if ((i+1) % 6 == 3) {
                list.get(i).setColumnSpan(1);
                list.get(i).setRowSpan(1);
                list.get(i).setPosition(i);
            } else if ((i+1) % 6 == 4) {
                list.get(i).setColumnSpan(1);
                list.get(i).setRowSpan(1);
                list.get(i).setPosition(i);
            } else if ((i+1) % 6 == 5) {
                list.get(i).setColumnSpan(1);
                list.get(i).setRowSpan(1);
                list.get(i).setPosition(i);
            } else if ((i+1) % 6 == 0) {
                list.get(i).setColumnSpan(2);
                list.get(i).setRowSpan(2);
                list.get(i).setPosition(i);
            }
        }

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
    }

    public void addPictureToGallery(Uri uri) {
        int listSize;
        if (mPhoto != null) {
            listSize = mPhoto.size();
        } else {
            listSize = 0;
        }
        Photo photo = new Photo();
        photo.setPhotoUri(uri);
        photo.setUser(ParseUser.getCurrentUser());
        photo.setPosition(listSize + 1);
        if ((listSize + 1) % 6 == 1 || (listSize + 1) % 6 == 0) {
            photo.setRowSpan(2);
            photo.setColumnSpan(2);
        } else {
            photo.setRowSpan(1);
            photo.setColumnSpan(1);
        }

        try {
            byte[] photoByte = Util.UriToByte(getActivity(), uri);
            photoByte = Util.scalePhoto(photoByte);
            ParseFile savePhoto = new ParseFile("galleryPic", photoByte);
            photo.setPhoto(savePhoto);
            photo.saveInBackground();
            mPhoto.add(photo);
            mAdapter.notifyDataSetChanged();
            mGridView.setAdapter(mAdapter);
        } catch (IOException e) {
            // Log
        }
    }
}
