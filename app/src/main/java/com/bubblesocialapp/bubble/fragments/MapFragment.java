package com.bubblesocialapp.bubble.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.backgroundTasks.GetPlacesTask;
import com.bubblesocialapp.bubble.customLayouts.custom_map_markers.IconGenerator;
import com.bubblesocialapp.bubble.interfaces.ReturnPlacesInterface;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MapFragment extends Fragment implements ReturnPlacesInterface{

    private static final String USER_PICKED_SEARCH_RADIUS = "user_picked_search_radius";

    private static final String SHARE_LOCATION = "Share_location";

    final private double metersToMiles = 0.000621371;

    private final int MAX_PLACES = 20;

    private MapView mapView;
    private GoogleMap googleMap;
    private Location mCurrentLocation;
    private int zoomLevel = 13;
    private LatLng currentLocation;
    private Drawable backgroundDrawable;
    private int bubblePink;
    private int bubblePinkTransparent;
    private int white;
    private Marker startupMarker;
    private Circle startupCircle;
    private Marker marker;
    private Circle circle;
    private SharedPreferences sp;
    private Marker[] mBusinessMarkers;
    private MarkerOptions[] mMarkerOptions;

    public MapFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        sp = PreferenceManager
                .getDefaultSharedPreferences(getActivity());

        View rootView = inflater.inflate(R.layout.fragment_map, container, false);
        mapView = (MapView) rootView.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        googleMap = mapView.getMap();

        centerMapOnMyLocation();

        showNearbyBusinesses();

        return rootView;
    }

    private void showNearbyBusinesses() {
        mBusinessMarkers = new Marker[MAX_PLACES];

    }

    private void getResourcesForCircle() {
        bubblePink = getResources().getColor(R.color.bubble_pink);
        bubblePinkTransparent = getResources().getColor(R.color.bubble_pink_transparent);
        white = getResources().getColor(R.color.white);
        backgroundDrawable = ContextCompat.getDrawable(getActivity(),R.drawable.background_bubble_radius_circle);
    }

    private void centerMapOnMyLocation() {

        getResourcesForCircle();

        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        mCurrentLocation = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, false));

        if (mCurrentLocation != null) {
            currentLocation = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());

            IconGenerator iconGenerator = new IconGenerator(getActivity());
            iconGenerator.setBackground(backgroundDrawable);
            BitmapDescriptor backgroundRadius = BitmapDescriptorFactory.fromBitmap(iconGenerator.makeIcon("10"));

            zoomLevel = 10;

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(currentLocation)      // Sets the center of the map to location user
                    .zoom(zoomLevel)
                    .build();                   // Creates a CameraPosition from the builder

            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            MarkerOptions markerOptions = new MarkerOptions()
                    .position(currentLocation)
                    .flat(true)
                    .icon(backgroundRadius)
                    .anchor(0.5f, 0.5f);

            CircleOptions circleOptions = new CircleOptions()
                    .center(currentLocation)
                    .radius(16094)
                    .strokeColor(white)
                    .strokeWidth(2)
                    .fillColor(bubblePinkTransparent);

            startupMarker = googleMap.addMarker(markerOptions);

            startupCircle = googleMap.addCircle(circleOptions);
            startupCircle.setVisible(true);

            new GetPlacesTask(getActivity(),mCurrentLocation,this).execute();
            setZoomChangeListener();

        }
    }

    public void setZoomChangeListener() {
        googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                if (Math.abs(cameraPosition.zoom - zoomLevel) > 0.1){

                    startupMarker.remove();
                    startupCircle.remove();

                    if (marker != null){
                        marker.remove();
                    }
                    if (circle != null){
                        circle.remove();
                    }

                    zoomLevel = (int) cameraPosition.zoom;

                    int radius = zoomToRadius(cameraPosition.zoom);
                    double radiusInMiles = radius*metersToMiles;

                    IconGenerator iconGenerator = new IconGenerator(getActivity());
                    iconGenerator.setBackground(backgroundDrawable);
                    BitmapDescriptor backgroundRadius =
                            BitmapDescriptorFactory.fromBitmap(iconGenerator.makeIcon(Integer.toString((int) radiusInMiles)));

                    MarkerOptions markerOptions = new MarkerOptions()
                            .position(currentLocation)
                            .flat(true)
                            .icon(backgroundRadius)
                            .anchor(0.5f, 0.5f);

                    CircleOptions circleOptions = new CircleOptions()
                            .center(currentLocation)
                            .radius(radius)
                            .strokeColor(white)
                            .fillColor(bubblePinkTransparent);

                    sp.edit().putInt(USER_PICKED_SEARCH_RADIUS, (int) radiusInMiles).apply();

                    marker = googleMap.addMarker(markerOptions);

                    circle = googleMap.addCircle(circleOptions);
                    circle.setVisible(true);

                }
            }
        });
    }

    private int zoomToRadius(float zoom){

        double radius = 0;
        double earthLength = 40075004; //Circumference of Earth in Meters
        Point size = new Point();
        getActivity().getWindowManager().getDefaultDisplay().getSize(size);
        double widthInPixels = size.x;
        double metersInPixels = earthLength/256;

        if (zoom > 0){
            radius = metersInPixels*widthInPixels/(6*Math.pow(2,zoom));
        } else {
            radius = metersInPixels*widthInPixels;
        }

        if (radius >= 16093.4) //1 mile = 1609.34 meters
            return (int) 16100;
        else
            return (int) radius;
    }

    @Override
    public void onPlacesReturned(JSONObject jsonObject) {
        if (jsonObject != null) {
            removeBusinessLocations();
            try {
                JSONArray placesArray = jsonObject.getJSONArray("results");
                updateBusinessLocations(placesArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void removeBusinessLocations() {
        if (mBusinessMarkers != null) {
            for (int businessMarker=0; businessMarker < mBusinessMarkers.length; businessMarker++) {
                if (mBusinessMarkers[businessMarker] != null) {
                    mBusinessMarkers[businessMarker].remove();
                }
            }
        }
    }

    private void updateBusinessLocations(JSONArray jsonArray) {
        mMarkerOptions = new MarkerOptions[jsonArray.length()];
        //loop through places
        for (int p=0; p < jsonArray.length(); p++) {
            boolean missingValue = false;
            LatLng placeLL=null;
            String placeName="";
            String vicinity="";
            try{
                JSONObject placeObject = jsonArray.getJSONObject(p);
                JSONObject loc = placeObject.getJSONObject("geometry").getJSONObject("location");
                placeLL = new LatLng(
                        Double.valueOf(loc.getString("lat")),
                        Double.valueOf(loc.getString("lng")));
                JSONArray types = placeObject.getJSONArray("types");
                for(int t=0; t<types.length(); t++){
                    String thisType=types.get(t).toString();
                    if(thisType.contains("food")){
                        break;
                    }
                    else if(thisType.contains("bar")){
                        break;
                    }
                    else if(thisType.contains("store")){
                        break;
                    }
                }
                vicinity = placeObject.getString("vicinity");
                placeName = placeObject.getString("name");
            }
            catch(JSONException jse){
                missingValue=true;
                jse.printStackTrace();
            }
            if(missingValue) {
                mMarkerOptions[p] = null;
            } else {
                mMarkerOptions[p]=new MarkerOptions()
                        .position(placeLL)
                        .title(placeName)
                        .snippet(vicinity);
            }
        }

        if(mMarkerOptions!=null && mBusinessMarkers!=null){
            for(int p=0; p<mMarkerOptions.length && p<mBusinessMarkers.length; p++){
                //will be null if a value was missing
                if(mMarkerOptions[p]!=null)
                    mBusinessMarkers[p]=googleMap.addMarker(mMarkerOptions[p]);
            }
        }
    }
}
