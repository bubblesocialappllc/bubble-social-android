package com.bubblesocialapp.bubble.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.adapters.NotificationAdapter;
import com.bubblesocialapp.bubble.customLayouts.customRecyclerView.CustomRecyclerView;
import com.bubblesocialapp.bubble.parseObjects.Notification;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.List;

/**
 * Created by Sadaf Mackertich on 11/9/2014.
 */
public class NotificationsFragment extends Fragment {
    private CustomRecyclerView mRecyclerView;
    private NotificationAdapter mAdapter;

    public NotificationsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_notifications_recycler_view, container, false);
        mRecyclerView = (CustomRecyclerView) rootView.findViewById(R.id.fragment_recycler_view_notifications);
        loadInfoView();
        getNotifications();
        return rootView;
    }

    private void loadInfoView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(layoutManager);
    }

    public void getNotifications(){
        ParseQuery<Notification> query = ParseQuery.getQuery("Notification");
        query.whereMatches("sendTo", ParseUser.getCurrentUser().getUsername());
        query.orderByDescending("createdAt");
        query.findInBackground(new FindCallback<Notification>() {
            @Override
            public void done(List<Notification> notifications, ParseException e) {
                if (e == null) {
                    // Display Notification. On click send to post or comment
                    attachAdapter(notifications);
                } else {
                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void attachAdapter(List<Notification> notification) {
        mAdapter = new NotificationAdapter(getActivity(),notification);
        mRecyclerView.setAdapter(mAdapter);
    }



}
