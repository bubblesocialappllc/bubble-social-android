package com.bubblesocialapp.bubble.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.adapters.PhotoPagerAdapter;

import java.util.List;

/**
 * Created by sadaf on 8/1/15.
 */
public class DisplayPhotoFragment extends Fragment {

    private static final String URL = "All_URLS";
    private static final String POSITON = "position_of_picture";

    private PhotoPagerAdapter mPagerAdapter;
    private ViewPager mViewPager;
    private String[] mURL;
    private String singleURL;
    private int mPosition;

    public static DisplayPhotoFragment newInstance(String[] url, int position){
        DisplayPhotoFragment photoFragment = new DisplayPhotoFragment();
        Bundle bundle = new Bundle();
        bundle.putStringArray(URL, url);
        bundle.putInt(POSITON, position);
        photoFragment.setArguments(bundle);
        return photoFragment;
    }

    // Using single URL instance constructor now to compensate for future changes to backend
    public static DisplayPhotoFragment newInstance(String url, int position){
        DisplayPhotoFragment photoFragment = new DisplayPhotoFragment();
        Bundle bundle = new Bundle();
        bundle.putString(URL, url);
        bundle.putInt(POSITON, position);
        photoFragment.setArguments(bundle);
        return photoFragment;
    }

    public DisplayPhotoFragment() {
        super();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().getStringArray(URL) != null) {
                mURL = getArguments().getStringArray(URL);
            } else {
                singleURL = getArguments().getString(URL);
            }
            mPosition = getArguments().getInt(POSITON);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_display_photo,container, false);

        mViewPager = (ViewPager) view.findViewById(R.id.photo_pager_adapter);
        // Pass cached Uri
        if (mURL != null) {
            mPagerAdapter = new PhotoPagerAdapter(getActivity(), mURL);
        } else {
            mPagerAdapter = new PhotoPagerAdapter(getActivity(),singleURL);
        }
        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.setCurrentItem(mPosition);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
