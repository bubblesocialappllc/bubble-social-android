package com.bubblesocialapp.bubble.fragments;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.dialogs.CameraDialog;
import com.bubblesocialapp.bubble.parseObjects.User;
import com.google.gson.Gson;

import de.hdodenhof.circleimageview.CircleImageView;

public class SignUpSecondFragment extends Fragment {
    private static final String USER_OBJECT = "user";

    // TODO: Rename and change types of parameters
    private CircleImageView mImageView;
    private EditText mDescription;
    private boolean imageSet = false;
    private CameraDialog mDialog;
    private User mCurrentUser;

    private OnSecondSignUpFragmentListener mListener;

    public SignUpSecondFragment() {
        // Required empty public constructor
    }

    public interface OnSecondSignUpFragmentListener {
        void onSecondSignUpFragmentComplete(String JsonObject);
    }

    public static SignUpSecondFragment newInstance(String userJson) {
        SignUpSecondFragment fragment = new SignUpSecondFragment();
        Bundle args = new Bundle();
        args.putString(USER_OBJECT, userJson);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String userJson = getArguments().getString(USER_OBJECT);
            Gson gSon = new Gson();
            mCurrentUser = gSon.fromJson(userJson, User.class);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View rootView = inflater.inflate(R.layout.fragment_signup_second, container, false);
        mImageView = (CircleImageView) rootView.findViewById(R.id.profile_image);
        mDescription = (EditText) rootView.findViewById(R.id.description_textbox);
        mImageView.setImageResource(R.drawable.ic_stub);

        setImageChooserDialog();

        setDescription();

        return rootView;
    }

    public void setDescription(){
        mDescription.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mCurrentUser.setAboutMe(mDescription.getText().toString());
            }
        });
    }

    public void setImageChooserDialog(){
        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog = new CameraDialog();
                mDialog.show(getFragmentManager(),"profilePicture");
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_sign_up_second_fragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case (R.id.continue_sign_up):
                verifyData();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void verifyData(){
        if (!imageSet){
            Toast.makeText(getActivity(),"Please pick a profile picture",Toast.LENGTH_LONG).show();
        }

        if (mDescription.getText() == null){
            Toast.makeText(getActivity(),"Please tell us a little about yourself",Toast.LENGTH_LONG).show();
        }

        if (mDescription.getText() != null && imageSet){
            startNextFragment();
        }
    }

    public void startNextFragment(){
        String userJson = new Gson().toJson(mCurrentUser);
        mListener.onSecondSignUpFragmentComplete(userJson);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnSecondSignUpFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void setProfilePic(Uri image) {

        //TODO: Change over to bootstrap circle view
        mImageView.setImageURI(image);
        imageSet = true;
        mCurrentUser.setProfilePic(image.toString());

    }

}
