package com.bubblesocialapp.bubble.fragments;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.adapters.AroundMeAdapter;
import com.bubblesocialapp.bubble.common.Util;
import com.bubblesocialapp.bubble.customLayouts.customRecyclerView.CustomRecyclerView;
import com.bubblesocialapp.bubble.common.LocationController;
import com.bubblesocialapp.bubble.customLayouts.customViewPager.MaterialViewPagerHelper;
import com.bubblesocialapp.bubble.customLayouts.customViewPager.RecyclerViewMaterialAdapter;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.List;

/**
 * Created by Sadaf on 2/21/2015.
 */
public class AroundMeFragment extends Fragment implements LocationController.LocationCallBack{

    private static final String USER_PICKED_SEARCH_RADIUS = "user_picked_search_radius";

    private static final String SHARE_LOCATION = "Share_location";

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private CustomRecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private LocationController mLocationController;
    private Location mCurrentLocation;
    private double maxDistance = 10;
    private SharedPreferences.OnSharedPreferenceChangeListener mListener;
    private ScrollListenerInterface mOnScrollInterface;
    private Point size;

    public interface ScrollListenerInterface {
        public void onScrolledInFragment(int dy);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        setLocationController();

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
        maxDistance = sp.getInt(USER_PICKED_SEARCH_RADIUS,5);

        mListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
            public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
                if (key.equals(USER_PICKED_SEARCH_RADIUS) ) {
                    maxDistance = prefs.getInt(key, 5);
                    initiateRefresh();
                }
            }
        };

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_around_me_recycler_view,container,false);

        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);

        mRecyclerView = (CustomRecyclerView)
                rootView.findViewById(R.id.fragment_recycler_view_bubblers);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(layoutManager);

        setScrollListener();

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        size = Util.getDisplaySize(getActivity());
        size.x = (int) Util.pxToDp(getActivity(), size.x);
        size.y = (int) Util.pxToDp(getActivity(),size.y);

        mSwipeRefreshLayout.setProgressViewOffset(false, -200, size.y-100);
        mSwipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.bubble_pink));

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                initiateRefresh();
            }
        });

    }

    public void setLocationController() {
        mLocationController = new LocationController(getActivity(),this,LocationController.FIND_LOCATION);
        mLocationController.connectToGoogleApiClient();
    }

    public void setScrollListener() {
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                mOnScrollInterface.onScrolledInFragment(dy);
                super.onScrolled(recyclerView, dx, dy);
            }
        });
    }

    public void getData(Location location) {

        ParseQuery<ParseUser> query = new ParseQuery<>("_User");
        ParseGeoPoint currLocation = new ParseGeoPoint(location.getLatitude(), location.getLongitude());
        query.whereNotEqualTo("username",ParseUser.getCurrentUser().getUsername());
        query.whereWithinMiles("location", currLocation, maxDistance);
        query.orderByDescending("createdAt");
        //TODO: Query by most popular
        query.findInBackground(new FindCallback<ParseUser>() {
            @Override
            public void done(List<ParseUser> users, ParseException e) {

                mAdapter = new RecyclerViewMaterialAdapter(new AroundMeAdapter(getActivity(), users));
                mRecyclerView.setAdapter(mAdapter);
                MaterialViewPagerHelper.registerRecyclerView(getActivity(), mRecyclerView, null);
                onRefreshComplete();
            }
        });
    }

    private void initiateRefresh() {

        mLocationController.startLocationUpdates();
        mSwipeRefreshLayout.setRefreshing(true);
        //TODO: Put in network time out error
    }


    private void onRefreshComplete() {

        // Stop the refreshing indicator
        mAdapter.notifyDataSetChanged();
        mSwipeRefreshLayout.setRefreshing(false);
    }


    @Override
    public void onNewLocation(Location location, float accuracy) {
        mCurrentLocation = location;
        getData(location);
    }

    @Override
    public void onAddressFound(List<Address> address) {

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception.
        try {
            mOnScrollInterface = (ScrollListenerInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement PostListener");
        }
    }
}
