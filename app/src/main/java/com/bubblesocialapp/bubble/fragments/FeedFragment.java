package com.bubblesocialapp.bubble.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.adapters.FeedAdapter;
import com.bubblesocialapp.bubble.adapters.NoConnectionAdapter;
import com.bubblesocialapp.bubble.common.Constants;
import com.bubblesocialapp.bubble.customLayouts.customRecyclerView.CustomRecyclerView;
import com.bubblesocialapp.bubble.common.LocationController;
import com.bubblesocialapp.bubble.common.Util;
import com.bubblesocialapp.bubble.customLayouts.customViewPager.MaterialViewPagerHelper;
import com.bubblesocialapp.bubble.customLayouts.customViewPager.RecyclerViewMaterialAdapter;
import com.bubblesocialapp.bubble.dialogs.CustomProgressDialog;
import com.bubblesocialapp.bubble.parseObjects.Post;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class FeedFragment extends Fragment implements LocationController.LocationCallBack {



    private static final String SHARE_LOCATION = "Share_location";

    private static final String KEY = "Query_Key";

    private static final String CONNECTION = "connected";

    public static final String SHOW_POSTS_HAPPENING_NOW = "Show_current_posts";

    public static final String SHOW_POPULAR_POSTS = "Popular_posts";

    public static final String SHOW_USERS_OLD_POSTS = "Old_posts";

    private static final int LAST_24_HOURS = 1000 * 60 * 60 * 24;

    private SharedPreferences.OnSharedPreferenceChangeListener mListener;
    private PostListener mPostListener;
    private View mViewRecyclerCardsView;
    private CustomRecyclerView mRecyclerView;
    private FloatingActionMenu mFloatingActionMenu;
    private FloatingActionButton mFloatingBuddiesButton;
    private FloatingActionButton mFloatingNotificationsButton;
    private FloatingActionButton mFloatingPostButton;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView.Adapter mAdapter;
    private FeedAdapter feedAdapter;
    private LocationController mLocationController;
    private Location mCurrentLocation;
    private double maxDistance = 10;
    private String mFlag;
    private String mDataString;
    private Point size; // Screen Size
    private FrameLayout mButtonBackground;
    private Toolbar mToolbar;
    private LinearLayoutManager mLinearLayoutManager;
    private List<Post> mPosts = null;
    private boolean paginationStarted = false;
    private boolean mNetworkConnected;
    private ProgressDialog mProgressDialog;
    private boolean onFirstLoaded = false;

    public interface PostListener {
        /**
         * Called when posting
         */
        public void onPostButtonPressed();
        public void onNotificationsButtonPressed();
        public void onBuddiesButtonPressed();
        public void onScrolledInFragment(int dy);
    }

    public static FeedFragment newInstance(String flagType, boolean isConnected) {
        FeedFragment fragment = new FeedFragment();
        Bundle bundle = new Bundle();
        bundle.putString(KEY, flagType);
        bundle.putBoolean(CONNECTION, isConnected);
        fragment.setArguments(bundle);
        return fragment;
    }

    private static final String LOG_TAG = FeedFragment.class.getSimpleName();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: Add redundant is network available check
        if (getArguments() != null) {
            mNetworkConnected = getArguments().getBoolean(CONNECTION);
            if (getArguments().getString(KEY) != null) {
                mFlag = getArguments().getString(KEY);
            } else {
                mFlag = SHOW_POSTS_HAPPENING_NOW;
            }
        } else {
            mFlag = SHOW_POSTS_HAPPENING_NOW;
        }

        // Get Screen size to expand picture
        size = Util.getDisplaySize(getActivity());
        size.x = (int) Util.pxToDp(getActivity(), size.x);
        size.y = (int) Util.pxToDp(getActivity(),size.y);

        // Notify the system to allow an options menu for this fragment.
        setHasOptionsMenu(true);

        mLocationController = new LocationController(getActivity(),this,LocationController.FIND_LOCATION);
        mLocationController.connectToGoogleApiClient();
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
        maxDistance = sp.getInt(Constants.USER_PICKED_SEARCH_RADIUS,10);

        mListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
            public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
                if (key.equals(Constants.USER_PICKED_SEARCH_RADIUS) ) {
                    maxDistance = prefs.getInt(key, 10);
                    initiateRefresh();
                }
            }
        };
        sp.registerOnSharedPreferenceChangeListener(mListener);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception.
        try {
            mPostListener = (PostListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement PostListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mViewRecyclerCardsView = inflater.inflate(R.layout.fragment_feed_recycler_view, container, false);
        mToolbar = (Toolbar) mViewRecyclerCardsView.findViewById(R.id.toolbar);
        mToolbar.setVisibility(View.GONE);

        loadViewComponents();
        loadInfoView();

        // If network is not connected
        if (!mNetworkConnected) {

            NoConnectionAdapter connectionAdapter = new NoConnectionAdapter(getActivity());

            mAdapter = new RecyclerViewMaterialAdapter(connectionAdapter);

            mRecyclerView.setAdapter(mAdapter);
            MaterialViewPagerHelper.registerRecyclerView(getActivity(), mRecyclerView, null);

        } else {
            if (mFlag.equals(SHOW_POSTS_HAPPENING_NOW)) {
                mProgressDialog = CustomProgressDialog.newInstance(getActivity());
                mProgressDialog.show();
            }
            setClickListeners();
            setScrollListener();
        }

        return mViewRecyclerCardsView;
    }

    public void setScrollListener() {
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                // Hide Floating action Menu
                if (dy > 0) {
                    mFloatingActionMenu.hideMenuButton(true);
                } else {
                    mFloatingActionMenu.showMenuButton(true);
                }
                if (mFlag.equals(SHOW_POPULAR_POSTS)) {
                    // Hide bottom Toolbar
                    mPostListener.onScrolledInFragment(dy);
                }
                // For pagination
                int count = mLinearLayoutManager.getItemCount();
                if (mLinearLayoutManager.findLastVisibleItemPosition() > count-4) {
                    int lastItem = count - 2;
                    Post post = feedAdapter.getItem(lastItem);
                    setQuery(mCurrentLocation, post.getCreatedAt());
                }

            }
        });
    }

    public void setClickListeners(){


        mButtonBackground.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (mFloatingActionMenu.isEnabled()) {
                    mFloatingActionMenu.toggle(true);
                    return true;
                }
                return false;
            }
        });

        if (mFlag.equals(SHOW_POSTS_HAPPENING_NOW)) {

            mFloatingActionMenu.setOnMenuToggleListener(new FloatingActionMenu.OnMenuToggleListener() {
                @Override
                public void onMenuToggle(boolean b) {
                    if (b) {
                        mFloatingPostButton.setVisibility(View.VISIBLE);
                        mButtonBackground.setVisibility(View.VISIBLE);
                    } else {
                        mFloatingPostButton.setVisibility(View.GONE);
                        mButtonBackground.setVisibility(View.GONE);
                    }
                }
            });

            mFloatingPostButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mPostListener.onPostButtonPressed();
                }
            });

            mFloatingBuddiesButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mPostListener.onBuddiesButtonPressed();
                }
            });

            mFloatingNotificationsButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mPostListener.onNotificationsButtonPressed();
                }
            });
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mSwipeRefreshLayout.setProgressViewOffset(false, -200, size.y-100);
        mSwipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.bubble_pink));

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                initiateRefresh();
            }
        });

    }

    private void loadViewComponents() {

        mSwipeRefreshLayout = (SwipeRefreshLayout)
                mViewRecyclerCardsView.findViewById(R.id.swipe_container);

        mRecyclerView = (CustomRecyclerView)
                mViewRecyclerCardsView.findViewById(R.id.fragment_recycler_view_feed);

        mFloatingActionMenu = (FloatingActionMenu)
                mViewRecyclerCardsView.findViewById(R.id.fragment_float_action_menu);

        mButtonBackground = (FrameLayout)
                mViewRecyclerCardsView.findViewById(R.id.floating_button_background);

        if (mFlag.equals(SHOW_POSTS_HAPPENING_NOW)) {

            mFloatingBuddiesButton = (FloatingActionButton)
                    mViewRecyclerCardsView.findViewById(R.id.buddy_feed_button);

            mFloatingNotificationsButton = (FloatingActionButton)
                    mViewRecyclerCardsView.findViewById(R.id.notifications_button);

            mFloatingPostButton = (FloatingActionButton)
                    mViewRecyclerCardsView.findViewById(R.id.fragment_float_action_post_button);

        } else {

            mFloatingActionMenu.setVisibility(View.GONE);

        }
    }

    private void loadInfoView() {
        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
    }

    private void getData(Location location) {

        if (mFlag.equals(SHOW_POSTS_HAPPENING_NOW)) {
            setQuery(location, null);
        } else if (mFlag.equals(SHOW_POPULAR_POSTS)) {
            setQuery(location, null);
        } else if (mFlag.equals(SHOW_USERS_OLD_POSTS)) {
            mToolbar.setVisibility(View.VISIBLE);
            setQuery(location, null);
        }

    }

    public void setQuery(Location location, final Date paginationTime) {
        paginationStarted = false;
        ParseQuery<Post> query = new ParseQuery<>("Post");
        ParseGeoPoint point = new ParseGeoPoint(location.getLatitude(), location.getLongitude());
        query.whereWithinMiles("location", point, maxDistance);
        Calendar currentDate = Calendar.getInstance();
        Date searchTime = new Date();
        searchTime.setTime(currentDate.getTimeInMillis() - LAST_24_HOURS);
        query.setLimit(50);

        if (mFlag.equals(SHOW_POSTS_HAPPENING_NOW)) {

            query.whereGreaterThanOrEqualTo("createdAt", searchTime);
            query.orderByDescending("createdAt");

        } else if (mFlag.equals(SHOW_POPULAR_POSTS)) {

            query.whereGreaterThanOrEqualTo("createdAt", searchTime);
            query.orderByDescending("points");

        } else if (mFlag.equals(SHOW_USERS_OLD_POSTS)) {

            query.whereEqualTo("authorId", ParseUser.getCurrentUser().getObjectId());
            query.orderByDescending("createdAt");

        }

        if (paginationTime != null) {
            query.whereLessThan("createdAt", paginationTime);
            paginationStarted = true;
        }

            query.findInBackground(new FindCallback<Post>() {
                @Override
                public void done(List<Post> parseObjects, ParseException e) {
                    // Set Adapter once Posts are retrieved

                    // Only load this dialog during initial startup
                    if (!onFirstLoaded && mFlag.equals(SHOW_POSTS_HAPPENING_NOW)) {
                        mProgressDialog.dismiss();
                        onFirstLoaded = true;
                    }

                    if (e != null) {

                        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();

                    } else {
                        if (!paginationStarted) {
                            mPosts = parseObjects;
                            feedAdapter = new FeedAdapter(getActivity(), mPosts, size, mFlag);
                            mAdapter = new RecyclerViewMaterialAdapter(feedAdapter);
                            mRecyclerView.setAdapter(mAdapter);
                            if (!mFlag.equals(SHOW_USERS_OLD_POSTS)) {
                                MaterialViewPagerHelper.registerRecyclerView(getActivity(), mRecyclerView, null);
                            }
                        } else {
                            mPosts.addAll(parseObjects);
                            feedAdapter = new FeedAdapter(getActivity(), mPosts, size, mFlag);
                            feedAdapter.notifyDataSetChanged();
                            mAdapter.notifyDataSetChanged();
                        }
                    }
                    onRefreshComplete();
                }
            });
        }


    private void initiateRefresh() {
        mLocationController.startLocationUpdates();
        mSwipeRefreshLayout.setRefreshing(true);
        //TODO: Put in network time out error
    }


    private void onRefreshComplete() {

        // Stop the refreshing indicator
        feedAdapter.notifyDataSetChanged();
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onNewLocation(Location location, float accuracy) {
        // If data is from splash screen get new location does not update the posts
        // User must initiate Refresh
        ParseGeoPoint geoPoint = new ParseGeoPoint(location.getLatitude(), location.getLongitude());
        ParseUser.getCurrentUser().put("location", geoPoint);
        ParseUser.getCurrentUser().saveInBackground();

        mCurrentLocation = location;
        getData(location);
    }

    @Override
    public void onAddressFound(List<Address> address) {
        // Dont require address for this fragment
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public void addNewPost(Post post) {
        mPosts.add(0,post);
        feedAdapter = new FeedAdapter(getActivity(), mPosts, size, mFlag);
        feedAdapter.notifyDataSetChanged();
        mAdapter.notifyDataSetChanged();
        mRecyclerView.scrollToPosition(0);
        mFloatingActionMenu.toggle(true);
    }
}