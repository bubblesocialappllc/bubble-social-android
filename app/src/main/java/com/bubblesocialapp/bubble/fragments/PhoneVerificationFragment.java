package com.bubblesocialapp.bubble.fragments;

import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bubblesocialapp.bubble.R;
import com.bubblesocialapp.bubble.common.ParseCloudCode;
import com.bubblesocialapp.bubble.common.Util;
import com.bubblesocialapp.bubble.dialogs.LoadingDialog;
import com.bubblesocialapp.bubble.parseObjects.User;
import com.google.gson.Gson;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

import java.io.IOException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this mFragment must implement the
 * {@link OnPhoneVerificationListener} interface
 * to handle interaction events.
 */
public class PhoneVerificationFragment extends Fragment {

    private static final String ARG_PARAM1 = "userJSonObject";

    private OnPhoneVerificationListener mListener;
    private static PhoneVerificationFragment mFragment;

    private static final int TIMER_LENGTH = 3000;
    private static final int TIMER_LENGTH2 = 3000;

    private Button mVerifyButton;
    private EditText mNumberInput;
    private LoadingDialog mDialog;
    private String userJSonObject;
    private ParseUser mCurrentUser;
    private Boolean mTimeout = false;
    private EditText mVerificationCode;

    public PhoneVerificationFragment(){}

    public static PhoneVerificationFragment newInstance() {
        mFragment = new PhoneVerificationFragment();
        /*Bundle args = new Bundle();
        args.putString(ARG_PARAM1, jSonObject);
        mFragment.setArguments(args);*/
        return mFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this mFragment

        View rootView = inflater.inflate(R.layout.fragment_phone_verification, container, false);

        mNumberInput = (EditText) rootView.findViewById(R.id.phone_number_input);

        mVerifyButton = (Button) rootView.findViewById(R.id.start_verification_button);

        mVerificationCode = (EditText) rootView.findViewById(R.id.verification_code_textview);

        mVerificationCode.setVisibility(View.INVISIBLE);

        mVerifyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mVerifyButton.getText().toString().toLowerCase().equals("send verification code")) {
                    mDialog = new LoadingDialog();
                    mDialog.show(getFragmentManager(), "dialog loading");
                    setVerificationSequence();
                }
            }
        });

        return rootView;
    }

    public void setVerificationSequence(){
        if ( mNumberInput.getText().toString().equals("") ){
            Toast.makeText(getActivity(),"Please enter a number", Toast.LENGTH_SHORT).show();
        } else if (mNumberInput.getText().length() < 10 || mNumberInput.getText().length() > 10 ){
            Toast.makeText(getActivity(),"Please enter a valid 10-digit phone number", Toast.LENGTH_SHORT).show();
        }
        else {
            ParseCloudCode.phoneVerification(mNumberInput.getText().toString());
            Timer timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    autoVerifySMS();
                }
            }, TIMER_LENGTH);

        }
    }

    public void autoVerifySMS(){
        Cursor smsCursor = getActivity().getContentResolver()
                .query(Uri.parse("content://sms/inbox"),
                        new String[]{"_id", "thread_id", "address", "person", "date", "body"},
                        null, null, null);

        if (smsCursor.moveToFirst()) {

            String phoneNumber = smsCursor.getString(smsCursor.getColumnIndex("address"));
            if (phoneNumber.equals("4072698066")){
                String messageBody = smsCursor.getString(smsCursor.getColumnIndex("body"));
                String[] messageParts = messageBody.split(":");
                String verificationCode = messageParts[1].trim();
                verifyCodeWithParse(verificationCode);
                smsCursor.close();
            } else if (mTimeout){
                mDialog.dismiss();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        manuallyVerifyCode();
                        Toast.makeText(getActivity(), "AutoVerification failed. Please enter verification code", Toast.LENGTH_LONG).show();
                    }
                });

            } else if (!mTimeout){

                // If message has not been received then wait. TODO: Must incorporate timeout.
                mTimeout = true;
                Timer timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        autoVerifySMS();
                    }
                }, TIMER_LENGTH2);
            }
        } else {
            mDialog.dismiss();
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    manuallyVerifyCode();
                    Toast.makeText(getActivity(), "AutoVerification failed. Please enter verification code", Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    public void manuallyVerifyCode(){
        mVerificationCode.setVisibility(View.VISIBLE);
        mNumberInput.setVisibility(View.INVISIBLE);
        mVerifyButton.setText("Verify Code");
        mVerifyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mVerificationCode.getText() != null){
                    mDialog.show(getFragmentManager(), "dialog loading");
                    verifyCodeWithParse(mVerificationCode.getText().toString());
                }
            }
        });
    }

    public void verifyCodeWithParse(final String verificationCode){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Phone");
        query.whereEqualTo("phoneNumber", "+1" + mNumberInput.getText());
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (e == null) {
                    for (ParseObject object:list) {
                        if (String.valueOf(object.getNumber("verificationCode")).equals(verificationCode)) {
                            object.put("verified", true);
                            object.saveInBackground();
                            //signUp();
                            mDialog.dismiss();
                            mListener.onSignUpClickFillInfo();
                            //mListener.onVerificationComplete();

                            break;
                        }
                    }
                } else {
                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnPhoneVerificationListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * mFragment to allow an interaction in this mFragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnPhoneVerificationListener {
        // TODO: Update argument type and name
         void onVerificationComplete();
        void onSignUpClickFillInfo();
    }

}
